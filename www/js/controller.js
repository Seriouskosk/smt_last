/**
 * Created by Wahlberg on 11.10.2014.
 */


 angular.module('ui', ['datePicker', 'ui.bootstrap', 'textAngular', 'checklist-model','ngSanitize', 'n3-line-chart']);
 var angModule = angular.module('ui'); 

 angModule.config(function($sceProvider,$httpProvider) {
    $sceProvider.enabled(false);
     //$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
});


function AlertsCtrl($scope, alertsManager) {
    $scope.alerts = alertsManager.alerts;

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };
}


angModule.factory('alertsManager', function() {
    return {
        alerts: [],
        addAlert: function(type, message) {
            this.alerts.push({ type: type, msg: message });
            console.log(this.alerts);
        },
        closeAlert : function(index) {
            this.alerts.splice(index, 1);
            console.log(this.alerts);
        },
        close : function(index) {
            this.alerts.splice(index, 1);
            console.log(this.alerts);
        },
        clear : function() {
            for(var x in this.alerts){
                delete this.alerts[x];
            }
        }
    };
});

//angModule.factory('api',function($http){
//   return {
//       updateDate: function(){
//           return $http.get('/main/getNewsCount',{
//           });
//       }
//   }
//});

angModule
.controller('globalVar', ['$scope', '$http', '$templateCache',
    function($scope, $rootScope, $http, $templateCache) {

    }]);

 angModule.controller('PaginationNewsCtrl', ['$scope', '$http', '$templateCache', '$sce',
    function($scope, $http, $templateCache, $sce) {
        $scope.method = 'post';
        $scope.dataToPost = {'offset':'0', 'count': '4'};
        var editCount;

        $scope.updateDate = function () {
            // $scope.bigTotalItems = date;
            $scope.method = 'post';
            $scope.url = '/main/getNewsCount';
            $scope.code = null;
            $scope.response = null; 
            $http({method: $scope.method, url: $scope.url, cache: $templateCache}).
            success(function(data, status) {
                $scope.status = status;
                $scope.datas = data;
                $scope.bigTotalItems = data.count-1;
                editCount = data.count;
                console.log($scope.url);
            }).
            error(function(data, status) {
                $scope.datas = data || "Request failed";
                $scope.status = status;
            });
        };


        $scope.viewAll = function(){
            $scope.dataToPost = {'offset': 0, 'count': editCount};
            $scope.fetch();
        };


        $scope.pageChanged = function() {
            var intoUrl = $scope.bigCurrentPage - 1;
            $scope.dataToPost = {'offset': intoUrl*4, 'count': '4'};
            console.log($scope.dataToPost);
            $scope.fetch();
        };

        $scope.maxSize = 5;
        $scope.bigTotalItems = 100;
        $scope.bigCurrentPage = 1;

        $scope.loading = true;

        $scope.fetch = function() {
            $scope.loading = true;
            $scope.code = null;
            $scope.response = null;
            $scope.url = '/main/getNews';
            console.log($scope.url);
            $http({method: $scope.method, url: $scope.url, cache: $templateCache, data: $scope.dataToPost}).
            success(function(data, status) {
                $scope.status = status;
                $scope.datas = data;
                $scope.loading = false;


                console.log($scope.url);
                console.log($scope.dataToPost);
                console.log($scope.datas);
            }).
            error(function(data, status) {
                $scope.datas = data || "Request failed";
                $scope.status = status;
            });
        };

    }]);


angModule.controller('AlertDemoCtrl', function ($scope) {
    $scope.alerts = [
    { type: 'danger', msg: 'Нет существует пользователя с таким логином и паролем.' }
    ];

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };
});

angModule.controller('AlertSuccessAfterCreate', function ($scope) {
    $scope.alerts = [
    { type: 'success', msg: 'Аттестационный центр успешно добавлен!' }
    ];

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };
});

angModule.controller('AlertSuccessAfterUserCreated', function ($scope) {
    $scope.alerts = [
    { type: 'success', msg: 'Пользователь успешно создан!' }
    ];

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };
});


angModule.controller('AlertSuccessAfterOrgCreated', function ($scope) {
    $scope.alerts = [
    { type: 'success', msg: 'Организация успешно добавлена!' }
    ];

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };
});

 

angModule
.controller('ModalOrganizationsCtrl', function ($scope, $modal, $log) {

    $scope.items = ['item1', 'item2', 'item3'];

    $scope.open = function (size) {

        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrl',
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
});




angModule
.controller('ModalNews', function ($scope, $modal, $log) {

    $scope.items = ['item1', 'item2', 'item3'];

    $scope.open = function (size) {

        var modalInstance = $modal.open({
            templateUrl: 'myModalContent2.html',
            controller: 'ModalInstanceCtrl',
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
});
 
angModule
.controller('addNewsCtrl', 
    function($scope, $http, $templateCache) {

        $scope.orightml = '';
        $scope.htmlcontent = $scope.orightml;

        $scope.method = 'post';
        $scope.url = '/main/addNews';


        document.getElementById('uplodaingImage').onchange = function (e) {
            var addSomeImage;
            if ( this.files && this.files[0] ) {
                var FR= new FileReader();
                FR.onload = function(e) {
                    var victory = '<img src="' + e.target.result + '"/>';
                    $scope.htmlcontent = $scope.htmlcontent + victory;
                };
                FR.readAsDataURL( this.files[0] );

            }
        };

        $scope.addNewsAjax = function() { 
            $scope.code = null;
            $scope.response = null; 
            $scope.dataToPost = {'title':$scope.titlenews, 'text': $scope.htmlcontent};
            $http({method: $scope.method, url: $scope.url, cache: $templateCache, data: $scope.dataToPost}).
            success(function(data, status) {
                console.log($scope.dataToPost);  
                console.log(data);
                console.log(data.success);
//                $scope.addAlert(data);
            }).
            error(function(data, status) {
                $scope.datas = data || "Request failed";
                $scope.status = status;
            });
        }; 
    }); 
 



// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

angModule
.controller('ModalInstanceCtrl', function ($scope, $modalInstance, items) {

    $scope.items = items;
    $scope.selected = {
        item: $scope.items[0]
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});


angModule
.controller('ModalInstanceCtrlForCenters', function ($http, $templateCache, $scope, $modalInstance, items, alertsManager) {

    $scope.method = 'post';

    $scope.addCentersAjax = function() {
        $scope.code = null;
        $scope.response = null;

        $scope.dataToPost = {
            'name':$scope.score1,
            'phone': $scope.score2,
            'adress': $scope.score3,
            'lon': $scope.score4,
            'lat': $scope.score5,
            'city': $scope.city,
            'weldersCode': $scope.score7 == undefined ? '': $scope.score7,
            'techCode': $scope.score9 == undefined ? '': $scope.score9,
            'equipCode': $scope.score11 == undefined ? '': $scope.score11
        };

        console.log($scope.dataToPost);

        $http({method: $scope.method, url: $scope.url = '/certification/addCenter', cache: $templateCache, data: $scope.dataToPost}).
        success(function(data, status) {

            if (data.success == false){
                alertsManager.addAlert('danger', data.errorMessage);
            } else {
                $scope.cancel();
            }
            console.log(data);
            // document.location.reload();
        }).
        error(function(data, status) {
            $scope.datas = data || "Request failed";
            $scope.status = status;
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});


angModule
.controller('ModalInstanceCtrlForCentersUpdate', function ($http, $templateCache, $scope, $modalInstance, items) {

    $scope.method = 'post';

    console.log(items);
    $scope.score1 = items.name;
    $scope.score2 = items.phone;
    $scope.score3 = items.adress;
    $scope.score4 = items.lat;
    $scope.score5 = items.lon;
    $scope.city   = items.city;

    if (items.weldersCode != ''){
        $scope.score6 = true;
        $scope.score7 = items.weldersCode;
    }

    if (items.equipCode != ''){
        $scope.score8 = true;
        $scope.score9 = items.equipCode;
    }

    if (items.techCode != ''){
        $scope.score10 = true;
        $scope.score11 = items.techCode;
    }

    $scope.addCentersAjax = function() {
        $scope.code = null;
        $scope.response = null;

        $scope.items = {
            'id':items.id,
            'name':$scope.score1,
            'phone': $scope.score2,
            'adress': $scope.score3,
            'lon': $scope.score4,
            'lat': $scope.score5,
            'city': $scope.city,
            'weldersCode': $scope.score6 == true ? $scope.score7: '',
            'equipCode': $scope.score8 == true ? $scope.score9: '',
            'techCode': $scope.score10 == true ? $scope.score11: ''
        };

        console.log($scope.items);

        $http({method: $scope.method, url: $scope.url = '/certification/updateCenter', cache: $templateCache, data: $scope.items}).
        success(function(data, status) {
            console.log(data);
        }).
        error(function(data, status) {
            $scope.datas = data || "Request failed";
            $scope.status = status;
        });

    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});


angModule
    .controller('ModalInstanceCtrlForNews', function ($http, $templateCache, $scope, $modalInstance, items, alertsManager) {

        $scope.method = 'post';

        $scope.addCentersAjax = function() {
            $scope.code = null;
            $scope.response = null;

            $scope.dataToPost = {
                'name':$scope.score1,
                'phone': $scope.score2,
                'adress': $scope.score3,
                'lon': $scope.score4,
                'lat': $scope.score5,
                'city': $scope.city,
                'weldersCode': $scope.score7 == undefined ? '': $scope.score7,
                'techCode': $scope.score9 == undefined ? '': $scope.score9,
                'equipCode': $scope.score11 == undefined ? '': $scope.score11
            };

            console.log($scope.dataToPost);

            $http({method: $scope.method, url: $scope.url = '/certification/addCenter', cache: $templateCache, data: $scope.dataToPost}).
                success(function(data, status) {

                    if (data.success == false){
                        alertsManager.addAlert('danger', data.errorMessage);
                    } else {
                        $scope.cancel();
                    }
                    console.log(data);
                    // document.location.reload();
                }).
                error(function(data, status) {
                    $scope.datas = data || "Request failed";
                    $scope.status = status;
                });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    });


angModule
    .controller('ModalInstanceCtrlForNewsUpdate', function ($http, $templateCache, $scope, $modalInstance, items) {

        $scope.method = 'post';
        $scope.titlenews = items.title;
        $scope.htmlcontent = items.text;

        $scope.updateAjaxNews = function() {
            $scope.code = null;
            $scope.response = null;

            $scope.dataToPost = {
                'id':items.id,
                'params':{
                    'title': $scope.titlenews,
                    'text': $scope.htmlcontent
                }
            };

            console.log($scope.dataToPost);
            $http({method: $scope.method, url: $scope.url = '/main/updateNews', cache: $templateCache, data: $scope.dataToPost}).
                success(function(data, status) {
                    console.log(data);
                }).
                error(function(data, status) {
                    $scope.datas = data || "Request failed";
                    $scope.status = status;
                });

        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    });


angModule
.controller('ModalAccept', function ($http, $templateCache, $scope, $modalInstance, items, name) {
    $scope.url = name;
    $scope.records = items;
    $scope.method = 'post';

    $scope.deleteFunction = function() {  
        $http({method: $scope.method, url: $scope.url, cache: $templateCache, data: {records: $scope.records}}).
        success(function(data, status) {
            $scope.status = status;
            $scope.datas = data;
            $modalInstance.close(false);
            console.log(data);
            console.log($scope.records);
        }).
        error(function(data, status) {
            $scope.datas = data || "Request failed";
            $scope.status = status;
        });    
    }; 

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}); 

angModule
.controller('ajaxCtrlCenters',
    function($scope, $http, $templateCache, $modal, $timeout) {
        $scope.method = 'post'; 
        $scope.interval = 50;
        $scope.code = null;
        $scope.response = null;

        var sort;

        $scope.maxSize = 50;
        $scope.bigTotalItems = 25;
        $scope.bigCurrentPage = 1;

        $scope.toStartDate = ($scope.bigCurrentPage-1) * $scope.interval;
        $scope.toEndDate = $scope.bigCurrentPage * $scope.interval;

        $scope.dataToPost = {'offset':$scope.interval, 'startPosition': $scope.toStartDate, 'city':$scope.input1item,'orgName': $scope.input2item,'code': $scope.input3item };
        $scope.loading = true;

        $scope.selection = {
            ids: {}
        };

        var inputChangedPromise;

        $scope.inputChange = function () {
            if(inputChangedPromise){
                $timeout.cancel(inputChangedPromise);
            }
            inputChangedPromise = $timeout(toTimeout,1000);
        };

        function toTimeout(){
            $scope.fetch();
        };

        var inputChangedPromise;
        $scope.open = function (size) {
            var modalInstance = $modal.open({
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrlForCenters',
                scope: $scope.fetch(),
                size: size,
                resolve: {
                    items: function () {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
                $scope.fetch();
            }, function () {
                $scope.fetch();
                console.log('Modal dismissed at: ' + new Date());
            });
        };

        $scope.toUpdate = function(size, items) {

            console.log(items);

            $scope.items = items;

            var modalInstance = $modal.open({
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrlForCentersUpdate',
                size: size,
                resolve: {
                    items: function () {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $scope.fetch();
                console.log('Modal updated at: ' + new Date());
            });
        };


        $scope.isDisabled = false;

        $scope.toDelete = function () {
            var jsonToObj = [];
            var key;
            var i = 0;
            for (key in $scope.selection.ids) {
                if ($scope.selection.ids[key] == true){
                    jsonToObj[i++] = key;
                }
            }
            $scope.isDisabled = true;
            $scope.records = jsonToObj;
            if ($scope.records.length==0){
                $scope.isDisabled = false;
            }
            console.log($scope.records);
        };


        $scope.toDeleteURL = function (size, items, name){
            $scope.code = null;
            $scope.response = null;

            $scope.items = $scope.records;
            $scope.deleteUrl = '/certification/deleteCenters';
            var modalInstance = $modal.open({
                templateUrl: 'ModalAccept.html',
                controller: 'ModalAccept',
                size: 'sm',
                resolve: {
                    items: function () {
                        return $scope.items;
                    },
                    name: function() {
                        return $scope.deleteUrl;
                    }
                }
            });

            modalInstance.result.then(function (statusCheck) {
                $scope.isDisabled = statusCheck;
                $scope.fetch();
            }, function () {
                $scope.fetch();
            });
        };

        $scope.emptyArr = false;

        $scope.fetch = function() {
            $scope.loading = true;
            $scope.dataToPost = {'offset':$scope.interval, 'startPosition': $scope.toStartDate, 'city':$scope.input1item,'orgName': $scope.input2item,'code': $scope.input3item };
            console.log($scope.dataToPost);
            $http({method: $scope.method, url: '/certification/getCenters', cache: $templateCache, data: $scope.dataToPost}).
            success(function(data, status) {
                $scope.loading = false;
                $scope.status = status;
                $scope.attestations = data;
                $scope.emptyArr = false;
                if (data.toString() == ''){
                    console.log('prokol');
                    $scope.emptyArr=true;
                }
                // console.log($scope.url);
                for (var i = $scope.attestations.length - 1; i >= 0; i--) {
                    if ($scope.attestations[i].isOnline == true){
                        $scope.attestations[i].isOnline = 'Online';
                    } else {
                        $scope.attestations[i].isOnline = 'Offline';
                    }
                };
                console.log($scope.attestations);
                // console.log($scope.dataToPost);
            }).
            error(function(data, status) {
                $scope.datas = data || "Request failed";
                $scope.status = status;
            });
        };

        $scope.getCount = function() {
            $scope.code = null;
            $scope.response = null;
            $http({method: $scope.method, url: '/certification/getCentersCount', cache: $templateCache, data: $scope.dataToPost}).
            success(function(data, status) {
                $scope.status = status;
                $scope.bigTotalItems = data.count;
                console.log(data.count);
            }).
            error(function(data, status) {
                $scope.datas = data || "Request failed";
                $scope.status = status;
            });
        };
    });


angModule
    .controller('ajaxCtrlNews',
    function($scope, $http, $templateCache, $modal, $sce) {
        $scope.method = 'post';
        $scope.interval = 50;
        $scope.code = null;
        $scope.response = null;

        var sort;

        $scope.dataToPost = {'offset':0, 'count':0};
        $scope.loading = true;

        $scope.selection = {
            ids: {}
        };

        $scope.open = function (size) {
            var modalInstance = $modal.open({
                templateUrl: 'addNewsModal.html',
                controller: 'ModalInstanceCtrlForNews',
                scope: $scope.fetch(),
                size: size,
                resolve: {
                    items: function () {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $scope.fetch();
                console.log('Modal dismissed at: ' + new Date());
            });
        };

        $scope.toUpdate = function(size, items) {

            console.log(items);

            $scope.items = items;

            var modalInstance = $modal.open({
                templateUrl: 'redactorModal.html',
                controller: 'ModalInstanceCtrlForNewsUpdate',
                size: size,
                resolve: {
                    items: function () {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $scope.fetch();
            });
        };


        $scope.isDisabled = false;

        $scope.toDelete = function () {
            var jsonToObj = [];
            var key;
            var i = 0;
            for (key in $scope.selection.ids) {
                if ($scope.selection.ids[key] == true){
                    jsonToObj[i++] = key;
                }
            }
            $scope.isDisabled = true;
            $scope.records = jsonToObj;
            if ($scope.records.length==0){
                $scope.isDisabled = false;
            }
            console.log($scope.records);
        };


        $scope.toDeleteURL = function (size, items, name){
            $scope.code = null;
            $scope.response = null;

            $scope.items = $scope.records;
            $scope.deleteUrl = '/main/deleteNews';
            var modalInstance = $modal.open({
                templateUrl: 'ModalAccept.html',
                controller: 'ModalAccept',
                size: 'sm',
                resolve: {
                    items: function () {
                        return $scope.items;
                    },
                    name: function() {
                        return $scope.deleteUrl;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $scope.fetch();
                $scope.isDisabled = false;
            });
        };




        $scope.updateDate = function () {
            // $scope.bigTotalItems = date;
            $scope.method = 'post';
            $scope.url = '/main/getNewsCount';
            $scope.code = null;
            $scope.response = null;
            $http({method: $scope.method, url: $scope.url, cache: $templateCache}).
                success(function(data, status) {
                    $scope.status = status;
                    $scope.dataToPost.count = data.count;
                    $scope.fetch();
                    console.log($scope.dataToPost);
                }).
                error(function(data, status) {
                    $scope.datas = data || "Request failed";
                    $scope.status = status;
                });
             };


        $scope.fetch = function() {
            $scope.loading = true;
            $scope.code = null;
            $scope.response = null;
            $scope.url = '/main/getNews';
            console.log($scope.dataToPost);
            $http({method: $scope.method, url: $scope.url, cache: $templateCache, data: $scope.dataToPost}).
                success(function(data, status) {
                    $scope.status = status;
                    $scope.news = data;
                    $scope.loading = false;

                    $scope.news.htmlSafe = $sce.trustAsHtml($scope.news.html);

                    console.log($scope.datas);
                }).
                error(function(data, status) {
                    $scope.datas = data || "Request failed";
                    $scope.status = status;
            });
        };

        $scope.updateDate();
    });




angModule
.controller('ModalInstanceCtrlForDevice',
    function ($http, $templateCache, $scope, $modalInstance, items, alertsManager) {

        $scope.method = 'post';

        $scope.addDeviceAjax = function(id) {

            $scope.dataToPost = {
                'code':$scope.score1,
                'centerId': id,
            };

            console.log($scope.score1);

            $http({method: $scope.method, url: '/certification/addDevice', cache: $templateCache, data: $scope.dataToPost}).
            success(function(data, status) {

                if (data.success == false){
                    alertsManager.addAlert('danger', data.errorMessage);
                } else {
                    $scope.cancel();
                }
                console.log(data);
            }).
            error(function(data, status) {
                $scope.datas = data || "Request failed";
                $scope.status = status;
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    });


angModule
.controller('ModalInstanceCtrlForDeviceUpdate',
    function ($http, $templateCache, $scope, $modalInstance, items){

        $scope.method = 'post';

        console.log(items);
        $scope.score1 = items.code;

        $scope.addDeviceAjax = function(id) {

            $scope.items = {
                'code':$scope.score1,
                'centerId': id,
                'id': items.id,
            };

            console.log(id);


            $http({method: $scope.method, url: $scope.url = '/certification/updateDevice', cache: $templateCache, data: $scope.items})
            .success(function(data, status) {
                console.log($scope.items);
                console.log(data);
            })
            .error(function(data, status) {
                $scope.datas = data || "Request failed";
                $scope.status = status;
            });

        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    });

angModule
.controller('ajaxCtrlDevices',
    function ($scope, $http, $templateCache, $modal, $timeout)  {
        $scope.method = 'post';
        $scope.interval = 50;

        var sort;

        $scope.maxSize = 50;
        $scope.bigTotalItems = 25;
        $scope.bigCurrentPage = 1;
        $scope.loading = true;

        $scope.toStartDate = ($scope.bigCurrentPage-1) * $scope.interval;
        $scope.toEndDate = $scope.bigCurrentPage * $scope.interval;

        $scope.method = 'post';

        $scope.open = function (size) {

            var modalInstance = $modal.open({
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrlForDevice',
                size: size,
                resolve: {
                    items: function () {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $scope.fetch();
            });
        };



        $scope.toUpdate = function(size, items) {

            console.log(items);

            $scope.items = items;

            var modalInstance = $modal.open({
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrlForDeviceUpdate',
                size: size,
                resolve: {
                    items: function () {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $scope.fetch();
            });
        }

            // $scope.liveLabels = [
            //   {name:'ФИО члена комиссии', sortable:'2'},
            //   {name:'ФИО сварщика', sortable:'5'},
            //   {name:'Аттестационный пункт', sortable:'18'},
            // ];

        $scope.initId = function(id){
            $scope.startId = id;
        };


        $scope.emptyArr = false;

        $scope.fetch = function() {
            $scope.loading = true;
            $scope.dataToPost = {'offset':$scope.interval, 'startPosition': $scope.toStartDate, 'centerId': $scope.startId, 'code': $scope.toFindInResult};
            console.log($scope.url);
            $http({method: $scope.method, url: '/certification/getDevices', cache: $templateCache, data: $scope.dataToPost}).
            success(function(data, status) {
                $scope.loading = false;
                $scope.status = status;
                $scope.attestations = data;
                console.log($scope.attestations);
                console.log($scope.dataToPost);
                $scope.emptyArr = false;

                if (data.toString() == ''){
                    console.log('prokol');
                    $scope.emptyArr=true;
                }

                for (var i = $scope.attestations.length - 1; i >= 0; i--) {
                    if ($scope.attestations[i].isOnline == true){
                        $scope.attestations[i].isOnline = 'Online';
                        $scope.attestations[i].isOnlineMessage = 'Онлайн';
                    } else {
                        $scope.attestations[i].isOnline = 'Offline';
                        $scope.attestations[i].isOnlineMessage = 'Оффлайн';
                    }
                };
            }).
            error(function(data, status) {
                $scope.datas = data || "Request failed";
                $scope.status = status;
            });

        };

        var inputChangedPromise;

        $scope.inputChange = function () {
            if(inputChangedPromise){
                $timeout.cancel(inputChangedPromise);
            }
            inputChangedPromise = $timeout(toTimeout,1000);
        };

        function toTimeout(){
            $scope.fetch();
        };

        $scope.getCount = function(id) {
            $scope.code = null;
            $scope.response = null;
            $scope.centerId = {'centerId': id};
            console.log($scope.centerId);
            $http({method: $scope.method, url: '/certification/getDevicesCount', cache: $templateCache, data: $scope.centerId}).
            success(function(data, status) {
                $scope.status = status;
                $scope.bigTotalItems = data.count;
                console.log(data.count);
            }).
            error(function(data, status) {
                $scope.datas = data || "Request failed";
                $scope.status = status;
            });
        };

        $scope.selection = {
            ids: {}
        };



        $scope.isDisabled = false;

        $scope.toDelete = function () {
            var jsonToObj = [];
            var key;
            var i = 0;
            for (key in $scope.selection.ids) {
                if ($scope.selection.ids[key] == true){
                    jsonToObj[i++] = key;
                }
            }
            $scope.isDisabled = true;
            $scope.records = jsonToObj;
            if ($scope.records.length==0){
                $scope.isDisabled = false;
            }
            console.log($scope.records);
        };


        $scope.toDeleteURL = function (size, items, name){
            $scope.code = null;
            $scope.response = null;

            $scope.items = $scope.records;
            $scope.deleteUrl = '/certification/deleteDevices';
            var modalInstance = $modal.open({
                templateUrl: 'ModalAccept.html',
                controller: 'ModalAccept',
                size: 'sm',
                resolve: {
                    items: function () {
                        return $scope.items;
                    },
                    name: function() {
                        return $scope.deleteUrl;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $scope.fetch();
                $scope.isDisabled = false;
            });
        };


    })






angModule
.controller('ajaxCtrlPracticalResult',
    function($scope, $http, $templateCache) {
        $scope.method = 'post';
        $scope.interval = 50;

        $scope.maxSize = 50;
        $scope.bigTotalItems = 25;
        $scope.bigCurrentPage = 1;

        $scope.toStartDate = ($scope.bigCurrentPage-1) * $scope.interval;
        $scope.toEndDate = $scope.bigCurrentPage * $scope.interval;

        $scope.dataToPost = {'offset':$scope.interval, 'startPosition': $scope.toStartDate};
        $scope.loading = true;

        $scope.fetch = function(id) {
            $scope.loading = true;
            $scope.code = null;
            $scope.response = null;
            var varData = {'offset':$scope.interval, 'startPosition': $scope.toStartDate.toString(), 'pracId':id};
            console.log(varData);
            $http({method: $scope.method, url: '/data/getEquipPracData', cache: $templateCache, data: varData}).
            success(function(data, status) {
                $scope.loading = false;
                $scope.status = status;
                $scope.datas = data;
                console.log($scope.datas);
            }).
            error(function(data, status) {
                $scope.datas = data;
                $scope.status = status;
            });
        };

        $scope.getCount = function(id) {
            $scope.code = null;
            $scope.response = null;
            data = {'offset':$scope.interval, 'startPosition': $scope.toStartDate, 'pracId':id};
            $http({method: $scope.method, url: '/data/getEquipPracDataCount', cache: $templateCache, data: ''}).
            success(function(data, status) {
                $scope.status = status;
                $scope.bigTotalItems = data.count;
                console.log(data.count);
            }).
            error(function(data, status) {
                $scope.datas = data || "Request failed";
                $scope.status = status;
            });
        };
        console.log($scope.dataToPost);
    });






angModule
.controller('historyAjax',
    function($scope, $http, $templateCache, $modal) {
        $scope.method = 'post';
        $scope.url = '/certification/getAuthSessions';
        $scope.interval = 50;

        var sort;

        $scope.maxSize = 50;
        $scope.bigTotalItems = 25;
        $scope.bigCurrentPage = 1;

        $scope.toStartDate = ($scope.bigCurrentPage-1) * $scope.interval;
        $scope.toEndDate = $scope.bigCurrentPage * $scope.interval;

        $scope.loading = true;

        $scope.fetch = function() {
        $scope.loading = true;
        $scope.code = null;
        $scope.response = null;
        console.log($scope.url);
        $scope.dataToPost = {'offset':$scope.interval, 'startPosition': $scope.toStartDate, 'centerId' : $scope.centerId};
        $http({method: $scope.method, url: $scope.url, cache: $templateCache, data: $scope.dataToPost}).
            success(function(data, status) {
                $scope.loading = false;
                $scope.status = status;
                $scope.stors = data;
                console.log($scope.url);
                console.log($scope.stors);
                for (var i = $scope.stors.length - 1; i >= 0; i--) {
                    if ($scope.stors[i].device == 0){
                        $scope.stors[i].device = 'Компьютера';
                    } else {
                        $scope.stors[i].device = 'СПО';
                    }
                };

            }).
            error(function(data, status) {
                $scope.datas = data || "Request failed";
                $scope.status = status;
            });
        };


        $scope.pageChanged = function() {
            $scope.toStartDate = ($scope.bigCurrentPage-1) * $scope.interval;
            $scope.toEndDate = $scope.bigCurrentPage * $scope.interval;
            $scope.dataToPost = {'offset':$scope.interval, 'startPosition': $scope.toStartDate, 'centerId' : $scope.centerId};
            $scope.fetch();
        };

        $scope.getCount = function(id) {
            $scope.code = null;
            $scope.response = null;
            $scope.centerId = {'centerId': id};
            $http({method: $scope.method, url: '/certification/getAuthSessionsCount', cache: $templateCache, data: $scope.centerId}).
            success(function(data, status) {
                $scope.status = status;
                $scope.bigTotalItems = data.count;
                console.log(data.count);
            }).
            error(function(data, status) {
                $scope.datas = data || "Request failed";
                $scope.status = status;
            });
            $scope.centerId = id;
        }
    });



angModule
.controller('listAttensionCenterAjax',
    function($scope, $http, $templateCache, $modal) {
        $scope.method = 'post';
        $scope.url = '/certification/getAuthSessions';
        $scope.interval = 50;

        var sort;

        $scope.maxSize = 50;
        $scope.bigTotalItems = 25;
        $scope.bigCurrentPage = 1;

        $scope.toStartDate = ($scope.bigCurrentPage-1) * $scope.interval;
        $scope.toEndDate = $scope.bigCurrentPage * $scope.interval;

        $scope.loading = true;

            // $scope.liveLabels = [
            //   {name:'ФИО члена комиссии', sortable:'2'},
            //   {name:'ФИО сварщика', sortable:'5'},
            //   {name:'Аттестационный пункт', sortable:'18'},
            // ];

        $scope.fetch = function() {
            $scope.loading = true;
            $scope.code = null;
            $scope.response = null;
            console.log($scope.url);
            $scope.dataToPost = {'offset':$scope.interval, 'startPosition': $scope.toStartDate, 'centerId' : $scope.centerId};
            $http({method: $scope.method, url: $scope.url, cache: $templateCache, data: $scope.dataToPost}).
            success(function(data, status) {
                $scope.loading = false;
                $scope.status = status;
                $scope.stors = data;
                console.log($scope.url);
                console.log($scope.stors);
            }).
            error(function(data, status) {
                $scope.datas = data || "Request failed";
                $scope.status = status;
            });
        };

        $scope.getCount = function(id) {
            $scope.code = null;
            $scope.response = null;
            $scope.centerId = {'centerId': id};
            $http({method: $scope.method, url: '/certification/getAuthSessionsCount', cache: $templateCache, data: $scope.centerId}).
            success(function(data, status) {
                $scope.status = status;
                $scope.bigTotalItems = data.count;
                console.log(data.count);
            }).
            error(function(data, status) {
                $scope.datas = data || "Request failed";
                $scope.status = status;
            });
            $scope.centerId = id;
            console.log($scope.centerId);
        }
    })


angModule
.controller('ajaxCtrlWelderResult',
    function($scope, $http, $templateCache, $filter, $modal) {
        $scope.method = 'post';
        $scope.url = '/data/getParameters';
        $scope.interval = 50;

        var sort;

        $scope.maxSize = 50;
        $scope.bigTotalItems = 25;
        $scope.bigCurrentPage = 1;

        $scope.toStartDate = ($scope.bigCurrentPage-1) * $scope.interval;
        $scope.toEndDate = $scope.bigCurrentPage * $scope.interval;

        $scope.loading = true;

            // $scope.liveLabels = [
            //   {name:'ФИО члена комиссии', sortable:'2'},
            //   {name:'ФИО сварщика', sortable:'5'},
            //   {name:'Аттестационный пункт', sortable:'18'},
            // ];

        var orderBy = $filter('orderBy');

        $scope.order = function(predicate, reverse) {
          $scope.datas = orderBy($scope.datas, predicate, reverse);
          console.log('order');
        };

        $scope.fetch = function(id, type) {
            $scope.loading = true;
            $scope.code = null;
            $scope.response = null;
            console.log($scope.url);
            $scope.dataToPost = {'certId' : id, 'certType': type};
            console.log($scope.dataToPost);
            $http({method: $scope.method, url: $scope.url, cache: $templateCache, data: $scope.dataToPost}).
            success(function(data, status) {
                $scope.loading = false;
                $scope.status = status;
                $scope.datas = data;
                console.log($scope.url);
                console.log($scope.datas);
                $scope.order('-date',true);
            }).
            error(function(data, status) {
                $scope.datas = data || "Request failed";
                $scope.status = status;
            });
        };

        $scope.getCount = function(id) {
            $scope.code = null;
            $scope.response = null;
            $scope.centerId = {'centerId': id};
            $http({method: $scope.method, url: '/certification/getAuthSessionsCount', cache: $templateCache, data: $scope.centerId}).
            success(function(data, status) {
                $scope.status = status;
                $scope.bigTotalItems = data.count;
                console.log(data.count);
            }).
            error(function(data, status) {
                $scope.datas = data || "Request failed";
                $scope.status = status;
            });
            $scope.centerId = id;
            console.log($scope.centerId);
        }
    })





angModule.controller('chat',
    function($scope, $http, $templateCache, $modal) {
        $scope.method = 'post';
        $scope.interval = 5;
        $scope.loading = true;

        $scope.getMessage = function(id) {
            $scope.code = null;
            $scope.response = null;
            $scope.loading = true;

            $scope.dataToPost = {'userId': id, 'startPosition': 0, 'offset': 50};
            $http({method: $scope.method, url: '/messages/get' , cache: $templateCache, data: $scope.dataToPost}).
            success(function(data, status) {
                $scope.loading = false;
                $scope.status = status;
                $scope.messages = data;
                for (var i = $scope.messages.length - 1; i >= 0; i--) {
                    if ($scope.messages[i].isAdmin == 1) {
                        $scope.messages[i].name = 'admin'
                    };
                 };
                // console.log($scope.messages);
            }).
            error(function(data, status) {
                $scope.messages = data || "Request failed";
                $scope.status = status;
            });
        };

        $scope.getMessageId = function() {
            $scope.code = null;
            $scope.response = null;
            $scope.loading = true;

            $scope.dataToPost = {'userId': ezbrz, 'startPosition': 0, 'offset': 50};
            console.log($scope.dataToPost);
            $http({method: $scope.method, url: '/messages/get' , cache: $templateCache, data: $scope.dataToPost}).
            success(function(data, status) {
                $scope.status = status;
                $scope.loading = false;
                $scope.messages = data;
                for (var i = $scope.messages.length - 1; i >= 0; i--) {
                    if ($scope.messages[i].isAdmin == 1) {
                        $scope.messages[i].name = 'admin'
                    };
                 };
                console.log($scope.messages);
            }).
            error(function(data, status) {
                $scope.messages = data || "Request failed";
                $scope.status = status;
            });
        };

        var ezbrz;

        $scope.initId = function (arg) {
            ezbrz = arg;
        }

        $scope.getMessage2 = function (item) {
            $scope.code = null;
            $scope.response = null;
            $scope.loading = true;
            ezbrz = item;

            $scope.dataToPost = {'userId': item, 'startPosition': 0, 'offset': 50};
            console.log($scope.dataToPost);
            $http({method: $scope.method, url: '/messages/get' , cache: $templateCache, data: $scope.dataToPost}).
            success(function(data, status) {
                $scope.status = status;
                $scope.loading = false;
                $scope.messages = data;
                for (var i = $scope.messages.length - 1; i >= 0; i--) {
                    if ($scope.messages[i].isAdmin == 1) {
                        $scope.messages[i].name = 'admin'
                    };
                 }; 
                console.log($scope.dataToPost.isAdmin );
                console.log($scope.messages);
            }).
            error(function(data, status) {
                $scope.messages = data || "Request failed";
                $scope.status = status;
            });

        }

        $scope.sendMessage = function (id) {
            console.log($scope.ta_model);
            $scope.code = null;
            $scope.response = null; 
            $scope.loading = true;

            $scope.dataToPost = {'userId': id, 'message': $scope.ta_model, 'isAdmin': 0}; 
            $http({method: $scope.method, url: '/messages/send' , cache: $templateCache, data: $scope.dataToPost}).
            success(function(data, status) {
                $scope.loading = false;
                $scope.status = status;
                $scope.sendMessageBody = data; 

                for (var i = $scope.messages.length - 1; i >= 0; i--) {
                    $scope.messages[i+1] = $scope.messages[i];
                };

                $scope.messages[0] = $scope.sendMessageBody;

                // console.log($scope.messages);
                // console.log($scope.messages.length);
                // console.log($scope.sendMessage);
                $scope.ta_model = '';
                // $scope.getMessage(id);
            }).
            error(function(data, status) {
                $scope.messages = data || "Request failed";
                $scope.status = status;
            });
        };

        $scope.sendMessageFromAdmin = function () {
            console.log($scope.ta_model);
            $scope.code = null;
            $scope.response = null; 
            $scope.loading = true;

            $scope.dataToPost = {'userId': ezbrz, 'message': $scope.ta_model, 'isAdmin': 1};

            $http({method: $scope.method, url: '/messages/send' , cache: $templateCache, data: $scope.dataToPost}).
            success(function(data, status) {
                $scope.loading = false;
                $scope.status = status;
                $scope.messages = data; 
                $scope.ta_model = '';
                $scope.getMessageId($scope.userId);
                console.log($scope.messages);
            }).
            error(function(data, status) {
                $scope.messages = data || "Request failed";
                $scope.status = status;

            });
        };

        $scope.getDialogs = function (){ 
            $scope.code = null;
            $scope.response = null; 
            $scope.loading = true;
 
            $http({method: $scope.method, url: '/messages/getDialogs' , cache: $templateCache, data: $scope.dataToPost}).
            success(function(data, status) {
                $scope.loading = false;
                $scope.status = status;
                $scope.dialogs = data; 
 
                for (var i = $scope.dialogs.length - 1; i >= 0; i--) {
                    if ($scope.dialogs[i].name == null) {
                        $scope.dialogs[i].name = 'nameProblem'
                    };
                };

                console.log($scope.dialogs);
            }).
            error(function(data, status) {
                $scope.messages = data || "Request failed";
                $scope.status = status;
            });
        } 

});



function chartCtrl($scope) {



    $scope.toVACVar = function(data, nameLabel) {
        $scope.data = data;
        $scope.nameLabel = nameLabel;
        console.log($scope.data);

    };

    // $scope.data = [
    //   {x: 0.6, value: 4},
    //   {x: 1, value: 8},
    //   {x: 2, value: 15},
    //   {x: 3, value: 16},
    //   {x: 4, value: 23},
    //   {x: 5, value: 42}
    // ];

    $scope.options = {
        axes: {
            x: {key: 'x', labelFunction: function(value) {return value;}, type: 'linear'},
            y: {type: 'linear', min: 0},
        },
        series: [
            {y: 'value', color: 'steelblue', thickness: '2px', type: '', dotSize: 4},
        ],
        lineMode: 'linear',
        tension: 0.7,
        tooltip: {mode: 'scrubber'},
        drawLegend: false,
        drawDots: true,
        columnsHGap: 5
    }

}

