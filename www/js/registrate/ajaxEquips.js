

angModule
    .controller('ajaxEquips',
    function($scope, $http, $templateCache, $timeout, $modal, $location)  {
        $scope.method = 'post';

        var sort;
        var oldPage = 1;

        $scope.maxSize = 50;
        $scope.bigTotalItems = 25;
        $scope.bigCurrentPage = 1;
        $scope.toStartDate = 0;
        $scope.interval = 50;

        $scope.loading = true;

        $scope.sI1i = null;
        $scope.sI2i = null;
        $scope.sI3i = null;
        $scope.sI4i = null;
        $scope.sI5i = null;
        $scope.sI7i = null;
        $scope.sI8i = null;
        $scope.dataEnd = new Date();
        $scope.dataFrom = new Date();

        $scope.toDescModel = true;

        $scope.liveLabels = [
            {nameEng: 'userName', name:'ФИО члена комиссии', sortable:'2'},
            {nameEng: 'orgName', name:'Название организации', sortable:'18'},
            {nameEng: 'tradeMark', name:'Марка оборудования', sortable:'6'},
            {nameEng: 'factoryNum', name:'Заводской №', sortable:'20'},
            {nameEng: 'invNum', name:'Инвертарный №', sortable:'11'},
            {nameEng: 'programName', name:'Номер программы', sortable:'8'},
            {nameEng: 'weldMethod', name:'Шифр способа', sortable:'19'}
        ];

        $scope.sort = $scope.liveLabels[5];

        $scope.sortingFunc = function(){


            $scope.toStartDate = ($scope.bigCurrentPage-1) * $scope.interval;
            $scope.toEndDate = $scope.bigCurrentPage * $scope.interval;

            var filterJson = {
                'userName':$scope.sI1i,
                'orgName':$scope.sI3i,
                'tradeMark':$scope.sI4i,
                'factoryNum':$scope.sI5i,
                'invNum':$scope.sI6i,
                'programName':$scope.sI7i,
                'weldMethod':$scope.sI8i
            };
            str = {
                'offset':'50',
                'startPosition':$scope.toStartDate.toString(),
                'orderBy': $scope.sort.nameEng,
                'order':$scope.toDescModel,
                'toDescModel':$scope.toDescModel,
                'userName':$scope.sI1i,
                'orgName':$scope.sI3i,
                'tradeMark':$scope.sI4i,
                'factoryNum':$scope.sI5i,
                'invNum':$scope.sI6i,
                'programName':$scope.sI7i,
                'weldMethod':$scope.sI8i,
                'centerId':$scope.centerId
            };


            $scope.dataJson = {'offset':'50', 'startPosition':$scope.toStartDate.toString(), 'orderBy': $scope.sort.nameEng, 'order':$scope.toDescModel.toString(), 'toDataModel':$scope.toDataModel, 'filter':filterJson, 'centerId':$scope.centerId};

            console.log(str);
            $location.search(str);

            console.log($scope.toDataModel);

            var EndMonth=$scope.dataEnd.getMonth()+1;
            var FromMonth=$scope.dataFrom.getMonth()+1;
            var filterJson = {
                'userName':$scope.sI1i,
                'orgName':$scope.sI3i,
                'tradeMark':$scope.sI4i,
                'factoryNum':$scope.sI5i,
                'invNum':$scope.sI6i,
                'programName':$scope.sI7i,
                'weldMethod':$scope.sI8i
            };

            $scope.dataJson = {'offset':'50', 'startPosition':$scope.toStartDate.toString(), 'orderBy': $scope.sort.nameEng, 'order':$scope.toDescModel.toString(), 'filter':filterJson, 'centerId':$scope.centerId};
            $scope.fetch();

        };


        function searchToObject() {
            var pairs = window.location.hash.substring(2).split("&"),
                obj = {},
                pair,
                i;

            for ( i in pairs ) {
                if ( pairs[i] === "" ) continue;

                pair = pairs[i].split("=");
                obj[ decodeURIComponent( pair[0] ) ] = decodeURIComponent( pair[1] );
            }

            return obj;
        }

        $scope.getCId = function (id) {
//            $location.search = url;
            $scope.centerId = id;
            console.log(window.location.hash.length);
            if (window.location.hash.length != 0){
                var jsonString = searchToObject();
                console.log(jsonString.orderBy);
                $scope.sI1i = jsonString.userName || null;
                $scope.sI3i = jsonString.orgName || null;
                $scope.sI4i = jsonString.tradeMark || null;
                $scope.sI5i = jsonString.factoryNum || null; 
                $scope.sI6i = jsonString.invNum || null;
                $scope.sI7i = jsonString.programName || null;
                $scope.sI8i = jsonString.weldMethod || null;
                $scope.dataEnd = jsonString.endDate||new Date();
                $scope.dataFrom = jsonString.startDate||new Date();
                $scope.toDataModel = jsonString.toDataModel||null;
                $scope.toDescModel = jsonString.toDescModel || null;

                for (i in $scope.liveLabels){
                    if ($scope.liveLabels[i].nameEng==jsonString.orderBy){
                        $scope.sort = $scope.liveLabels[i] || null;
                    }
                }

                $scope.getCount($scope.centerId);
                $scope.sortingFunc();
            } else {
                $scope.dataJson = {'offset':'50', 'startPosition':$scope.toStartDate.toString(), 'centerId':$scope.centerId, 'orderBy': 'userName', 'order':$scope.toDescModel};
                $scope.getCount($scope.centerId);
                $scope.sortingFunc();
            }
        };

        $scope.selection = {
            ids: {}
        };

        $scope.isDisabled = false;

        $scope.toDelete = function () {
            var jsonToObj = [];
            var key;
            var i = 0;
            for (key in $scope.selection.ids) {
                if ($scope.selection.ids[key] == true){
                    jsonToObj[i++] = key;
                }
            }
            $scope.isDisabled = true;
            $scope.records = jsonToObj;
            if ($scope.records.length==0){
                $scope.isDisabled = false;
            }
            console.log($scope.records);
        };


        $scope.toDeleteURL = function (size, items, name){
            $scope.code = null;
            $scope.response = null;

            $scope.items = $scope.records;
            $scope.deleteUrl = '/data/deleteEquipData';
            var modalInstance = $modal.open({
                templateUrl: 'ModalAccept.html',
                controller: 'ModalAccept',
                size: 'sm',
                resolve: {
                    items: function () {
                        return $scope.items;
                    },
                    name: function() {
                        return $scope.deleteUrl;
                    }
                }
            });


            modalInstance.result.then(function (statusCheck) {
                $scope.isDisabled = statusCheck;
                location.reload();
            }, function () {
                $scope.fetch();
            });
        };

        var inputChangedPromise;

        $scope.sortable = function () {
            if(inputChangedPromise){
                $timeout.cancel(inputChangedPromise);
            }
            inputChangedPromise = $timeout(toTimeout,1000);
        };


        function toTimeout() {
            $scope.sortingFunc();
        }

        $scope.refresh = function(){
            $scope.sI1i = null;
            $scope.sI2i = null;
            $scope.sI3i = null;
            $scope.sI4i = null;
            $scope.sI5i = null;
            $scope.sI7i = null;
            $scope.sI8i = null;

            $scope.toDescModel = true;
            $scope.sort = false;
            $scope.sortingFunc();
        };

        $scope.toDesc = function () {
            $scope.sortingFunc();
        };

        $scope.pageChanged = function() {
            $scope.sortingFunc();
        };

        $scope.timeDataToCtrl = function (){
            $scope.sortingFunc();
        };

        $scope.emptyArr = false;

        $scope.fetch = function() {
            $scope.loading = true;
            $scope.code = null;
            $scope.response = null;
            console.log($scope.dataJson);

            $http({method: $scope.method, url: '/data/getEquipData', cache: $templateCache, data: $scope.dataJson}).
                success(function(data, status) {
                    $scope.loading = false;
                    $scope.status = status;
                    $scope.datas = data;
                    $scope.emptyArr = false;
                    if (data.toString() == ''){
                        console.log('prokol');
                        $scope.emptyArr=true;
                    }
                    console.log(data);
                    console.log($scope.dataJson);
                }).
                error(function(data, status) {
                    $scope.datas = data || "Request failed";
                    $scope.status = status;
                });
        };



        $scope.getCount = function (id) {
            $scope.code = null;
            $scope.response = null;
            $scope.dataJson = {'offset':'50', 'startPosition':$scope.toStartDate.toString(), 'centerId':id, 'orderBy': 'userName'};
            var cid = {'centerId': id};
            $http({method: $scope.method, url: '/data/getEquipDataCount', cache: $templateCache, data: cid}).
                success(function(data, status) {
                    $scope.status = status;
                    $scope.bigTotalItems = data.count;
                    console.log(data.count);
                }).
                error(function(data, status) {
                    $scope.datas = data || "Request failed";
                    $scope.status = status;
                });
        }
    });

