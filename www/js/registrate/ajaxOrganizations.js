
angModule
.controller('ajaxCtrlOrganizations',
function ($scope, $http, $templateCache, $modal, $timeout)  {
    $scope.method = 'post';
    $scope.interval = 50;

    var sort;

    $scope.maxSize = 50;
    $scope.bigTotalItems = 25;
    $scope.bigCurrentPage = 1;
    $scope.loading = true;

    $scope.toStartDate = ($scope.bigCurrentPage-1) * $scope.interval;
    $scope.toEndDate = $scope.bigCurrentPage * $scope.interval;

    $scope.method = 'post';

    $scope.open = function (size) {

        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrlForOrganization',
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            $scope.fetch();
        });
    };



    $scope.toUpdate = function(size, items) {

        console.log(items);

        $scope.items = items;

        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrlForOrganizationUpdate',
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            $scope.fetch();
        });
    };

    // $scope.liveLabels = [
    //   {name:'ФИО члена комиссии', sortable:'2'},
    //   {name:'ФИО сварщика', sortable:'5'},
    //   {name:'Аттестационный пункт', sortable:'18'},
    // ];

    $scope.initId = function(id){
        $scope.startId = id;
    };
    $scope.toFindInResult = null;
    $scope.toFindInResult2 = null;


    $scope.emptyArr = false;

    $scope.fetch = function() {
        $scope.loading = true;
        $scope.dataToPost = {'centerId':$scope.startId.toString(), 'offset':$scope.interval, 'startPosition': $scope.toStartDate, 'name': $scope.toFindInResult, 'inn': $scope.toFindInResult2};
        console.log($scope.dataToPost);
        $http({method: $scope.method, url: '/certification/getOrganizations', cache: $templateCache, data: $scope.dataToPost}).
            success(function(data, status) {
                $scope.loading = false;
                $scope.status = status;
                $scope.attestations = data;
                $scope.emptyArr = false;

                if (data.toString() == ''){
                    console.log('prokol');
                    $scope.emptyArr=true;
                }
                console.log($scope.attestations);
                console.log($scope.dataToPost);
                for (var i = $scope.attestations.length - 1; i >= 0; i--) {
                    if ($scope.attestations[i].isOnline == true){
                        $scope.attestations[i].isOnline = 'Online';
                        $scope.attestations[i].isOnlineMessage = 'Онлайн';
                    } else {
                        $scope.attestations[i].isOnline = 'Offline';
                        $scope.attestations[i].isOnlineMessage = 'Оффлайн';
                    }
                };
            }).
            error(function(data, status) {
                $scope.datas = data || "Request failed";
                $scope.status = status;
            });
    };

    var inputChangedPromise;

    $scope.inputChange = function () {
        if(inputChangedPromise){
            $timeout.cancel(inputChangedPromise);
        }
        inputChangedPromise = $timeout(toTimeout,1000);
    };

    function toTimeout(){
        $scope.fetch();
    };

    $scope.inputChange = function () {
        if(inputChangedPromise){
            $timeout.cancel(inputChangedPromise);
        }
        inputChangedPromise = $timeout(toTimeout,1000);
    };

    function toTimeout(){
        $scope.fetch();
    };

    $scope.getCount = function(cID) {
        $scope.code = null;
        $scope.response = null;
        $http({method: $scope.method, url: '/certification/getOrganizationsCount', cache: $templateCache, data: {centerId: cID}}).
            success(function(data, status) {
                $scope.status = status;
                $scope.bigTotalItems = data.count;
                console.log(data.count);
            }).
            error(function(data, status) {
                $scope.datas = data || "Request failed";
                $scope.status = status;
            });
    };

    $scope.selection = {
        ids: {}
    };

    $scope.isDisabled = false;

    $scope.toDelete = function () {
        var jsonToObj = [];
        var key;
        var i = 0;
        for (key in $scope.selection.ids) {
            if ($scope.selection.ids[key] == true){
                jsonToObj[i++] = key;
            }
        }
        $scope.isDisabled = true;
        $scope.records = jsonToObj;
        if ($scope.records.length==0){
            $scope.isDisabled = false;
        }
        console.log($scope.records);
    };


    $scope.toDeleteURL = function (size, items, name){
        $scope.code = null;
        $scope.response = null;

        $scope.items = $scope.records;
        $scope.deleteUrl = '/certification/deleteOrganizations';
        var modalInstance = $modal.open({
            templateUrl: 'ModalAccept.html',
            controller: 'ModalAccept',
            size: 'sm',
            resolve: {
                items: function () {
                    return $scope.items;
                },
                name: function() {
                    return $scope.deleteUrl;
                }
            }
        });

        modalInstance.result.then(function (statusCheck) {
            $scope.isDisabled = statusCheck;
            location.reload();
        }, function () {
            $scope.fetch();
        });
    };

});


angModule
.controller('ModalInstanceCtrlForOrganization',
function ($http, $templateCache, $scope, $modalInstance, alertsManager, items) {

    $scope.method = 'post';

    $scope.addOrganizationAjax = function(id) {
        $scope.dataToPost = {
            'name':$scope.score1,
            'inn':$scope.score4,
            'adress':$scope.score2,
            'phone':$scope.score3,
            'centerId': id,
        };

        console.log($scope.dataToPost);

        if ($scope.score1==null ||$scope.score2==null ||$scope.score3==null ||$scope.score4==null){
            alertsManager.addAlert('danger', 'Не все поля формы заполнены');
        } else {
            console.log($scope.dataToPost);
            $http({method: $scope.method, url: '/certification/addOrganization', cache: $templateCache, data: $scope.dataToPost}).
                success(function(data, status) {
                    if (data.success == false){
                        alertsManager.addAlert('danger', data.errorMessage);
                    } else {
                        alertsManager.addAlert('success', 'Успешно добавлено');
                        $scope.cancel();
                    }
                }).
                error(function(data, status) {
                    $scope.datas = data || "Request failed";
                    $scope.status = status;
                })
        };
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

});


angModule
.controller('ModalInstanceCtrlForOrganizationUpdate'
,function ($http, $templateCache, $scope, $modalInstance, items){
 
    $scope.method = 'post';

    console.log(items);
    $scope.score1 = items.name;
    $scope.score2 = items.inn;
    $scope.score3 = items.adress;
    $scope.score4 = items.phone;

    $scope.addOrganizationAjax = function(id) {

        $scope.items = {
            'name':$scope.score1,
            'inn':$scope.score2,
            'adress':$scope.score3,
            'phone':$scope.score4,
            'centerId': id,
            'id': items.id,
        };

        $http({method: $scope.method, url: $scope.url = '/certification/updateOrganization', cache: $templateCache, data: $scope.items})
            .success(function(data, status) {
                if (data.success == false){
                    alertsManager.addAlert('danger', data.errorMessage);
                } else {
                    $scope.cancel();
                }
                console.log($scope.items);
                console.log(data);
            })
            .error(function(data, status) {
                $scope.datas = data || "Request failed";
                $scope.status = status;
            });

    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

});
