angModule
.controller('ModalInstanceCtrlForUsers'
,function ($http, $templateCache, $scope, $modalInstance, items, alertsManager) {

    $scope.method = 'post';

    $scope.addUserAjax = function(id) {
        $scope.code = null;
        $scope.response = null;

        $scope.dataToPost = {
            'name': $scope.score1,
            'accessLevel': $scope.score4,
            'login': $scope.score2,
            'password': $scope.score3,
            'centerId': id,
            'welders': $scope.score6 == undefined ? false: true,
            'tech': $scope.score8 == undefined ? false: true,
            'equip': $scope.score10 == undefined ? false: true
        };

        if ($scope.score1==null ||$scope.score2==null ||$scope.score3==null ||$scope.score4==null){
            alertsManager.addAlert('danger', 'Не все поля формы заполнены');
        } else {
            console.log($scope.dataToPost);
            $http({method: $scope.method, url: '/certification/addUser', cache: $templateCache, data: $scope.dataToPost}).
                success(function(data, status) {
                    console.log(data);
                    if (data.success == false){
                        alertsManager.addAlert('danger', 'Логин занят');
                    } else {
                        $scope.cancel();
                    }
                }).
                error(function(data, status) {
                    $scope.datas = data || "Request failed";
                    $scope.status = status;
                })
        };
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

});


angModule
.controller('ModalInstanceCtrlForUsersUpdate'
,function ($http, $templateCache, $scope, $modalInstance, items){

    $scope.method = 'post';

    console.log(items);
    $scope.score1 = items.name;
    $scope.score2 = items.login;
    $scope.score3 = items.password;
    $scope.score4 = parseInt(items.level);
    $scope.city   = items.city;

    if (items.welders == true){
        $scope.score6 = true;
    }

    if (items.tech == true){
        $scope.score8 = true;
    }

    if (items.equip == true){
        $scope.score10 = true;
    }

    $scope.addUserAjax = function(id) {

        $scope.items = {
            'name':$scope.score1,
            'login': $scope.score2,
            'password': $scope.score3,
            'level': $scope.score4,
            'centerId': id,
            'id': items.id,
            'welders': $scope.score6,
            'tech': $scope.score8,
            'equip': $scope.score10
        };

        console.log(items);
        console.log($scope.items);


        $http({method: $scope.method, url: $scope.url = '/certification/updateUser', cache: $templateCache, data: $scope.items})
            .success(function(data, status) {
                console.log($scope.items);
                console.log(data);
                $modalInstance.dismiss('cancel');
            })
            .error(function(data, status) {
                $scope.datas = data || "Request failed";
                $scope.status = status;
            });

    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

});

angModule
.controller('ajaxCtrlUsers'
,function($scope, $http, $templateCache, $modal, $timeout) {
    var sort;
    var attest;
    $scope.method = 'post';
    $scope.interval = 50;
    $scope.loading = true;

    $scope.maxSize = 50;
    $scope.bigTotalItems = 25;
    $scope.bigCurrentPage = 1;

    $scope.toStartDate = ($scope.bigCurrentPage-1) * $scope.interval;
    $scope.toEndDate = $scope.bigCurrentPage * $scope.interval;

    $scope.toFindInResult = '';

    $scope.selection = {
        ids: {}
    };

    $scope.getLogin = function (id) {
        $scope.levelGet = id;
    };

    $scope.initId = function(id){
        $scope.startId = id;
    };

    $scope.toUpdate = function(size, items) {

        console.log(items);

        $scope.items = items;

        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrlForUsersUpdate',
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            $scope.fetch();
        });
    };

    $scope.open = function (size, obj) {

        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrlForUsers',
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            $scope.fetch();
            console.log('Modal dismissed at: ' + new Date());
        });
    };


    $scope.isDisabled = false;

    $scope.toDelete = function () {

        var jsonToObj = [];
        var key;
        var i = 0;


        for (key in $scope.selection.ids) {
            console.log($scope.selection.ids[key]);
            for(var j=0; j<attest.length;j++){
                if ($scope.selection.ids[key] != attest[j].isActive){
                    jsonToObj[i++] = key;
                    break;
                }
            }
        }

        $scope.isDisabled = true;
        $scope.records = jsonToObj;

        if ($scope.records.length==0){
            $scope.isDisabled = false;
        }

        console.log($scope.records);
    };

    $scope.toDeleteURL = function (size, items, name){
        $scope.code = null;
        $scope.response = null;

        $scope.items = $scope.records;
        $scope.deleteUrl = '/certification/deleteUsers';
        var modalInstance = $modal.open({
            templateUrl: 'ModalAccept.html',
            controller: 'ModalAccept',
            size: 'sm',
            resolve: {
                items: function () {
                    return $scope.items;
                },
                name: function() {
                    return $scope.deleteUrl;
                }
            }
        });


        modalInstance.result.then(function (statusCheck) {
            $scope.isDisabled = statusCheck;
            location.reload();
        }, function () {
            $scope.fetch();
        });
    };

    $scope.fetch = function() {
        $scope.loading = true;
        $scope.code = null;
        $scope.response = null;
        $scope.dataToPost = {'offset':$scope.interval, 'startPosition': $scope.toStartDate, 'centerId': $scope.startId, 'loginInfo': $scope.levelGet, 'userName': $scope.toFindInResult};


        console.log($scope.dataToPost);
        $http({method: $scope.method, url:'/certification/getUsersByRights', cache: $templateCache, data: $scope.dataToPost}).
            success(function(data, status) {
                $scope.loading = false;
                $scope.status = status;
                $scope.attestations = data;
                console.log(data);

                for (var i = $scope.attestations.length - 1; i >= 0; i--) {
                    if ($scope.attestations[i].isOnline == true){
                        $scope.attestations[i].isOnline = 'Online';
                        $scope.attestations[i].isOnlineMessage = 'Онлайн';
                    } else {
                        $scope.attestations[i].isOnline = 'Offline';
                        $scope.attestations[i].isOnlineMessage = 'Оффлайн';
                    }
                };

                for (var i = $scope.attestations.length - 1; i >= 0; i--) {
                    if ($scope.attestations[i].isActive == 1){
                        $scope.attestations[i].isActive = false;
                    } else {
                        $scope.attestations[i].isActive = true;
                    }
                };

                attest = $scope.attestations;
            }).
            error(function(data, status) {
                $scope.datas = data || "Request failed";
                $scope.status = status;
            });

    };

    var inputChangedPromise;

    $scope.inputChange = function () {
        if(inputChangedPromise){
            $timeout.cancel(inputChangedPromise);
        }
        inputChangedPromise = $timeout(toTimeout,1000);
    };

    function toTimeout(){
        $scope.fetch();
    };

    $scope.pageChanged = function() {
        $scope.toStartDate = ($scope.bigCurrentPage-1) * $scope.interval;
        $scope.toEndDate = $scope.bigCurrentPage * $scope.interval;
        $scope.dataToPost = {'offset': $scope.interval, 'startPosition': $scope.toStartDate, 'centerId' : $scope.centerId};
        $scope.fetch();
    };

    $scope.getCount = function(id) {
        $scope.code = null;
        $scope.response = null;
        $scope.centerId = {'centerId': id};
        $http({method: $scope.method, url: '/certification/getUsersCount', cache: $templateCache, data: $scope.centerId}).
            success(function(data, status) {
                $scope.status = status;
                $scope.bigTotalItems = data.count;
                console.log(data.count);
            }).
            error(function(data, status) {
                $scope.datas = data || "Request failed";
                $scope.status = status;
            });
    };
});

