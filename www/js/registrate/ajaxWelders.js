
angModule
    .controller('ajaxWelders',
    function($scope, $http, $templateCache, $timeout, $modal, $location) {

        $scope.method = 'post';


        $scope.sI1i = null;
        $scope.sI2i = null;
        $scope.sI3i = null;
        $scope.sI4i = null;
        $scope.sI5i = null;
        $scope.sI7i = null;
        $scope.sI8i = null;
        $scope.dataEnd = new Date();
        $scope.dataFrom = new Date();


        $scope.maxSize = 50;
        $scope.bigTotalItems = 25;
        $scope.bigCurrentPage = 1;
        $scope.interval = 50;
        $scope.toStartDate = 0;

        $scope.toDescModel = true;
        $scope.toDataModel = 'disabled';
        $scope.loading = true;

        $scope.liveLabels = [
            {nameEng: 'userName',name:'ФИО члена комиссии', sortable:'2'},
            {nameEng: 'workerName',name:'ФИО сварщика', sortable:'5'},
            {nameEng: 'punktName',name:'Аттестационный пункт', sortable:'18'},
            {nameEng: 'workerStump',name:'Клеймо', sortable:'6'},
            {nameEng: 'weldMethod',name:'Шифр способа', sortable:'20'},
            {nameEng: 'material',name:'Материал КСС', sortable:'21'},
            {nameEng: 'techCardNum',name:'Тех. карта №', sortable:'19'}
        ];


        $scope.sort = $scope.liveLabels[3];

        $scope.selection = {
            ids: {}
        };

        $scope.isDisabled = false;

        $scope.toDelete = function () {
            var jsonToObj = [];
            var key;
            var i = 0;
            for (key in $scope.selection.ids) {
                if ($scope.selection.ids[key] == true){
                    jsonToObj[i++] = key;
                }
            }
            $scope.isDisabled = true;
            $scope.records = jsonToObj;
            if ($scope.records.length==0){
                $scope.isDisabled = false;
            }
            console.log($scope.records);
        };


        function searchToObject() {
            var pairs = window.location.hash.substring(2).split("&"),
                obj = {},
                pair,
                i;

            for ( i in pairs ) {
                if ( pairs[i] === "" ) continue;

                pair = pairs[i].split("=");
                obj[ decodeURIComponent( pair[0] ) ] = decodeURIComponent( pair[1] );
            }

            return obj;
        }

        $scope.getCId = function (id) {
//            $location.search = url;
            $scope.centerId = id;
            console.log(window.location.hash);
            if (window.location.hash.length != 0){
                var jsonString = searchToObject();
                console.log(jsonString.orderBy);
                $scope.sI1i = jsonString.userName || null;
                $scope.sI2i = jsonString.workerName || null;
                $scope.sI3i = jsonString.punktName || null;
                $scope.sI4i = jsonString.workerStump || null;
                $scope.sI5i = jsonString.weldMethod || null;
                $scope.sI7i = jsonString.material || null;
                $scope.sI8i = jsonString.techCardNum || null;
                $scope.dataEnd = jsonString.endDate||new Date();
                $scope.dataFrom = jsonString.startDate||new Date();
                $scope.toDataModel = jsonString.toDataModel||null;
                $scope.toDescModel = jsonString.toDescModel || null;
                for (i in $scope.liveLabels){
                    if ($scope.liveLabels[i].nameEng==jsonString.orderBy){
                        $scope.sort = $scope.liveLabels[i] || null;
                    }
                }
                $scope.getCount($scope.centerId);
                $scope.sortingFunc();
            } else {
                $scope.dataJson = {'offset':'50', 'startPosition':$scope.toStartDate.toString(), 'centerId':$scope.centerId, 'orderBy': 'userName'};
                $scope.getCount($scope.centerId);
                $scope.sortingFunc();
            }
        };

        $scope.toDeleteURL = function (size, items, name){
            $scope.code = null;
            $scope.response = null;

            $scope.items = $scope.records;
            $scope.deleteUrl = '/data/deleteWeldersData';
            var modalInstance = $modal.open({
                templateUrl: 'ModalAccept.html',
                controller: 'ModalAccept',
                size: 'sm',
                resolve: {
                    items: function () {
                        return $scope.items;
                    },
                    name: function() {
                        return $scope.deleteUrl;
                    }
                }
            });

            modalInstance.result.then(function (statusCheck) {
                $scope.isDisabled = statusCheck;
                location.reload();
            }, function () {
                $scope.fetch();
            });
        };



        $scope.sortingFunc = function(){
            $scope.toStartDate = ($scope.bigCurrentPage-1) * $scope.interval;
            $scope.toEndDate = $scope.bigCurrentPage * $scope.interval;
            $scope.dataEnd = new Date($scope.dataEnd);
            $scope.dataFrom = new Date($scope.dataFrom);

            //console.log($scope.toStartDate);
            if ($scope.toDataModel == 'disabled'){
                var filterJson = {
                    'userName':$scope.sI1i,
                    'workerName':$scope.sI2i,
                    'punktName':$scope.sI3i,
                    'workerStump':$scope.sI4i,
                    'weldMethod':$scope.sI5i,
                    'material':$scope.sI7i,
                    'techCardNum':$scope.sI8i
                };
                str = {
                    'offset':'50',
                    'startPosition':$scope.toDataModel,
                    'orderBy': $scope.sort.nameEng,
                    'order':$scope.toDescModel,
                    'toDescModel':$scope.toDescModel,
                    'toDataModel':$scope.toDataModel,
                    'userName':$scope.sI1i,
                    'workerName':$scope.sI2i,
                    'punktName':$scope.sI3i,
                    'workerStump':$scope.sI4i,
                    'weldMethod':$scope.sI5i,
                    'material':$scope.sI7i,
                    'techCardNum':$scope.sI8i,
                    'centerId':$scope.centerId
                };
            } else {
                var EndMonth=$scope.dataEnd.getMonth()+1;
                var FromMonth=$scope.dataFrom.getMonth()+1;
                var filterJson = {
                    'userName':$scope.sI1i,
                    'workerName':$scope.sI2i,
                    'punktName':$scope.sI3i,
                    'workerStump':$scope.sI4i,
                    'weldMethod':$scope.sI5i,
                    'material':$scope.sI7i,
                    'techCardNum':$scope.sI8i,
                    'endDate':$scope.dataEnd.getFullYear()+'-'+EndMonth+'-'+$scope.dataEnd.getDate(),
                    'startDate':$scope.dataFrom.getFullYear()+'-'+FromMonth+'-'+$scope.dataFrom.getDate()
                };
                str = {
                    'offset':'50',
                    'startPosition':$scope.toStartDate,
                    'orderBy': $scope.sort.nameEng,
                    'order':$scope.toDescModel,
                    'toDescModel':$scope.toDescModel,
                    'toDataModel':$scope.toDataModel,
                    'userName':$scope.sI1i,
                    'workerName':$scope.sI2i,
                    'punktName':$scope.sI3i,
                    'workerStump':$scope.sI4i,
                    'weldMethod':$scope.sI5i,
                    'material':$scope.sI7i,
                    'techCardNum':$scope.sI8i,
                    'endDate':$scope.dataEnd.getFullYear()+'-'+EndMonth+'-'+$scope.dataEnd.getDate(),
                    'startDate':$scope.dataFrom.getFullYear()+'-'+FromMonth+'-'+$scope.dataFrom.getDate(),
                    'centerId':$scope.centerId
                };
            }
            //console.log($scope.sort);
            //console.log($scope.toDescModel);

            //console.log($scope.toStartDate.toString());

            $scope.dataJson = {
                'offset':'50',
                'startPosition':$scope.toStartDate.toString(),
                'orderBy': $scope.sort.nameEng,
                'order':$scope.toDescModel.toString(),
                'toDataModel':$scope.toDataModel,
                'filter':filterJson,
                'centerId':$scope.centerId
            };

            //console.log(str);
            $location.search(str);
            //console.log($scope.dataJson);

            $scope.fetch();
        };


        var inputChangedPromise;

        $scope.sortable = function () {
            if(inputChangedPromise){
                $timeout.cancel(inputChangedPromise);
            }
            inputChangedPromise = $timeout(toTimeout,1000);
        };


        function toTimeout () {
            $scope.sortingFunc();
        }

        $scope.refresh = function(){
            $scope.toStartDate = ($scope.bigCurrentPage-1) * $scope.interval;
            $scope.toEndDate = $scope.bigCurrentPage * $scope.interval;

            $scope.varOrderBy = null;
            $scope.varOrder = null;
            $scope.sI1i = null;
            $scope.sI2i = null;
            $scope.sI3i = null;
            $scope.sI4i = null;
            $scope.sI5i = null;
            $scope.sI7i = null;
            $scope.sI8i = null;
            $scope.toDataModel = 'disabled';
            $scope.toDescModel = true;
            $scope.sort = false;

            $scope.sortingFunc();
        };


        $scope.toDesc = function () {
            $scope.sortingFunc();
        };

        $scope.pageChanged = function() {
            $scope.sortingFunc();
        };

        $scope.timeDataToCtrl = function (){
            $scope.sortingFunc();
        };


        var zagl = 0;
        var toStringJSON;

        $scope.emptyArr = false;

        $scope.fetch = function() {
            $scope.loading = true;
            $scope.code = null;
            $scope.response = null;
            console.log($scope.dataJson);

            $http({method: $scope.method, url: '/data/getWeldersData', cache: $templateCache, data: $scope.dataJson}).
                success(function(data, status) {
                    $scope.loading = false;
                    $scope.status = status;
                    $scope.datas = data;
                    $scope.emptyArr=false;

                    if (data.toString() == ''){
                        console.log('prokol');
                        $scope.emptyArr=true;
                    }
                    console.log(data);
                }).
                error(function(data, status) {
                    $scope.datas = data || "Request failed";
                    $scope.status = status;
                });
        };

        $scope.getCount = function (id) {
            var cid = {'centerId': id};
            $http({method: $scope.method, url: '/data/getWeldersDataCount', cache: $templateCache, data: cid}).
                success(function(data, status) {
                    $scope.status = status;
                    $scope.bigTotalItems = data.count;
                    console.log(data.count);
                }).
                error(function(data, status) {
                    $scope.datas = data || "Request failed";
                    $scope.status = status;
                });
        }
    });

