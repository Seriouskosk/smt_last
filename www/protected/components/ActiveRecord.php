<?php

abstract class ActiveRecord extends CActiveRecord
{
	const ROWS_PER_PAGE = 50;

	public static function model($className = __CLASS__)
	{
	    return parent::model($className);
	}
}