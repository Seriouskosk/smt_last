<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    public function getParameter($postData, $name)
    {
        if(isset($postData[$name]) && (!empty($postData[$name]) || $postData[$name] === "0"))
            if(!is_array($postData[$name]))
                return str_replace('\'', '\\\'', trim($postData[$name]));
            else {
                if(count($postData[$name]) <= 0)
                    return array();
                else {
                    $reArray = array();
                    foreach($postData[$name] as $arrayKey => $arrayVal)
                        $reArray[$arrayKey] = $this->getParameter($postData[$name], $arrayKey);

                    return $reArray;
                }
            }
        else return "";
    }

	public $layout='//layouts/mainView';

	public $menu=array();
	public $breadcrumbs=array();
}