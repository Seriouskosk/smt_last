<?

class TopMenu extends CComponent
{
    private  $categories;

    public static function createUrl($path)
    {
        return Yii::app()->createAbsoluteUrl($path, array(), 'http');
    }

    public function init()
    {
        $this->categories = array();

        if(Yii::app()->user->checkAccess(User::ROLE_COMMISSION_MEMBER))
            $this->addCategory('Все юридические лица', TopMenu::createUrl('certification/all'));
    }

    public function setCenter($centerId)
    {
        if(Yii::app()->user->checkAccess(User::ROLE_MODER)) {
            $cCenter = CCenter::model()->findByPk($centerId);

            $this->addCategory($cCenter->name, TopMenu::createUrl('certification/center/' . $centerId), array(
                0 => array(
                    'name' => 'Члены аттестационной комиссии центра',
                    'url' => TopMenu::createUrl('certification/users/' . $centerId)
                ),

                1 => array(
                    'name' => 'Устройства центра',
                    'url' => TopMenu::createUrl('certification/devices/' . $centerId)
                ),

                2 => array(
                    'name' => 'Организации',
                    'url' => TopMenu::createUrl('certification/organizations/' . $centerId)
                )
            ));

            $this->addCategory('Результаты аттестаций центра', TopMenu::createUrl('data/results/' . $centerId), array(
                0 => array(
                    'name' => 'Аттестация работников',
                    'url' => TopMenu::createUrl('data/welders/' . $centerId)
                ),

                1 => array(
                    'name' => 'Аттестация технологий',
                    'url' => TopMenu::createUrl('data/tech/' . $centerId)
                ),

                2 => array(
                    'name' => 'Аттестация оборудования',
                    'url' => TopMenu::createUrl('data/equip/' . $centerId)
                )
            ));
        }
        else {
            $this->addCategory('Мои результаты регистрации', TopMenu::createUrl('data/results/' . $centerId));
        }
    }

    public function addCategory($catName, $url, $subCat = array())
    {
        $this->categories[] = array(
            'name' => $catName,
            'url' => $url,
            'subCat' => $subCat
        );
    }

    public function getCategories()
    {
        $this->addCategory('Перейти на форум', TopMenu::createUrl('forum/'));

        print_r($this->categories);

        return $this->categories;
    }

    public function getUsername()
    {
        return User::model()->findByPk(Yii::app()->user->getId())->name;
    }

    public function getExitUrl()
    {
        return TopMenu::createUrl('main/logout/');
    }

}