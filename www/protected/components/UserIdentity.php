<?php

class UserIdentity extends CUserIdentity
{
    private $_id;
    private $_username;

    public function authenticate()
    {
        //Ищем пользователя
        $user = User::model()->findBySql("select * from users " . 
            "where LOWER(login) = '" . strtolower($this->username) . "' and password = '" . $this->password . "'");

        //Если такой имеется и он активен, то все в порядке
        if($user !== null && $user->isActive) {
            //Ставим параметры
            $this->_id = $user->id;
            $this->_username = $user->login;

            //Ставим привилегии пользователя
            $uAccess = array();
            $uAccess['welders'] = $user->welders;
            $uAccess['tech'] = $user->tech;
            $uAccess['equip'] = $user->equip;

            $this->setState('userAccess', $uAccess);

            $this->errorCode = self::ERROR_NONE;
            
        }
        //Иначе пишем, что ошибка валидации
        else $this->errorCode=self::ERROR_PASSWORD_INVALID;

        return $this->errorCode == self::ERROR_NONE;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getUsername()
    {
        return $this->_username;
    }
}