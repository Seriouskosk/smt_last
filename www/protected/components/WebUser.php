<?php

class WebUser extends CWebUser {
    private $_model = null;
    private $_identity;

    function getRole() {
        if($user = $this->getModel()) {

            $role = "";
            switch($user->level)
            {
                case 1:
                    $role = 'administrator';
                    break;
                case 2:
                    $role = 'commissionMember';
                    break;
                case 3:
                    $role = 'moderator';
                    break;
                default:
                    $role = 'user';
            }

            return $role;
        }
    }

    public function login($identity, $duration=0) {
        $this->_identity = $identity;
        return parent::login($identity, $duration);
    }

    private function getModel(){
        if (!$this->isGuest && $this->_model === null){
            $this->_model = User::model()->findByPk($this->id, array('select' => 'level'));
        }
        return $this->_model;
    }

    protected function afterLogin($fromCookie) {
        if ($this->_identity !== null) {
            if (Yii::app()->phpBB->login($this->_identity->username, $this->_identity->password) != 'SUCCESS') {
                Yii::log("Ошибка авторизации на форуме({$this->_identity->username})", CLogger::LEVEL_ERROR);
            }
        }

        parent::afterLogin($fromCookie);
    }

    protected function afterLogout() {
        Yii::app()->phpBB->logout();

        parent::afterLogout();
    }
}