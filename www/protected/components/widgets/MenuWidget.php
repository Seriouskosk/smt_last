<?

class MenuWidget extends CWidget
{
    public $centerId;

    public function init()
    {
        // этот метод будет вызван внутри CBaseController::beginWidget()
    }

    public function run()
    {
        $cCenter = CCenter::model()->findByPk($this->centerId);

        $certTypes = array();

        $certTypes['welders'] = $cCenter->weldersCode !== "" ? true : false;
        $certTypes['tech'] = $cCenter->techCode !== "" ? true : false;
        $certTypes['equip'] = $cCenter->equipCode !== "" ? true : false;

        $this->render('menu_tpl', array(
            'certTypes' => $certTypes
        ));
    }
}