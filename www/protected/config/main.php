<?php

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'SMT',
    'defaultController' => 'main',

	'preload'=>array('log'),

	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.helpers.*',
		'application.widgets.*'
	),

	'components'=>array(
        'phpBB'=>array(
            'class'=>'ext.phpBB.phpBB',
            'path'=>'webroot.forum',
        ),
        'request'=>array(
            // Возможно это и костыль, но без него никуда не поехать, тут мы определяем базовый URL нашего приложения.
            'baseUrl'=>$_SERVER['DOCUMENT_ROOT'].$_SERVER['PHP_SELF'] != $_SERVER['SCRIPT_FILENAME'] ? 'http://'.$_SERVER['HTTP_HOST'] : '',
            // ...
        ),
        'topMenu' => array(
            'class' => 'application.components.TopMenu'
        ),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin' => true,
            'class' => 'WebUser',
		),
		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			'urlFormat'=>'path',
            'showScriptName' => false,
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>/<certId:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>/<id:\d+>/<certId:\d+>/<layerNum:\d+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>/<certId:\d+>/<pageName:\w+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>/<certId:\d+>/<pageName:\w+>/<pracId:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>/<id:\d+>/<certId:\d+>/<pageName:\w+>/<pracId:\d+>/<layerNum:\d+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),

        'authManager' => array(
            // Будем использовать свой менеджер авторизации
            'class' => 'PhpAuthManager',
            // Роль по умолчанию. Все, кто не админы, модераторы и юзеры — гости.
            'defaultRoles' => array('guest'),
        ),

		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=smt_db;',
			//'connectionString' => 'mysqli:host=127.0.0.1;dbname=smt_db',
			//'emulatePrepare' => true,
			'username' => 'smt',
			'password' => 'rw3uMDYTC-s.F.-@',
			'charset' => 'utf8',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning, info'
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);