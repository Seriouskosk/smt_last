<?

class ApiController extends Controller
{
    private $_safePost;

    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);

        //Делаем $_POST безопасным
        $this->_safePost = new CEscapeData($_POST);

        file_put_contents('apilog', date('d.m.Y H:i:s') . ' (' . $_SERVER['REQUEST_URI'] . ') ' . print_r(array_merge($_REQUEST, $_FILES), true), FILE_APPEND);
    }

    protected function returnSuccess($params = null)
    {
        $data = [
            'success' => true
        ];

        //Переносим дополнительные параметры, если таковые имеются
        if($params !== null)
            foreach ($params as $key => $value)
                $data[$key] = $value;

        echo CJSON::encode($data);

        //Завершаем работу скрипта
        Yii:die();
    }

    protected function returnError($message = 'Неизвестная ошибка')
    {
        echo CJSON::encode(array(
            'success' => false,
            'errorMessage' => $message
        ));

        //Завершаем работу скрипта
        Yii:die();
    }

    protected static function savePhoto(&$object, $field)
    {
        //Устанавливаем новое имя для фотографии
        $fileName = date('dmYHis').urldecode(CText::randString(5).CText::translate($object->{$field}->getName()));

        //Устанавливаем путь сохранения фото
        $path = Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.
            'upload'.DIRECTORY_SEPARATOR.'photos'.DIRECTORY_SEPARATOR.$fileName;
        
        //Сохраняем фото с его новом именем
        $object->{$field}->saveAs($path);
        $object->{$field} = $fileName;
    }

    public function actionDoc()
    {
        $this->layout = '//layouts/freeView';
        $this->setPageTitle('Тестирование API');
        $this->render('api_tpl');
    }

    public function actionAuth()
    {
        //Это сделает наш код чистым
        $data = &$this->_safePost;

        try
        {
            if(empty($data->login) || empty($data->password))
                throw new Exception("Отсутствует имя пользователя или пароль");

            if(empty($data->equipAddr))
                throw new Exception('Отсутствует адрес устройства');

            $login = $data->login;
            $password = $data->password;
            $deviceAddress = $data->equipAddr;

            //Проводим авторизацию
            $identity = new UserIdentity($login, $password);

            //Если авторизация не прошла, кидаем исключение
            if(!$identity->authenticate())
                throw new Exception("Пользователя с таким логином и паролем не существует");

            $device = Device::findByCode($deviceAddress);
            if($device === null)
                throw new Exception("Устройства с таким адресом не существует");

            //Получаем объект пользователя
            $userId = $identity->getId();
            $user = User::model()->findByPk($userId);

            if($user->centerId !== $device->centerId)
                throw new Exception("Устройство не принадлежит вашему юридическому лицу");
                
            //Если пользователь находится онлайн, то принудительно закрываем сессию
            if($user->isOnline)
                $user->closeApiSession();

            //Создаем новую сессию для текущей авторизации
            $user->createApiSession($device);

            //Выводим результат
            echo $this->returnSuccess(['userId' => intval($userId)]);
        }
        catch(Exception $ex)
        {
            $this->returnError($ex->getMessage());
        }
    }

    public function actionAddCWelder()
    {
        //Это сделает наш код чистым
        $data = &$this->_safePost;

        //Необходимые параметры
        $requiredParameters = [
            'userId' => 'Не указан ID пользователя',
            'workerName' => 'Не указано имя работника',
            'workerStump' => 'Не указано клеймо сварщика',
            'weldMethod' => 'Не указан тип сварки',
            'jointType' => 'Не указан способ соединения',
            'material' => 'Не указан материал',
            'dFirst' => 'Не указан первый диаметр',
            'dSecond' => 'Не указан второй диаметр',
            'hFirst' => 'Не указана первая толщина',
            'hSecond' => 'Не указана вторая толщина',
            'lon' => 'Не указана широта',
            'lat' => 'Не указана долгота',
            'techCardNum' => 'Не указан номер тех. карты',
            'punktName' => 'Не указано название пункта проведения аттестации',
            'layerNorm' => 'Не указано нормативное количество слоев',
            'startDate' => 'Не указана дата начала аттестации'
        ];

        try
        {
            //Проверим, чтобы тип соединения был указан верно
            if(!preg_match("/^(Труба|Лист){1}\/(Труба|Лист){1}$/", $data->jointType))
                    throw new Exception("Тип соединения указан неверно");
                    
            //Создаем пользователя
            $user = User::model()->findByPk($data->userId);

            //Если такого нет, дальше делать нечего
            if($user === null)
                $this->returnError("Пользователя с указанным ID не существует");

            //Проверяем, что пользователь авторизован
            if(!$user->isOnline)
                $this->returnError("Создавать аттестации могут только авторизованные пользователи");

            //Получим последнюю аттестацию, которую делал этот пользователь
            //$cWelder = CWelder::getLastByUserId($user->id);
            //Если такой нет, или есть, но она закрыта, тогда создаем новую аттестацию
            //if($cWelder === null || $cWelder->isClosed)
                $cWelder = new CWelder;

            foreach($requiredParameters as $key => $value)
                //Если нужный параметр существует и не пустой
                if(isset($data->{$key}) && $data->{$key} !== "") {
                    //Добавляем этот параметр в нашу аттестацию
                    $cWelder->{$key} = $data->{$key};
                } else $this->returnError($requiredParameters[$key]);//Если параметра нет то дальше не идем

            $cWelder->centerId = $user->centerId;//Это костыль, но пока он нужен для совместимости
            //Явно указываем, что аттестация пока не закрыта и сохраняем
            $cWelder->isClosed = 0;
            $cWelder->save();

            //Выводим нужное значение
            $this->returnSuccess(['certId' => intval($cWelder->id)]);
        }
        catch(Exception $ex)
        {
            $this->returnError($ex->getMessage());
        }
    }

    public function actionAddCTech()
    {
        //Это сделает наш код чистым
        $data = &$this->_safePost;

        $requiredParameters = [
            'userId' => 'Не указан ID пользователя',
            'orgId' => 'Не указан ID организации',
            'workerName' => 'Не указано имя работника',
            'workerStump' => 'Не указано клеймо сварщика',
            'kccType' => 'Не указан тип КСС',
            'jointType' => 'Не указан способ соединения',
            'programName' => 'Не указано имя программы',
            'dFirst' => 'Не указан первый диаметр',
            'dSecond' => 'Не указан второй диаметр',
            'hFirst' => 'Не указана первая толщина',
            'hSecond' => 'Не указана вторая толщина',
            'lon' => 'Не указана широта',
            'lat' => 'Не указана долгота',
            'layerNorm' => 'Не указано нормативное количество слоев',
            'startDate' => 'Не указана дата начала аттестации'
        ];

        try
        {
            //Проверим, чтобы тип соединения был указан верно
            if(!preg_match("/^(Труба|Лист){1}\/(Труба|Лист){1}$/", $data->jointType))
                    throw new Exception("Тип соединения указан неверно");

            //Создаем пользователя
            $user = User::model()->findByPk($data->userId);

            //Если такого нет, дальше делать нечего
            if($user === null)
                $this->returnError("Пользователя с указанным ID не существует");

            //Проверяем, что пользователь авторизован
            if(!$user->isOnline)
                $this->returnError("Создавать аттестации могут только авторизованные пользователи");

            //Получим последнюю аттестацию, которую делал этот пользователь
            //$cTech = CTech::getLastByUserId($user->id);
            //Если такой нет, или есть, но она закрыта, тогда создаем новую аттестацию
            //if($cTech === null || $cTech->isClosed)
                $cTech = new CTech;

            foreach($requiredParameters as $key => $value)
                //Если нужный параметр существует и не пустой
                if(isset($data->{$key}) && $data->{$key} !== "") {
                    //Добавляем этот параметр в нашу аттестацию
                    $cTech->{$key} = $data->{$key};
                } else $this->returnError($requiredParameters[$key]);//Если параметра нет то дальше не идем

            $cTech->centerId = $user->centerId;//Это костыль, но пока он нужен для совместимости
            //Явно указываем, что аттестация пока не закрыта и сохраняем
            $cTech->isClosed = 0;
            $cTech->save();

            //Выводим нужное значение
            $this->returnSuccess(['certId' => intval($cTech->id)]);
        }
        catch(Exception $ex)
        {
            $this->returnError($ex->getMessage());
        }
    }

    public function actionAddCEquip()
    {
        //Это сделает наш код чистым
        $data = &$this->_safePost;

        $requiredParameters = [
            'userId' => 'Не указан ID пользователя',
            'orgId' => 'Не указан ID организации',
            'tradeMark' => 'Не указана марка оборудования',
            'factoryNum' => 'Не указан заводской номер',
            'invNum' => 'Не указан инвертарный номер',
            'weldMethod' => 'Не указан тип сварки',
            'programName' => 'Не указано имя программы',
        ];

        try
        {

           //Создаем пользователя
           $user = User::model()->findByPk($data->userId);

           //Если такого нет, дальше делать нечего
           if($user === null)
               $this->returnError("Пользователя с указанным ID не существует");

           //Проверяем, что пользователь авторизован
           if(!$user->isOnline)
               $this->returnError("Работать с аттестациями могут только авторизованные пользователи");

            //Берем незакрытую(!) запись с такими же параметрами
            $cEquip = CEquip::getByParams($data->orgId, $data->tradeMark, $data->factoryNum, $data->invNum, $data->weldMethod, $data->programName);

            if($cEquip === null)
            {
                //Если такой нет, тогда создаем новую аттестацию
                $cEquip = new CEquip;

                //И переносим параметры в нее
                foreach($requiredParameters as $key => $value)
                    //Если нужный параметр существует и не пустой
                    if(isset($data->{$key}) && $data->{$key} !== "") {
                        //Добавляем этот параметр в нашу аттестацию
                        $cEquip->{$key} = $data->{$key};
                    } else $this->returnError($requiredParameters[$key]);//Если параметра нет то дальше не идем

                $cEquip->centerId = $user->centerId;//Это костыль, но пока он нужен для совместимости
                //Явно указываем, что аттестация пока не закрыта и сохраняем
                $cEquip->isClosed = 0;
                $cEquip->save();

                //Выводим нужное значение
                $this->returnSuccess([
                        'certId' => intval($cEquip->id),
                        //Специальные испытания еще не выполнялись
                        'status' => false
                    ]);
            }
            else
            {
                //Возвращаем данные
                $this->returnSuccess([
                        'certId' => intval($cEquip->id),
                        'status' => CEquipSpec::isDone($cEquip->id)
                    ]);
            }
        }
        catch(Exception $ex)
        {
            $this->returnError($ex->getMessage());
        }
    }

    public function actionAddParameter()
    {
        //Это сделает наш код чистым
        $data = &$this->_safePost;

        $requiredParameters = [
            'certId' => "Не указан ID аттестации",
            'certType' => "Не указан тип аттестации",
            'layer' => "Не указан номер слоя",
            'position' => "Не указана позиция",
            'startDate' => "Не указано время начала сварки",
            'endDate' => "Не указано время окончания сварки"
        ];

        try
        {
            //Сначала проверим, чтот certType указан корректно
            if(!in_array(intval($data->certType), range(0, 2)))
                throw new Exception("Тип аттестации указан неверно (допускаются значения 0-2)");

            //И что позиция указана верно
            if(!in_array(intval($data->position), range(1, 5)))
                throw new Exception("Позиция указана неверно (допускаются значения 1-5)");        

            //Создаем новый параметр
            $cParameter = new CParameter;

            //И переносим параметры
            foreach($requiredParameters as $key => $value)
                //Если нужный параметр существует и не пустой
                if(isset($data->{$key}) && $data->{$key} !== "") {
                    //Добавляем этот параметр в нашу аттестацию
                    $cParameter->{$key} = $data->{$key};
                } else $this->returnError($requiredParameters[$key]);//Если параметра нет то дальше не идем

            //Проверяем на дубликаты
            $sql = "SELECT * FROM `params` WHERE `certId` = {$data->certId} AND `startDate` = '{$data->startDate}' AND `endDate` = '{$data->endDate}' LIMIT 1";

            $cParameterTry = CParameter::model()->findBySql($sql);

            if($cParameterTry !== null) {
                $cParameter = $cParameterTry;
            }

            //Проверяем что файл регистограммы на месте
            if(!empty($_FILES['regFile']['name']))
                $cParameter->regFileUrl = CUploadedFile::getInstanceByName('regFile');
            else throw new Exception("Файл регистограммы обязатлен");
            
            //Если нужно, подгрузим и фото
            if(!empty($_FILES['photo']['name']))
                $cParameter->photo = CUploadedFile::getInstanceByName('photo');
            else $cParameter->photo = "";

            //Если сохранение прошло упешно
            if($cParameter->save()) {

                //Сохраняем фотографию
                if(!empty($_FILES['photo']['name']))
                    self::savePhoto($cParameter, 'photo');

                //Сохраняем файл регистограмм
                $fileName = date('dmYHis') . CText::randString(5) . urldecode(CText::translate($cParameter->regFileUrl->getName()));

                //Устанавливаем путь сохранения файла регистограммы
                $path = Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.
                    'upload'.DIRECTORY_SEPARATOR.$fileName;

                //Сохраняем файл с новым именем
                $cParameter->regFileUrl->saveAs($path);

                //Закрепляем имена в базе данных
                //$cParameter->regFile = urldecode(CText::translate($cParameter->regFileUrl->getName())).'.csv';
                $cParameter->regFileUrl = $fileName;

                //Ставим параметр в очередь на обработку
                $cParameter->processed = 0;
                //Закрепляем изменения в базе данных
                $cParameter->save();

                //Все прошло успешно
                $this->returnSuccess();
            }
            //Если сохранение не прошло вываливаем все  ошибки
            else $this->returnError($cParameter->errorSummary());
        }
        catch(Exception $ex)
        {
            $this->returnError($ex->getMessage());
        }
    }

    public function actionCheckOrganization()
    {
        try
        {
            $inn = $this->_safePost->inn;
            //warning
            if(!isset($inn) || $inn == "")
                throw new Exception("ИНН не указан");
            
            //Получаеи организацию по ИНН    
            $org = Organization::findByInn($inn);

            //
            if($org === null)
                throw new Exception("Организации с указанным ИНН не существует");

            //Все прошло успешно
            $this->returnSuccess([
                    'orgId' => intval($org->id),
                    'orgName' => $org->name
                ]);
        }
        catch(Exception $ex)
        {
            $this->returnError($ex->getMessage());
        }
    }

    public function actionFinishCWelder()
    {
        $requiredParameters = array(
            'certId' => 'Не указан ID аттестации',
            'endDate' => 'Не указана дата окончания аттестации',
            'time' => 'Не указано время проведения аттестации',
            'layerCount' => 'Не указано количество слоев'
        );

        //Это сделает наш код чистым
        $data = &$this->_safePost;

        try
        {
            //Получаем аттестацию
            $cWelder = CWelder::getById($data->certId);

            if($cWelder === null)
                throw new Exception("Аттестации сварщика с таким ID не существует");  

            //Переносим параметры
            foreach($requiredParameters as $key => $value) {
                if($key === 'certId')
                    continue;
                //Если нужный параметр существует и не пустой
                if(isset($data->{$key}) && $data->{$key} !== "") {
                    //Добавляем этот параметр в нашу аттестацию
                    $cWelder->{$key} = $data->{$key};

                } else $this->returnError($requiredParameters[$key]);//Если параметра нет то дальше не идем                
            }

            //var_dump($data);
            //Первая фотография
            if(!empty($_FILES['photoFirst']['name']))
                $cWelder->photoFirst = CUploadedFile::getInstanceByName('photoFirst');
            else $cWelder->photoFirst = "";

            //Вторая фотография
            if(!empty($_FILES['photoSecond']['name']))
                $cWelder->photoSecond = CUploadedFile::getInstanceByName('photoSecond');
            else $cWelder->photoSecond = "";

            //Итоговая фотография
            if(!empty($_FILES['finishPhoto']['name']))
                $cWelder->finishPhoto = CUploadedFile::getInstanceByName('finishPhoto');
            else $cWelder->finishPhoto = "";

            //Явно закрываем аттестацию
            $cWelder->isClosed = 1;

            //И пытаемся сохранить
            if($cWelder->save()) {

                if(!empty($_FILES['photoFirst']['name']))
                    self::savePhoto($cWelder, 'photoFirst');

                if(!empty($_FILES['photoSecond']['name']))
                    self::savePhoto($cWelder, 'photoSecond');

                if(!empty($_FILES['finishPhoto']['name']))
                    self::savePhoto($cWelder, 'finishPhoto');

                //Если делали хоть какое то изменение, то сохраняем
                if(!empty($_FILES['photoFirst']['name']) || !empty($_FILES['photoSecond']['name']) || !empty($_FILES['finishPhoto']['name']))
                    $cWelder->save();

                //Все прошло успешно
                $this->returnSuccess();
            }
            //Иначе выводим все ошибки
            else $this->returnError($cParameter->errorSummary());
        }
        catch(Exception $ex)
        {
            $this->returnError($ex->getMessage());
        }
    }

    public function actionFinishCTech()
    {
        $requiredParameters = array(
            'certId' => 'Не указан ID аттестации',
            'endDate' => 'Не указана дата окончания аттестации',
            'time' => 'Не указано время проведения аттестации',
            'layerCount' => 'Не указано количество слоев'
        );

        //Это сделает наш код чистым
        $data = &$this->_safePost;

        try
        {
            //Получаем аттестацию
            $cTech = CTech::getById($data->certId);

            if($cTech === null)
                throw new Exception("Аттестации технологий с таким ID не существует");

            //Переносим параметры
            foreach($requiredParameters as $key => $value) {
                if($key === 'certId')
                    continue;
                //Если нужный параметр существует и не пустой
                if(isset($data->{$key}) && $data->{$key} !== "") {
                    //Добавляем этот параметр в нашу аттестацию
                    $cTech->{$key} = $data->{$key};
                } else $this->returnError($requiredParameters[$key]);//Если параметра нет то дальше не идем
            }

            //Первая фотография
            if(!empty($_FILES['photoFirst']['name']))
                $cTech->photoFirst = CUploadedFile::getInstanceByName('photoFirst');
            else $cTech->photoFirst = "";

            //Вторая фотография
            if(!empty($_FILES['photoSecond']['name']))
                $cTech->photoSecond = CUploadedFile::getInstanceByName('photoSecond');
            else $cTech->photoSecond = "";

            //Итоговая фотография
            if(!empty($_FILES['finishPhoto']['name']))
                $cTech->finishPhoto = CUploadedFile::getInstanceByName('finishPhoto');
            else $cTech->finishPhoto = "";

            //Явно закрываем аттестацию
            $cTech->isClosed = 1;

            //И пытаемся сохранить
            if($cTech->save()) {

                if(!empty($_FILES['photoFirst']['name'])) 
                    self::savePhoto($cTech, 'photoFirst');

                if(!empty($_FILES['photoSecond']['name'])) 
                    self::savePhoto($cTech, 'photoSecond');

                if(!empty($_FILES['finishPhoto']['name'])) 
                    self::savePhoto($cTech, 'finishPhoto');

                if(!empty($_FILES['photoFirst']['name']) || !empty($_FILES['photoSecond']['name']) || !empty($_FILES['finishPhoto']['name']))
                    $cTech->save();

                //Если все прошло успешно
                $this->returnSuccess();
            }
            //Иначе сообщаем об ошибке
            else $this->returnError($cParameter->errorSummary());
        }
        catch(Exception $ex)
        {
            $this->returnError($ex->getMessage());
        }
    }

    public function actionAddCEquipPrac()
    {

        $requiredParameters = array(
            'pracId' => 'Не указан ID аттестации оборудования',
            'workerName' => 'Не указано имя работника',
            'workerStump' => 'Не указано клеймо сварщика',
            'kccType' => 'Не указан тип КСС',
            'jointType' => 'Не указан способ соединения',
            'dFirst' => 'Не указан первый диаметр',
            'dSecond' => 'Не указан второй диаметр',
            'hFirst' => 'Не указана первая толщина',
            'hSecond' => 'Не указана вторая толщина',
            'lon' => 'Не указана широта',
            'lat' => 'Не указана долгота',
            'layerNorm' => 'Не указано нормативное количество слоев',
            'startDate' => 'Не указана дата начала аттестации'
        );

        //Это сделает наш код чистым
        $data = &$this->_safePost;

        try
        {
            //Получаем аттестацию оборудования
            $cEquip = CEquip::getById($data->pracId);

            //Проверяем, что она существует
            if($cEquip === null)
                throw new Exception("Аттестации оборудования с таким ID не существует");

            //И что она не закрыта (warning)
            if(intval($cEquip->isClosed) == 1)
                throw new Exception("Нельзя добавить практические испытания в закрытую аттестацию");

            //Получим последние практические испытания, которые делал этот пользователь
            //$cEquipPrac = CEquipPrac::getLastByEquipId($cEquip->id);
            //Если такой нет, или есть, но она закрыта, тогда создаем новую аттестацию (warning)
            //if($cEquipPrac === null || intval($cEquipPrac->isClosed))
            $cEquipPrac = new CEquipPrac;

            //Переносим параметры
            foreach($requiredParameters as $key => $value)
                //Если нужный параметр существует и не пустой
                if(isset($data->{$key}) && $data->{$key} !== "") {
                    //Добавляем этот параметр в нашу аттестацию
                    $cEquipPrac->{$key} = $data->{$key};
                } else $this->returnError($requiredParameters[$key]);//Если параметра нет то дальше не идем

            //Явно указываем, что практические испытания не закрыты
            $cEquipPrac->isClosed = 0;

            //И сохраняем
            $cEquipPrac->save();

            //Все прошло успешно
            $this->returnSuccess([
                    'pracId' => intval($cEquipPrac->id)
                ]);

        }
        catch(Exception $ex)
        {
            $this->returnError($ex->getMessage());
        }
    }

    public function actionFinishCEquipPrac()
    {
        $requiredParameters = array(
            'pracId' => 'Не указан ID практических испытаний',
            'endDate' => 'Не указана дата окончания аттестации',
            'time' => 'Не указано время проведения аттестации',
            'layerCount' => 'Не указано количество слоев'
        );

        //Это сделает наш код чистым
        $data = &$this->_safePost;

        try
        {
            //Получаем аттестацию
            $cEquipPrac = CEquipPrac::getById($data->pracId);

            //Если такой нет, то заканчиваем с ошибкой
            if($cEquipPrac === null)
                throw new Exception("Практических испытаний с таким ID не существует");

            //Переносим параметры
            foreach($requiredParameters as $key => $value) {
                if($key === 'pracId')
                    continue;
                //Если нужный параметр существует и не пустой
                if(isset($data->{$key}) && $data->{$key} !== "") {
                    //Добавляем этот параметр в нашу аттестацию
                    $cEquipPrac->{$key} = $data->{$key};
                } else $this->returnError($requiredParameters[$key]);//Если параметра нет то дальше не идем
            }

            //Первая фотография
            if(!empty($_FILES['photoFirst']['name']))
                $cEquipPrac->photoFirst = CUploadedFile::getInstanceByName('photoFirst');
            else $cEquipPrac->photoFirst = "";

            //Вторая фотография
            if(!empty($_FILES['photoSecond']['name']))
                $cEquipPrac->photoSecond = CUploadedFile::getInstanceByName('photoSecond');
            else $cEquipPrac->photoSecond = "";

            //Итоговая фотография
            if(!empty($_FILES['finishPhoto']['name']))
                $cEquipPrac->finishPhoto = CUploadedFile::getInstanceByName('finishPhoto');
            else $cEquipPrac->finishPhoto = "";

            //Явно закрываем аттестацию
            $cEquipPrac->isClosed = 1;


            if($cEquipPrac->save()) {

                if(!empty($_FILES['photoFirst']['name'])) 
                    self::savePhoto($cEquipPrac, 'photoFirst');

                if(!empty($_FILES['photoSecond']['name'])) 
                    self::savePhoto($cEquipPrac, 'photoSecond');

                if(!empty($_FILES['finishPhoto']['name'])) 
                    self::savePhoto($cEquipPrac, 'finishPhoto');

                if(!empty($_FILES['photoFirst']['name']) || !empty($_FILES['photoSecond']['name']) || !empty($_FILES['finishPhoto']['name']))
                    $cEquipPrac->save();

                //Все прошло успешно
                $this->returnSuccess();
            }
            //Если сохранение провалилось
            else $this->returnError($cParameter->errorSummary());

            //Все прошло успешно
            $this->returnSuccess();
        }
        catch(Exception $ex)
        {
            $this->returnError($ex->getMessage());
        }
    }

    public function actionFinishCEquip()
    {
        //Это сделает наш код чистым
        $data = &$this->_safePost;

        try
        {
            //Получаем аттестацию
            $cEquip = CEquip::getById($data->certId);

            //Если такой нет
            if($cEquip === null)
                $this->returnError('Аттестации оборудования с указанным ID не существует');

            //Явно закрываем и сохраняем
            $cEquip->isClosed = 1;
            $cEquip->save();

            //Все прошло успешно
            $this->returnSuccess();
        }
        catch(Exception $ex)
        {
            $this->returnError($ex->getMessage());
        }
    }

    public function actionAddCEquipSpec()
    {
        $requiredParameters = array(
            'certId' => 'Не указан ID аттестации оборудования',
            'startDate' => 'Не указана дата начала аттестации',
            'endDate' => 'Не указана дата окончания аттестации',
            'lon' => 'Не указана долгота (координаты)',
            'lat' => 'Не указана широта (координаты)',
            //Параметры специальных испытаний
            'minU' => "Не указано минимальное напряжение",
            'minI' => "Не указана минимальная сила тока",
            'maxU' => 'Не указано максимальное напряжение',
            'maxI' => "Не указана максимальная сила тока",
            'normU' => "Не указано напряжение",
            'normI' => "Не указана сила тока"
        );

        //Это сделает наш код чистым
        $data = &$this->_safePost;

        try
        {
            //Получаем аттестацию
            $cEquip = CEquip::getById($data->certId);

            //Если такой нет
            if($cEquip === null)
                $this->returnError('Аттестации оборудования с указанным ID не существует');   

            //Создаем специальные испытания
            $cEquipSpec = new CEquipSpec();

            //Переносим параметры
            foreach($requiredParameters as $key => $value)
                //Если нужный параметр существует и не пустой
                if(isset($data->{$key}) && $data->{$key} !== "") {
                    //Добавляем этот параметр в нашу аттестацию
                    $cEquipSpec->{$key} = $data->{$key};
                } else $this->returnError($requiredParameters[$key]);//Если параметра нет то дальше не идем

            //Сохраняем специальные испытания
            $cEquipSpec->save();

            $this->returnSuccess();
        }
        catch(Exception $ex)
        {
            $this->returnError($ex->getMessage());
        }
    }

    public function actionAddCEquipVac()
    {
        
        $requiredParameters = array(
            'certId' => 'Не указан ID аттестации',
            'mode' => 'Не указан режим (mode)',
            'u' => 'Не указано значение напряжения',
            'i' => 'Не указано значение силы тока',
        );

        //Это сделает наш код чистым
        $data = &$this->_safePost;

        try
        {
            //Получаем аттестацию
            $cEquip = CEquip::getById($data->certId);

            //Если такой нет
            if($cEquip === null)
                $this->returnError('Аттестации оборудрвания с указанным ID не существует'); 

            //Создаем новую точку ВАХ
            $cEquipVac = new CEquipVac();

            //Переносим параметры
            foreach($requiredParameters as $key => $value)
                //Если нужный параметр существует и не пустой
                if(isset($data->{$key}) && $data->{$key} !== "") {
                    //Добавляем этот параметр в нашу аттестацию
                    $cEquipVac->{$key} = $data->{$key};
                } else $this->returnError($requiredParameters[$key]);//Если параметра нет то дальше не идем

            
            //Сохраняем точку
            $cEquipVac->save();

            //Выходим
            $this->returnSuccess();
        }
        catch(Exception $ex)
        {
            $this->returnError($ex->getMessage());
        }
    }

    public function actionLogout()
    {
        //Это сделает наш код чистым
        $data = &$this->_safePost;

        try
        {   
            //Если параметр некорректен
            if(!isset($data->userId) || empty($data->userId))
                //Выкидываем ошибку
                throw new Exception("Не указан ID пользователя");

            //Находим пользователя
            $user = User::getById($data->userId);
            if($user === null)
                throw new Exception("Пользователя с указанным ID не существует");
                

            //Закрываем сессию
            $user->closeApiSession();

            $this->returnSuccess();
        }
        catch(Exception $ex)
        {
            $this->returnError($ex->getMessage());
        }
    }
}