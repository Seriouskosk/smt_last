<?
class CertificationController extends Controller
{
    public function actionIndex()
    {
        $this->actionAll();
    }

    public function actionAll()
    {
        //Проверка авторизации
        if(Yii::app()->user->getIsGuest())
            $this->redirect(Yii::app()->homeUrl);

        $userId = Yii::app()->user->getId();

        //Проверка прав пользователя
        if(!Yii::app()->user->checkAccess(User::ROLE_COMMISSION_MEMBER)) {
                $user = User::model()->findByPk(Yii::app()->user->getId());
                $this->redirect(Yii::app()->createUrl('certification/center/', array('id' => $user->centerId)));
        }

        $this->render('all_centers_tpl', array(
            'userId' => $userId
        ));
    }

    public function actionCenter($id)
    {
        //Проверка авторизации
        if(Yii::app()->user->getIsGuest())
            $this->redirect(Yii::app()->homeUrl);

        $userId = Yii::app()->user->getId();
        $user = User::model()->findByPk($userId);

        //Проверка прав пользователя
        if(!Yii::app()->user->checkAccess(User::ROLE_MODER))
            $this->redirect(Yii::app()->createUrl('data/results/', array('id' => $user->centerId)));

        //Проверка соответсвия аттестационного центра
        if($user->centerId !== $id && !Yii::app()->user->checkAccess(User::ROLE_COMMISSION_MEMBER))
            $this->redirect(Yii::app()->createUrl('certification/center/', array('id' => $user->centerId)));

        Yii::app()->topMenu->setCenter($id);

        $center = CCenter::model()->findByPk($id);
        if($center == null)
            throw new CHttpException(404, 'Такого юридического лица не существует.');

        $this->render('center_tpl', array(
                'userId' => $userId,
                'center' => $center
            ));
    }

    public function actionUsers($id)
    {
        //Проверка авторизации
        if(Yii::app()->user->getIsGuest())
            $this->redirect(Yii::app()->homeUrl);

        $userId = Yii::app()->user->getId();
        $user = User::model()->findByPk($userId);

        //Проверка прав пользователя
        if(!Yii::app()->user->checkAccess(User::ROLE_MODER))
            $this->redirect(Yii::app()->createUrl('data/welders/', array('id' => $user->centerId)));

        //Проверка соответсвия аттестационного центра
        if($user->centerId !== $id && !Yii::app()->user->checkAccess(User::ROLE_COMMISSION_MEMBER))
            $this->redirect(Yii::app()->createUrl('certification/users/', array('id' => $user->centerId)));

        Yii::app()->topMenu->setCenter($id);

        $cCenter = CCenter::model()->findByPk($id);

        $certTypes = array();

        $certTypes['welders'] = $cCenter->weldersCode !== "" ? true : false;
        $certTypes['tech'] = $cCenter->techCode !== "" ? true : false;
        $certTypes['equip'] = $cCenter->equipCode !== "" ? true : false;

        $this->render('users_tpl', array(
            'userId' => $userId,
            'centerId' => $id,
            'certTypes' => $certTypes
        ));
    }

    public function actionDevices($id)
    {
        //Проверка авторизации
        if(Yii::app()->user->getIsGuest())
            $this->redirect(Yii::app()->homeUrl);

        $userId = Yii::app()->user->getId();
        $user = User::model()->findByPk($userId);

        //Проверка прав пользователя
        if(!Yii::app()->user->checkAccess(User::ROLE_MODER))
            $this->redirect(Yii::app()->createUrl('data/welders/', array('id' => $user->centerId)));

        //Проверка соответсвия аттестационного центра
        if($user->centerId !== $id && !Yii::app()->user->checkAccess(User::ROLE_COMMISSION_MEMBER))
            $this->redirect(Yii::app()->createUrl('certification/devices/', array('id' => $user->centerId)));

        Yii::app()->topMenu->setCenter($id);

        $this->render('devices_tpl', array(
            'userId' => $userId,
            'centerId' => $id
        ));
    }

    public function actionOrganizations($id)
    {
        //Проверка авторизации
        if(Yii::app()->user->getIsGuest())
            $this->redirect(Yii::app()->homeUrl);

        $userId = Yii::app()->user->getId();
        $user = User::model()->findByPk($userId);

        //Проверка соответсвия аттестационного центра
        if($user->centerId !== $id  && !Yii::app()->user->checkAccess(User::ROLE_COMMISSION_MEMBER))
            $this->redirect(Yii::app()->createUrl('certification/organizations/', array('id' => $user->centerId)));

        Yii::app()->topMenu->setCenter($id);

        $this->render('organizations_tpl', array(
            'userId' => $userId,
            'centerId' => $id
        ));
    }

    //Checked
    public function actionAddCenter()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $name = $this->getParameter($postData, 'name');
        $adress =$this->getParameter($postData, 'adress');
        $phone = $this->getParameter($postData, 'phone');
        $lon = $this->getParameter($postData, 'lon');
        $lat = $this->getParameter($postData, 'lat');
        $city = $this->getParameter($postData, 'city');

        $weldersCode = $this->getParameter($postData, 'weldersCode');
        $techCode = $this->getParameter($postData, 'techCode');
        $equipCode = $this->getParameter($postData, 'equipCode');

        echo CCenter::addCenter($name, $adress, $phone, $lon, $lat, $city, $weldersCode, $techCode, $equipCode);
    }

    public function actionUpdateCenter()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        echo CCenter::updateCenter((int)$postData['id'], $postData);
    }

    //Checked
    public function actionGetCenters()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $startPosition = $this->getParameter($postData, 'startPosition');
        $offset = $this->getParameter($postData, 'offset');

        $city = $this->getParameter($postData, 'city');
        $orgName = $this->getParameter($postData, 'orgName');
        $code = $this->getParameter($postData, 'code');

        echo CCenter::getCenters($startPosition, $offset, $city, $orgName, $code);
    }

    //Checked
    public function actionGetCentersCount()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $city = $this->getParameter($postData, 'city');
        $orgName = $this->getParameter($postData, 'orgName');
        $code = $this->getParameter($postData, 'code');

        echo CCenter::getCentersCount($city, $orgName, $code);
    }

    public function actionDeleteCenters()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $records = $this->getParameter($postData, 'records');

        echo CCenter::DeleteCenters($records);
    }


    public function actionGetAuthSessions()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $startPosition = $this->getParameter($postData, 'startPosition');
        $offset =  $this->getParameter($postData, 'offset');
        $centerId =  $this->getParameter($postData, 'centerId');

        echo CSession::getAuthSessions($centerId, $startPosition, $offset);
    }

    public function actionGetAuthSessionsCount()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        echo CSession::getAuthSessionsCount($this->getParameter($postData, 'centerId'));
    }

    public function actionAddUser()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $centerId = $this->getParameter($postData, 'centerId');
        $name = $this->getParameter($postData, 'name');
        $login = $this->getParameter($postData, 'login');
        $password = $this->getParameter($postData, 'password');
        $level = $this->getParameter($postData, 'accessLevel');

        $welders = $this->getParameter($postData, 'welders');
        $tech = $this->getParameter($postData, 'tech');
        $equip = $this->getParameter($postData, 'equip');

        echo User::addUser($centerId, $name, $login,$password, $level, $welders, $tech, $equip);
    }

    public function actionUpdateUser()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        echo User::updateUser((int)$postData['id'], $postData);
    }

    public function actionGetUsersByRights()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $startPosition = $this->getParameter($postData, 'startPosition');
        $offset =  $this->getParameter($postData, 'offset');

        $centerId =  $this->getParameter($postData, 'centerId');
        //$certType =  $this->getParameter($postData, 'certType');
        $loginInfo = $this->getParameter($postData, 'loginInfo');
        $username =  $this->getParameter($postData, 'userName');

        echo User::getUsersByRights($centerId, $startPosition, $offset, $loginInfo, $username);
    }

    public function actionGetUsersCount()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $centerId = $this->getParameter($postData, 'centerId');
        //$certType =  $this->getParameter($postData, 'certType');

        $username = $this->getParameter($postData, 'userName');

        echo User::getUsersByRightsCount($centerId, $username);
    }

    public function actionDeleteUsers()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $records = $this->getParameter($postData, 'records');

        echo User::DeleteUsers($records);
    }

    public function actionActivateUsers()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $records =  $this->getParameter($postData, 'records');

        echo User::ActivateUsers($records);
    }

    public function actionAddDevice()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $centerId = $this->getParameter($postData, 'centerId');
        $code = $this->getParameter($postData, 'code');

        echo Device::addDevice($centerId, $code);
    }

    public function actionUpdateDevice()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        echo Device::updateDevice((int)$postData['id'], $postData);
    }

    public function actionGetDevices()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $startPosition = $this->getParameter($postData, 'startPosition');
        $offset =  $this->getParameter($postData, 'offset');

        $centerId =  $this->getParameter($postData, 'centerId');
        $code =  $this->getParameter($postData, 'code');

        echo Device::getDevices($centerId, $startPosition, $offset, $code);
    }

    public function actionGetDevicesCount()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $centerId =  $this->getParameter($postData, 'centerId');
        $code = $this->getParameter($postData, 'code');

        echo Device::getDevicesCount($centerId, $code);
    }

    public function actionDeleteDevices()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $records = $this->getParameter($postData, 'records');

        echo Device::DeleteDevices($records);
    }

    public function actionAddOrganization()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $name = $this->getParameter($postData, 'name');
        $inn = $this->getParameter($postData, 'inn');
        $adress = $this->getParameter($postData, 'adress');
        $phone = $this->getParameter($postData, 'phone');
        $centerId = $this->getParameter($postData, 'centerId');

        echo Organization::addOrganization($centerId, $name, $inn, $adress, $phone);
    }

    public function actionUpdateOrganization()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        echo Organization::updateOrganization((int)$postData['id'], $postData);
    }

    public function actionGetOrganizations()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $centerId = $this->getParameter($postData, 'centerId');

        $startPosition = $this->getParameter($postData, 'startPosition');
        $offset = $this->getParameter($postData, 'offset');

        $name = $this->getParameter($postData, 'name');
        $inn =  $this->getParameter($postData, 'inn');

        echo Organization::getOrganizations($centerId, $startPosition, $offset, $name, $inn);
    }

    public function actionGetOrganizationsCount()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $centerId = $this->getParameter($postData, 'centerId');
        $name =  $this->getParameter($postData, 'name');

        echo Organization::getOrganizationsCount($centerId, $name);
    }

    public function actionDeleteOrganizations()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $records = $this->getParameter($postData, 'records');

        echo Organization::DeleteOrganizations($records);
    }
}