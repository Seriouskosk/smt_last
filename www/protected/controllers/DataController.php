<?

class DataController extends Controller
{
    public function actionResults($id)
    {
        if(Yii::app()->user->getIsGuest())
            $this->redirect(Yii::app()->homeUrl);

        if(Yii::app()->user->userAccess['welders'])
            $this->redirect(Yii::app()->createUrl('data/welders/', array('id' => $id)));

        if(Yii::app()->user->userAccess['tech'])
            $this->redirect(Yii::app()->createUrl('data/tech/', array('id' => $id)));

        if(Yii::app()->user->userAccess['equip'])
            $this->redirect(Yii::app()->createUrl('data/equip/', array('id' => $id)));
    }

    public function actionWelders($id, $certId = 0, $layerNum = 0)
    {
        //Проверка авторизации
        if(Yii::app()->user->getIsGuest())
            $this->redirect(Yii::app()->homeUrl);

        $userId = Yii::app()->user->getId();

        if($certId == 0)
            $this->render('welders_tpl', array(
                'userId' => $userId,
                'centerId' => $id
            ));
        else
        {
			if($layerNum == 0) {
				$cWelder = CWelder::model()->findByPk($certId);

				$this->render('result_welder_tpl', array(
					'userId' => $userId,
					'centerId' => $id,
					'certId' => $certId,
					'cData' => $cWelder->getData()
				));
			}
			else {
				$cParameter = CParameter::model()->findByPk($layerNum);
				$regFile = $cParameter->regFile;
				$startDate = strtotime($cParameter->startDate);
				
				$this->render('practical_result_params_equip_chart_tpl', array(
					'userId' => $userId,
					'centerId' => $id,
					'certId' => $certId,
					'regFile' => $regFile,
					'startDate' => $startDate
				));
			}
        }
    }

    public function actionPdfWelders()
    {
        if(Yii::app()->user->getIsGuest()) {
            echo "Чтобы иметь возможность просматривать отчеты нужно авторизоваться";
            return;
        }

        $certId = $_GET['certId'];

        echo CWelder::renderPdf($certId);
    }

    public function actionPdfTech()
    {
        if(Yii::app()->user->getIsGuest()) {
            echo "Чтобы иметь возможность просматривать отчеты нужно авторизоваться";
            return;
        }

        $certId = $_GET['certId'];

        echo CTech::renderPdf($certId);
    }

    public function actionPdfEquipPrac()
    {
        if(Yii::app()->user->getIsGuest()) {
            echo "Чтобы иметь возможность просматривать отчеты нужно авторизоваться";
            return;
        }

        $certId = $_GET['certId'];

        echo CEquipPrac::renderPdf($certId);
    }

    public function actionPdfEquipSpec()
    {
        if(Yii::app()->user->getIsGuest()) {
            echo "Чтобы иметь возможность просматривать отчеты нужно авторизоваться";
            return;
        }

        $certId = $_GET['certId'];

        echo CEquipSpec::renderPdf($certId);
    }

    public function actionTech($id, $certId = 0, $layerNum = 0)
    {
        //Проверка авторизации
        if(Yii::app()->user->getIsGuest())
            $this->redirect(Yii::app()->homeUrl);

        Yii::app()->topMenu->setCenter($id);

        $userId = Yii::app()->user->getId();

        if($certId == 0)
            $this->render('tech_tpl', array(
                'userId' => $userId,
                'centerId' => $id
            ));
        else
        {
			if($layerNum == 0) {
				$cTech = CTech::model()->findByPk($certId);

				$this->render('result_tech_tpl', array(
					'userId' => $userId,
					'centerId' => $id,
					'certId' => $certId,
					'cData' => $cTech->getData()
				));
			}
			else {
				$cParameter = CParameter::model()->findByPk($layerNum);
				$regFile = $cParameter->regFile;
				$startDate = strtotime($cParameter->startDate);
				
				$this->render('practical_result_params_equip_chart_tpl', array(
					'userId' => $userId,
					'centerId' => $id,
					'certId' => $certId,
					'regFile' => $regFile,
					'startDate' => $startDate
				));
			}
        }
    }

    public function actionEquip($id, $certId = 0, $pageName = "", $pracId = 0, $layerNum = 0)
    {
        //Проверка авторизации
        if(Yii::app()->user->getIsGuest())
            $this->redirect(Yii::app()->homeUrl);

        Yii::app()->topMenu->setCenter($id);

        $userId = Yii::app()->user->getId();

        if($certId == 0)
            $this->render('equip_tpl', array(
                'userId' => $userId,
                'centerId' => $id
            ));
        else
        {
            $cEquip = CEquip::model()->findByPk($certId);

            if($pageName === "special") {
                $this->render('special_result_equip_tpl', array(
                    'userId' => $userId,
                    'centerId' => $id,
                    'certId' => $certId,
                    'cData' => $cEquip->getSpecialData(),
                    'vacData' => CEquipVac::getVac($certId)
                ));
            }
            else {

                if($pracId == 0)
                    $this->render('practical_result_equip_tpl', array(
                        'userId' => $userId,
                        'centerId' => $id,
                        'certId' => $certId,
                        'cData' => $cEquip->getPracticalData()
                    ));
                else
                {					
					if($layerNum == 0) {
						$cEquipPrac = CEquipPrac::model()->findByPk($pracId);
						
						$this->render('practical_result_params_equip_tpl', array(
							'userId' => $userId,
							'centerId' => $id,
							'certId' => $certId,
							'pracId' => $pracId,
							'cData' => $cEquipPrac->getData()
						));
					}
					else {
						$cParameter = CParameter::model()->findByPk($layerNum);
						$regFile = $cParameter->regFile;
						$startDate = strtotime($cParameter->startDate);
						
						$this->render('practical_result_params_equip_chart_tpl', array(
							'userId' => $userId,
							'centerId' => $id,
							'certId' => $certId,
							'pracId' => $pracId,
							'regFile' => $regFile,
							'startDate' => $startDate
						));
					}
                }
            }
        }
    }
    public function actionGetWeldersData()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $startPosition = $this->getParameter($postData, 'startPosition');
        $offset = $this->getParameter($postData, 'offset');

        $centerId = $this->getParameter($postData, 'centerId');
        $filter = $this->getParameter($postData, 'filter');

        $orderBy = $this->getParameter($postData, 'orderBy');
        $order = $this->getParameter($postData, 'order');

        echo CWelder::getWeldersData($centerId, $startPosition, $offset, $orderBy, $order, $filter);
    }

    public function actionGetWeldersDataCount()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $centerId =  $this->getParameter($postData, 'centerId');
        $filter = $this->getParameter($postData, 'filter');

        //$orderBy = $this->getParameter($postData, 'orderBy');
        //$order = $this->getParameter($postData, 'order');

        echo CWelder::getWeldersDataCount($centerId, $filter);
    }

    public function actionDeleteWeldersData()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $records = $this->getParameter($postData, 'records');

        echo CWelder::DeleteWeldersData($records);
    }

    public function actionGetTechData()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $startPosition = $this->getParameter($postData, 'startPosition');
        $offset = $this->getParameter($postData, 'offset');

        $centerId = $this->getParameter($postData, 'centerId');
        $filter = $this->getParameter($postData, 'filter');

        $orderBy = $this->getParameter($postData, 'orderBy');
        $order = $this->getParameter($postData, 'order');

        echo CTech::getTechData($centerId, $startPosition, $offset, $orderBy, $order, $filter);
    }

    public function actionGetTechDataCount()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $centerId =  $this->getParameter($postData, 'centerId');
        $filter = $this->getParameter($postData, 'filter');

        echo CTech::getTechDataCount($centerId, $filter);
    }

    public function actionDeleteTechData()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $records = $this->getParameter($postData, 'records');

        echo CTech::DeleteTechData($records);
    }

    public function actionGetEquipData()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $startPosition = $this->getParameter($postData, 'startPosition');
        $offset =  $this->getParameter($postData, 'offset');

        $centerId =  $this->getParameter($postData, 'centerId');
        $filter = $this->getParameter($postData, 'filter');

        $orderBy = $this->getParameter($postData, 'orderBy');
        $order = $this->getParameter($postData, 'order');

        echo CEquip::getEquipData($centerId, $startPosition, $offset, $orderBy, $order, $filter);
    }

    public function actionGetEquipDataCount()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $centerId = $this->getParameter($postData, 'centerId');
        $filter = $this->getParameter($postData, 'filter');

        echo CEquip::getEquipDataCount($centerId, $filter);
    }

    public function actionGetParameters()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);
        $certId =  $this->getParameter($postData, 'certId');
        $certType = $this->getParameter($postData, 'certType');

        echo CParameter::getParameters($certId, $certType);
    }

    public function actionGetEquipPracData()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $startPosition = $this->getParameter($postData, 'startPosition');
        $offset = $this->getParameter($postData, 'offset');

        $pracId = $this->getParameter($postData, 'pracId');
        $filter = $this->getParameter($postData, 'filter');

        $orderBy = $this->getParameter($postData, 'orderBy');
        $order = $this->getParameter($postData, 'order');

        echo CEquipPrac::getEquipPracData($pracId, $startPosition, $offset, $orderBy, $order, $filter);
    }

    public function actionGetEquipPracDataCount()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $pracId =  $this->getParameter($postData, 'pracId');
        $filter = $this->getParameter($postData, 'filter');

        echo CEquipPrac::getEquipPracDataCount($pracId, $filter);
    }

    public function actionDeleteEquipData()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $records = $this->getParameter($postData, 'records');

        echo CEquip::DeleteEquipData($records);
    }

    public function actionError()
    {
        if(Yii::app()->user->getIsGuest())
            $this->redirect(Yii::app()->homeUrl);

        $user = User::model()->findByPk(Yii::app()->user->getId());
        $centerId = CCenter::model()->findByPk($user->centerId);

        if($centerId === null) {
            echo "Ошибка данных";
            Yii:die();
        }

        $this->render('error_tpl', array(
            'centerId' => $centerId->id,
            'userId' => $user->id
        ));
    }
}