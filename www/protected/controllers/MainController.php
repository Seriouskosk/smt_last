<?

class MainController extends  Controller
{
    public function actionIndex()
    {
        if(!Yii::app()->user->getIsGuest())
            $this->redirect(Yii::app()->createUrl('certification/all'));

        $this->render('index_tpl');
    }

    public function actionNews()
    {
        if(Yii::app()->user->getIsGuest() || !Yii::app()->user->checkAccess(User::ROLE_ADMIN))
            $this->redirect(Yii::app()->homeUrl);

        $this->render('news_tpl');
    }

    public function actionLogin()
    {
        if(!Yii::app()->user->getIsGuest())
            $this->redirect(Yii::app()->createUrl('certification/all'));

        $model = new LoginForm;

        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
                $this->redirect(Yii::app()->createUrl('certification/all'));
        }

        $this->render('index_tpl', array('loginError'=> true));
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionAddNews()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $newsTitle = $this->getParameter($postData, 'title');
        $newsText = $this->getParameter($postData, 'text');

        echo News::addNews($newsTitle, $newsText);
    }

    public function actionUpdateNews()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $id = (int)$this->getParameter($postData, 'id');
        $params = $this->getParameter($postData, 'params');

        echo News::updateNews($id, $params);
    }

    public function actionDeleteNews()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $records = $this->getParameter($postData, 'records');

        echo News::deleteNews($records);
    }

    public function actionGetNews()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $offset = $this->getParameter($postData, 'offset');
        $count = $this->getParameter($postData, 'count');

        echo News::getNews($offset, $count);
    }

    public function  actionGetNewsCount()
    {
        echo News::getNewsCount();
    }

    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
            $this->render('404error', $error);

    }
}