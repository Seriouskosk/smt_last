<?

class MessagesController extends  Controller
{
    public function actionIndex()
    {
        //Проверка авторизации
        if(Yii::app()->user->getIsGuest())
            $this->redirect(Yii::app()->homeUrl);

        $userId = Yii::app()->user->getId();

        //Проверка прав пользователя
        if(!Yii::app()->user->checkAccess(User::ROLE_ADMIN)) {
            $user = User::model()->findByPk(Yii::app()->user->getId());
            $this->redirect(Yii::app()->createUrl('certification/all/'));
        }

        $this->render('dialogs_tpl', array(
            'userId' => $userId
        ));
    }

    public function actionGet()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $userId = $this->getParameter($postData, 'userId');

        $startPosition = $this->getParameter($postData, 'startPosition');
        $offset =  $this->getParameter($postData, 'offset');

        echo Message::getMessages($userId, $startPosition, $offset);
    }

    public function actionSend()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $userId = $this->getParameter($postData, 'userId');

        $message = $this->getParameter($postData, 'message');
        $isAdmin = $this->getParameter($postData, 'isAdmin');

        echo Message::sendMessage($userId, $message, $isAdmin);
    }

    public function actionGetDialogs()
    {
        echo Message::getDialogs();
    }

    public function actionCheckMessages()
    {
        $postData = json_decode(trim(file_get_contents('php://input')), true);

        $userId = $this->getParameter($postData, 'userId');

        echo Message::checkMessages($userId);
    }
}