$zip = new ZipArchive();
$x = $zip->open($path);
if ($x === true) {
    $zip->extractTo($webRoot); // change this to the correct site path
    $zip->close();

    unlink($path);
}

$fileName = $cParameter->regFileUrl->getName() . '.csv';
$filePath = $webRoot . $fileName;

//VARIABLES
$tmpUavr=0; $tmpIavr=0; $tmpUstd=0; $tmpIstd=0;
$WorkSamplesCount=0; $OCSamplesCount=0;
$Uxx = 0; //$Uavr = 0; $Iavr = 0; $Istd = 0; $Ustd = 0;

$WorkPointsBuffer = array(); //$count = 0;

//CONSTANTS
$minCurrent = 5; $minVoltage = 2.5; $maxVoltage = 40;
$first = true; $second = false; $dt = 0; $weldTime = 0;
//echo $filePath;
//if(!file_exists($filePath))
    //$this->returnError("Файл не существует.");

//echo substr(sprintf('%o', fileperms($filePath)), -4);
//fopen($filePath, "r");
if (($handle = fopen($filePath, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 60, ";")) !== FALSE) {

        if($second) {
            $second = false;
            $dt = floatval(str_replace(",", ".", $data[2]));
        }

        if($first) {
            $first = false;
            $second = true;
            continue;
        }

        $i = floatval(str_replace(",", ".", $data[0]));//сила тока
        $u = floatval(str_replace(",", ".", $data[1]));//напряжение

        if ($i > $minCurrent && $u > $minVoltage && $u < $maxVoltage)
        {
            $WorkPointsBuffer[]= $i;
            $WorkPointsBuffer[]= $u;
            $WorkSamplesCount += 2;
            //echo "I: " . $i . ", U: " . $u;
        }
        else if($u > $maxVoltage)
        {
            $Uxx += $u;
            $OCSamplesCount++;
        }
    }
}
else $this->returnError("Невозможно открыть файл регистограммы на чтение.");

    fclose($handle);

    if($OCSamplesCount > 2)
        $Uxx /= $OCSamplesCount;

    if($WorkSamplesCount > 2) {

        $input = intval($WorkSamplesCount) * intval($dt);
        $input = floor($input / 1000);

        $hours = floor($input / 3600);
        $input -= 3600 * $hours;

        $min = floor($input / 60);
        $input -= 60 * $min;

        $seconds = $input;
        $cParameter->workTime = (intval($hours / 10) == 0 ? "0" : "") . $hours . ":" . (intval($min / 10) == 0 ? "0" : "") . $min . ":" . (intval($seconds / 10) == 0 ? "0" : "") . $seconds;

        //Calculate average values
        for ($j = 0; $j < $WorkSamplesCount; $j += 2) {
            $tmpIavr += $WorkPointsBuffer[$j];
            $tmpUavr += $WorkPointsBuffer[$j + 1];
        }

        $Devider = $WorkSamplesCount >> 1;

        $Iavr = ($tmpIavr / $Devider) + ($tmpIavr / 1000);
        $Uavr = ($tmpUavr / $Devider) + ($tmpUavr / 1000);

        for ($j = 0; $j < $WorkSamplesCount; $j += 2) {
            $tmpIstd += ($WorkPointsBuffer[$j] - $Iavr) * ($WorkPointsBuffer[$j] - $Iavr);
            $tmpUstd += ($WorkPointsBuffer[$j + 1] - $Uavr) * ($WorkPointsBuffer[$j + 1] - $Uavr);
        }

        $Istd = sqrt($tmpIstd / $WorkSamplesCount);
        $Ustd = sqrt($tmpUstd / $WorkSamplesCount);

        $Istd += $Istd * $Devider / 1000;
        $Ustd += $Ustd * $Devider / 1000;

        /*$cParameter->maxU = $_POST['maxU'];
        $cParameter->avrU = $_POST['avrU'];
        $cParameter->stdU = $_POST['stdU'];
        $cParameter->avrI = $_POST['avrI'];
        $cParameter->stdI = $_POST['stdI'];*/

        $cParameter->maxU = $Uxx;
        $cParameter->avrU = $Uavr;
        $cParameter->stdU = $Ustd;
        $cParameter->avrI = $Iavr;
        $cParameter->stdI = $Istd;
    


   
}