<?php

class CEscapeData
{
	private $data;

	public function __get($name)
	{
		if(isset($this->data[$name]))
			return $this->data[$name];
		else return "";
	}

	public function __set($name, $value)
	{
		$this->data[$name] = $value;
	}

	public function __isset($name)
	{
		return true;
	}

	public function __construct($data)
	{
		if(is_array($data) || is_string($data))
			$this->data = CText::esc($data);
		else $this->data = [];
	}

	public function getData()
	{
		return $this->data;
	}
}