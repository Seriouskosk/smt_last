<?php

class Validator extends CFormModel {
    public function __get($name) {
        return isset($_POST[$name]) ? $_POST[$name] : null;
    }

    public static function validateFilee($rule, &$error) {
        $obj = new Validator();

        //Задание правил происходит так же как и в модели
        if(isset($rule[0], $rule[1])) {
            $validator = CValidator::createValidator($rule[1], $obj, $rule[0], array_slice($rules, 2));
            $validator->validate($obj);
        }
        else throw new Exception("Ошибка задания правил валидации");
        

        //Если есть ошибка, выводим
        $errors = $obj->getError();

        return !$obj->hasErrors();
    }
}