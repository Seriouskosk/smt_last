<?php

class CText
{
	const MYSQL_TIME_FORMAT = 0;
	const RUS_TIME_FORMAT = 1;

	public static function now($format = self::MYSQL_TIME_FORMAT)
	{
		switch ($format) {
			case self::MYSQL_TIME_FORMAT:
				return date('Y-m-d H:i:s');
			
			case self::RUS_TIME_FORMAT:
				defaultFormat: return date('d.m.Y H:i:s');

			default:
				goto defaultFormat;
		}
	}

	public static function dateTime($date, $format = self::RUS_TIME_FORMAT)
	{
		$formatString = "";

		switch ($format) {
			case self::MYSQL_TIME_FORMAT:
				$formarString = 'Y-m-d H:i:s';
				break;
			
			case self::RUS_TIME_FORMAT:
				defaultFormat:
					$formarString = 'd.m.Y H:i:s';
					break;

			default:
				goto defaultFormat;
		}

		$dateTmp = strtotime($date);
		return date($formarString, $dateTmp);
	}

	public static function esc($data)
	{
		if(is_array($data)) {
			$escaped = array();
			foreach ($data as $key => $val)
				$escaped[$key] = self::esc($val); 
			return $escaped;
		}
		else if(is_string($data))
			return CHtml::encode(trim(strip_tags($data)));
		else return $data;
	}

	public static function dump($var)
	{
		ob_start();
		var_dump($var);
		return ob_get_clean();
	}

	public static function ip()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		    return $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		    return $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
		    return $_SERVER['REMOTE_ADDR'];
		}
	}

	public static function randString($length = 10)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
		    $randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	public static function translate($str) {
		$rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
		$lat = array('A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya');
		return str_replace($rus, $lat, $str);
	}
}