<?php

define('URL_SEPARATOR', '/');

class CUrl
{
	public static function get($path = '')
	{
		return Yii::app()->params['baseUrl'].URL_SEPARATOR.$path;
	}

	public static function getImage($path = '')
	{
		return CUrl::get('images'.URL_SEPARATOR.$path);
	}

	public static function getUrl($path = '', $params = array())
	{
		return Yii::app()->params['baseUrl'].Yii::app()->urlManager->createUrl($path, $params);
	}
}