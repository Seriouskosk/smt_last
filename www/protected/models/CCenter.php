<?

class CCenter extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'centers';
    }

    public function isOnline()
    {
        $query = "select * from users where centerId = " . $this->id . " and isOnline = 1";

        //Get the first row
        $online = Yii::app()->db->createCommand($query)->queryRow();

        if($online !== false)//if we got something
            return true;//set CCenter to online state
        else return false;//else CCenter is offline
    }

    public static function addCenter($name, $adress, $phone, $lon, $lat, $city, $weldersCode, $techCode, $equipCode)
    {
        try
        {
            if(!Yii::app()->user->checkAccess(User::ROLE_ADMIN))
                throw new Exception("У вас не хватает прав для совершения этого действия");

            if(empty($name))
                throw new Exception("Название аттестационного центра не может быть пустой строкой");

            if(empty($adress))
                throw new Exception("У аттестационного центра должен быть адрес.");

            if(empty($city))
                throw new Exception("Укажите город для аттестационного центра.");

            if(empty($weldersCode) && empty($techCode) && empty($equipCode))
                throw new Exception("Аттестационный центр должен иметь право совершать хотя-бы один вид испытаний.");

            $findCenterSql = "select * from centers where name = '" . $name . "' or adress = '" . $adress . "' limit 1";

            //Get centers with same parameters
            $centerExists = CCenter::model()->findBySql($findCenterSql);

            if(isset($centerExists))//If we got something
                throw new Exception("Уже существует юридическое лицо с таким названием или адресом.");//Then throw an error

            $center = new CCenter;

            $center->name = $name;
            $center->adress = $adress;
            $center->phone = $phone;
            $center->lon = $lon;
            $center->lat = $lat;
            $center->city = $city;

            $center->weldersCode = $weldersCode;
            $center->techCode = $techCode;
            $center->equipCode = $equipCode;

            $center->save();

            return CJSON::encode(array(
                'success' => true
            ));
        }
        catch(Exception $ex)
        {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => $ex->getMessage()
            ));
        }
    }

    public static function updateCenter($id, $params)
    {
        try
        {
            if(!Yii::app()->user->checkAccess(User::ROLE_ADMIN))
                throw new Exception("У вас не хватает прав для совершения этого действия");

            $center = CCenter::model()->findByPk($id);

            if(!isset($center))
                throw new Exception("Не существует объекта с указанным ID.");

            $center->name = $params['name'];
            $center->adress = $params['adress'];
            $center->phone = $params['phone'];
            $center->lon = $params['lon'];
            $center->lat = $params['lat'];
            $center->city = $params['city'];

            $center->weldersCode = $params['weldersCode'];
            $center->techCode = $params['techCode'];
            $center->equipCode = $params['equipCode'];

            $center->save();

            return CJSON::encode(array(
                'success' => true,
                'centerInfo' => $params
            ));
        }
        catch(Exception $ex)
        {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => $ex->getMessage()
            ));
        }
    }

    public static function getCenters($startPosition = 0, $offset = 10, $city, $orgName, $code)
    {
        if($startPosition == null)
            $startPosition = 0;

        if($offset == null)
            $offset = 10;

        $query = "select *
            from `centers` ";

        $query .= "where `isActive` = 1 ";

        if(isset($city) && !empty($city))
            $query .= "and `city` like '%$city%' ";

        if(isset($orgName) && !empty($orgName))
            $query .= "and `name` like '%$orgName%' ";

        if(isset($code) && !empty($code)) {

            if(strpos($code, "АЦСТ-") !== false or strpos($code, "АЦСО-") !== false)
                $code = mb_substr($code, 5, null, 'utf-8');
                //echo $code;
            $query .= "and (`weldersCode` = '$code' or `techCode` = '$code' or `equipCode` = '$code') ";
        }

        $query .= "limit $startPosition, $offset";
        //echo $query;
        $entries = CCenter::model()->findAllBySql($query);

        $centers = array();
        foreach($entries as $entry)
         $centers[] = array(
             'id' => $entry->id,
             'name' => $entry->name,
             'phone' => $entry->phone,
             'weldersCode' => $entry->weldersCode,
             'techCode' => $entry->techCode,
             'equipCode' => $entry->equipCode,
             'city' => $entry->city,
             'lon' => $entry->lon,
             'lat' => $entry->lat,
             'adress' => $entry->adress,
             'isOnline' => $entry->isOnline()
         );

        return CJSON::encode($centers);
    }

    public static function getCentersCount($city, $orgName, $code)
    {
        //$query = "select `centers`.*,concat(`centers`.`weldersCode`,'АЦСО-',`centers`.`equipCode`,'АЦСТ-',`centers`.`techCode`) as `code`
        $query = "select `centers`.*
			from `centers` ";

        $query .= "where `centers`.`isActive` = 1 ";

        if(isset($city) && !empty($city))
            $query .= "and `centers`.`city` like '%$city%' ";

        if(isset($orgName) && !empty($orgName))
            $query .= "and `name` like '%$orgName%' ";

        if(isset($code) && !empty($code))
            //$query .= "having `code` like '%$code%' ";
		    $query .= "and (`weldersCode` = '$code' or `techCode` = 'АЦСТ-$code' or `equipCode` = '$code') ";


        return CJSON::encode(array(
            'count' => CCenter::model()->countBySql($query)
        ));
    }

    public static function DeleteCenters($records)
    {
        try
        {
            if(!Yii::app()->user->checkAccess(User::ROLE_ADMIN))
                throw new Exception("У вас не хватает прав для совершения этого действия");

            foreach($records as $recordId)
            {
                $record = CCenter::model()->findByPk($recordId);
                $record->isActive = 0;
                $record->save();
            }

            return CJSON::encode(array(
                'success' => true
            ));
        }
        catch(Exception $ex)
        {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => $ex->getMessage()
            ));
        }
    }
}