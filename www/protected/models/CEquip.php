<?

class CEquip extends ActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'equip_att';
    }

    public function afterFind()
    {
        parent::afterFind();

        $this->isClosed = filter_var($this->isClosed, FILTER_VALIDATE_BOOLEAN);
        return true;
    }

    /*public function beforeSave()
    {
        parent::beforeSave();

        $this->isClosed = $this->isClosed ? 1 : 0;
        return true;
    }*/

    public function getSpecialData()
    {
        date_default_timezone_set('UTC');

        $special = CEquipSpec::model()->find("certId = :certId", array(':certId' => $this->id));

        $user = User::model()->findByPk($this->userId);
        $org = Organization::model()->findByPk($this->orgId);
        $center =  CCenter::model()->findByPk($this->centerId);

        if($user === null || $org === null || $center === null || $special === null) {
            header("Location: http://" . $_SERVER['SERVER_NAME'] . "/data/error/");
            die();
        }

        $userName = $user->name;
        $orgName = $org->name;
        $centerName = $center->name;

        $date = strtotime($special->startDate);
        $correctDate = date('d.m.Y в H:i:s', $date);

        $startDate = $correctDate;

        $date = strtotime($special->endDate);
        $correctDate = date('d.m.Y в H:i:s', $date);

        $endDate = $correctDate;

        $startDateTime = new DateTime($special->startDate);
        $endDateTime = new DateTime($special->endDate);

        $diff = $endDateTime->diff($startDateTime);
        $timeDiff = $diff->format('%H:%I:%S');

        return array(
            'weldMethod' => $this->weldMethod,
            'userName' => $userName,
            'centerName' => $centerName,
            'orgName' => $orgName,
            'programName' => $this->programName,
            'tradeMark' => $this->tradeMark,
            'factoryNum' => $this->factoryNum,
            'ivnNum' => $this->invNum,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'timeDiff' => $timeDiff,
            'lon' => $special->lon,
            'lat' => $special->lat,
            'minU' => $special->minU,
            'maxU' => $special->maxU,
            'minI' => $special->minI,
            'maxI' => $special->maxI,
            'normU' => $special->normU,
            'normI' => $special->normI
        );
    }

    public function getPracticalData()
    {
        $user = User::model()->findByPk($this->userId);
        $org = Organization::model()->findByPk($this->orgId);
        $center =  CCenter::model()->findByPk($this->centerId);

        if($user === null || $org === null || $center === null) {
            header("Location: http://" . $_SERVER['SERVER_NAME'] . "/data/error/");
            die();
        }

        $userName = $user->name;
        $orgName = $org->name;
        $centerName = $center->name;

        return array(
            'weldMethod' => $this->weldMethod,
            'userName' => $userName,
            'centerName' => $centerName,
            'orgName' => $orgName,
            'programName' => $this->programName,
            'tradeMark' => $this->tradeMark,
            'factoryNum' => $this->factoryNum,
            'ivnNum' => $this->invNum
        );
    }

    public static function getById($id)
    {
        return CEquip::model()->findByPk($id);
    }

    public static function getByParams($orgId, $tradeMark, $factoryNum, $invNum, $weldMethod, $programName)
    {
        $sql = "select * from equip_att " .
            "where orgId = :orgId and tradeMark = :tradeMark and factoryNum = :factoryNum " .
            "and invNum = :invNum and weldMethod = :weldMethod and programName = :programName " .
            "and isClosed = 0 order by id desc " .
            "limit 1";

        return CTech::model()->findBySql($sql, array(
                ':orgId' => $orgId,
                ':tradeMark' => $tradeMark,
                ':factoryNum' => $factoryNum,
                ':invNum' => $invNum,
                ':weldMethod' => $weldMethod,
                ':programName' => $programName
            ));
    }

    public static function getEquipData($centerId, $startPosition, $offset, $orderBy, $order,  $filter)
    {
        $userName = isset($filter['userName']) && !empty($filter['userName']) ? $filter['userName'] : null;
        $orgName = isset($filter['orgName']) && !empty($filter['orgName']) ? $filter['orgName'] : null;
        $tradeMark = isset($filter['tradeMark']) && !empty($filter['tradeMark']) ? $filter['tradeMark'] : null;
        $factoryNum = isset($filter['factoryNum']) && !empty($filter['factoryNum']) ? $filter['factoryNum'] : null;
        $invNum = isset($filter['invNum']) && !empty($filter['invNum']) ? $filter['invNum'] : null;
        $programName = isset($filter['programName']) && !empty($filter['programName']) ? $filter['programName'] : null;
        $weldMethod = isset($filter['weldMethod']) && !empty($filter['weldMethod']) ? $filter['weldMethod'] : null;

        $startDate = isset($filter['startDate'])  && !empty($filter['startDate']) ? $filter['startDate'] : null;
        $endDate = isset($filter['endDate'])  && !empty($filter['endDate']) ? $filter['endDate'] : null;

        $query = "SELECT `equip_att`.*, `users`.`name` AS `userName`, `organizations`.`name` as `orgName` FROM `equip_att`
            LEFT JOIN `users` ON `users`.`id` = `equip_att`.`userId`
            LEFT JOIN `organizations` ON `organizations`.`id` = `equip_att`.`orgId`
            WHERE `equip_att`.`centerId` = " . $centerId . " ";

        if(isset($userName))
            $query .= "AND `users`.`name` LIKE '%$userName%' ";

        if(isset($orgName))
            $query .= "AND `organizations`.`name` LIKE '%$orgName%' ";

        if(isset($programName))
            $query .= "AND `equip_att`.`programName` LIKE '%$programName%' ";

        if(isset($weldMethod))
            $query .= "AND `equip_att`.`weldMethod` LIKE '%$weldMethod%' ";

        if(isset($tradeMark))
            $query .= "AND `equip_att`.`tradeMark` LIKE '%$tradeMark%' ";

        if(isset($factoryNum))
            $query .= "AND `equip_att`.`factoryNum` LIKE '%$factoryNum%' ";

        if(isset($invNum))
            $query .= "AND `equip_att`.`invNum` LIKE '%$invNum%' ";

        if(isset($startDate))
            $query .= "AND `equip_att`.`startDate` >= '$startDate  00:00:00' ";

        if(isset($endDate))
            $query .= "AND `equip_att`.`endDate` <= '$endDate  23:59:59' ";


        if(!Yii::app()->user->checkAccess(User::ROLE_MODER))
            $query .= "AND `equip_att`.`userId` = " . Yii::app()->user->getId() . " ";

        if(isset($orderBy) && !empty($orderBy))
            $query .= "ORDER BY `" . $orderBy . "` " . ($order == "true" ? "DESC " : "ASC ");

        $query .= "LIMIT $startPosition, $offset";
        //echo $query;
        $entries = Yii::app()->db->createCommand($query)->queryAll();
        return CJSON::encode($entries);
    }

    public static  function getEquipDataCount($centerId,  $filter)
    {
        $userName = isset($filter['userName']) && !empty($filter['userName']) ? $filter['userName'] : null;
        $orgName = isset($filter['orgName']) && !empty($filter['orgName']) ? $filter['orgName'] : null;
        $tradeMark = isset($filter['tradeMark']) && !empty($filter['tradeMark']) ? $filter['tradeMark'] : null;
        $factoryNum = isset($filter['factoryNum']) && !empty($filter['factoryNum']) ? $filter['factoryNum'] : null;
        $invNum = isset($filter['invNum']) && !empty($filter['invNum']) ? $filter['invNum'] : null;
        $programName = isset($filter['programName']) && !empty($filter['programName']) ? $filter['programName'] : null;
        $weldMethod = isset($filter['weldMethod']) && !empty($filter['weldMethod']) ? $filter['weldMethod'] : null;

        $startDate = isset($filter['startDate'])  && !empty($filter['startDate']) ? $filter['startDate'] : null;
        $endDate = isset($filter['endDate'])  && !empty($filter['endDate']) ? $filter['endDate'] : null;

        $query = "SELECT COUNT(*) as `count` FROM `equip_att`
            LEFT JOIN `users` ON `users`.`id` = `equip_att`.`userId`
            LEFT JOIN `organizations` ON `organizations`.`id` = `equip_att`.`orgId`
            WHERE `equip_att`.`centerId` = " . $centerId . " ";

        if(isset($userName))
            $query .= "AND `users`.`name` LIKE '%$userName%' ";

        if(isset($orgName))
            $query .= "AND `orgName` LIKE '%$orgName%' ";

        if(isset($programName))
            $query .= "AND `equip_att`.`programName` LIKE '%$programName%' ";

        if(isset($weldMethod))
            $query .= "AND `equip_att`.`weldMethod` LIKE '%$weldMethod%' ";

        if(isset($tradeMark))
            $query .= "AND `equip_att`.`tradeMark` LIKE '%$tradeMark%' ";

        if(isset($factoryNum))
            $query .= "AND `equip_att`.`factoryNum` LIKE '%$factoryNum%' ";

        if(isset($invNum))
            $query .= "AND `equip_att`.`invNum` LIKE '%$invNum%' ";

        if(isset($startDate))
            $query .= "AND `welder_att`.`startDate` >= '$startDate  00:00:00' ";

        if(isset($endDate))
            $query .= "AND `welder_att`.`endDate` <= '$endDate  23:59:59' ";

        if(!Yii::app()->user->checkAccess(User::ROLE_MODER))
            $query .= "AND `equip_att`.`userId` = " . Yii::app()->user->getId() . " ";

        $count = Yii::app()->db->createCommand($query)->queryRow();
        return CJSON::encode(array(
            'count' => intval($count['count'])
        ));
    }

    public static function DeleteEquipData($records)
    {
        try
        {
            if(!Yii::app()->user->checkAccess(User::ROLE_ADMIN))
                throw new Exception("У вас не хватает прав для совершения этого действия");

            foreach($records as $recordId)
            {
                $record = CEquip::model()->findByPk($recordId);
                $record->delete();

                CParameter::model()->deleteAll('certId = :certId', array(':certId' => $recordId));
                CEquipPrac::model()->deleteAll('pracId = :pracId', array(':pracId' => $recordId));
                CEquipSpec::model()->deleteAll('certId = :certId', array(':certId' => $recordId));
                CEquipVac::model()->deleteAll('certId = :certId', array(':certId' => $recordId));
            }

            return CJSON::encode(array(
                'success' => true
            ));
        }
        catch(Exception $ex)
        {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => $ex->getMessage()
            ));
        }
    }
}
