<?

class CEquipPrac extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'equip_kcc';
    }

    public function getData()
    {
        date_default_timezone_set('UTC');

        $cEquip = CEquip::model()->findByPk($this->pracId);

        $userName = User::model()->findByPk($cEquip->userId)->name;
        $orgName = Organization::model()->findByPk($cEquip->orgId)->name;
        $centerName = CCenter::model()->findByPk($cEquip->centerId)->name;

        $date = strtotime($this->startDate);
        $correctDate = date('d.m.Y в H:i:s', $date);

        $startDate = $correctDate;

        $date = strtotime($this->endDate);
        $correctDate = date('d.m.Y в H:i:s', $date);

        $endDate = $correctDate;

        $startDateTime = new DateTime($this->startDate);
        $endDateTime = new DateTime($this->endDate);

        $diff = $endDateTime->diff($startDateTime);
        $timeDiff = $diff->format('%H:%I:%S');

        //$timeAdditional = date('H:i:s', strtotime($timeDiff) - strtotime($this->time) + strtotime("1969-12-31 24:00:00"));

        $kccSize = "";
        if(floatval($this->dFirst) == 0)
            $kccSize .= "S" . $this->hFirst . "+";
        else $kccSize .= "D" . $this->dFirst . "x" . $this->hFirst . "+";

        if(floatval($this->dSecond) == 0)
            $kccSize .= "S" . $this->hSecond;
        else $kccSize .= "D" . $this->dSecond . "x" . $this->hSecond;

        $cParameters = CParameter::model()->findAll("certId = :certId", array(':certId' => $this->id));
        $weldTime = "00:00:00";

        foreach($cParameters as $cParameter)
        {
            $secs = strtotime($weldTime) - strtotime("00:00:00");
            $base = strtotime($cParameter->workTime);

            $weldTime = date("H:i:s", $base + $secs);
        }
		
		$timeAdditional = date('H:i:s', strtotime($timeDiff) - strtotime($weldTime) + strtotime("1969-12-31 24:00:00"));


        return array(
            'id' => $this->id,
            'workerName' => $this->workerName,
            'userName' => $userName,
            'orgName' => $orgName,
            'centerName' => $centerName,
            'tradeMark' => $cEquip->tradeMark,
            'workerStump' => $this->workerStump,
            'programName' => $cEquip->programName,
            'weldMethod' => $cEquip->weldMethod,
            'kccType' => $this->kccType,
            'kccSize' => $kccSize,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'timeDiff' => $timeDiff,
            'weldTime' => $weldTime,
            'additionalTime' => $timeAdditional,
            'jointType' => $this->jointType,
            'layerNorm' => $this->layerNorm,
            'layerCount' => $this->layerCount,
            'lon' => $this->lon,
            'lat' => $this->lat,
            'photoFirst' => $this->photoFirst,
            'photoSecond' => $this->photoSecond,
            'finishPhoto' => $this->finishPhoto,
            'invNum' => $cEquip->invNum,
            'factoryNum' => $cEquip->factoryNum
        );
    }

    public static function getLastByEquipId($id)
    {
        $sql = "select * from equip_kcc where pracId = " . CText::esc($id) . "  order by id desc limit 1";
        CEquipPrac::model()->findBySql($sql);
    }

    public static function getById($id)
    {
        return CEquipPrac::model()->findByPk($id);
    }

    public static function getEquipPracData($pracId, $startPosition, $offset, $orderBy, $order,  $filter)
    {
        $kccType = isset($filter['kccType']) && !empty($filter['kccType']) ? $filter['kccType'] : null;
        $workerName = isset($filter['workerName']) && !empty($filter['workerName']) ? $filter['workerName'] : null;
        $workerStump = isset($filter['workerStump']) && !empty($filter['workerStump']) ? $filter['workerStump'] : null;

        $startDate = isset($filter['startDate'])  && !empty($filter['startDate']) ? $filter['startDate'] : null;
        $endDate = isset($filter['endDate'])  && !empty($filter['endDate']) ? $filter['endDate'] : null;

        $query = "SELECT `equip_kcc`.* FROM `equip_kcc`
            WHERE `equip_kcc`.`pracId` = " . $pracId . " AND  `equip_kcc`.`isClosed` = 1 ";

        if(isset($kccType))
            $query .= "AND `equip_kcc`.`kccType` LIKE '%$kccType%' ";

        if(isset($workerName))
            $query .= "AND `equip_kcc`.`workerName` LIKE '%$workerName%' ";

        if(isset($workerStump))
            $query .= "AND `equip_kcc`.`workerStump` LIKE '%$workerStump%' ";

        if(isset($startDate))
            $query .= "AND `welder_att`.`startDate` >= '$startDate  00:00:00' ";

        if(isset($endDate))
            $query .= "AND `welder_att`.`endDate` <= '$endDate  23:59:59' ";

        if(isset($orderBy) && !empty($orderBy))
            $query .= "ORDER BY `" . $orderBy . "` " . ($order == "true" ? "DESC " : "ASC ");

        $query .= "LIMIT $startPosition, $offset";

        $data = Yii::app()->db->createCommand($query)->queryAll();

        for($i = 0; $i < count($data); $i++) {

            $data[$i]['startDate'] = CText::dateTime($data[$i]['startDate']);
            $data[$i]['endDate'] = CText::dateTime($data[$i]['endDate']);
        }
        return CJSON::encode($data);
    }

    public static function getEquipPracDataCount($pracId,  $filter)
    {
        $kccType = isset($filter['kccType']) && !empty($filter['kccType']) ? $filter['kccType'] : null;
        $workerName = isset($filter['workerName']) && !empty($filter['workerName']) ? $filter['workerName'] : null;
        $workerStump = isset($filter['workerStump']) && !empty($filter['workerStump']) ? $filter['workerStump'] : null;

        $startDate = isset($filter['startDate'])  && !empty($filter['startDate']) ? $filter['startDate'] : null;
        $endDate = isset($filter['endDate'])  && !empty($filter['endDate']) ? $filter['endDate'] : null;

        $query = "SELECT COUNT(*) as `count` FROM `equip_kcc`
            WHERE `equip_kcc`.`pracId` = " . $pracId . " AND  `equip_kcc`.`isClosed` = 1 ";

        if(isset($kccType))
            $query .= "AND `equip_kcc`.`kccType` LIKE '%$kccType%' ";

        if(isset($workerName))
            $query .= "AND `equip_kcc`.`workerName` LIKE '%$workerName%' ";

        if(isset($workerStump))
            $query .= "AND `equip_kcc`.`workerStump` LIKE '%$workerStump%' ";

        if(isset($startDate))
            $query .= "AND `welder_att`.`startDate` >= '$startDate  00:00:00' ";

        if(isset($endDate))
            $query .= "AND `welder_att`.`endDate` <= '$endDate  23:59:59' ";

        $count = Yii::app()->db->createCommand($query)->queryRow();
        return CJSON::encode(array(
            'count' => intval($count['count'])
        ));
    }

    public static function renderPdf($certId)
    {
        $cEquipPrac = CEquipPrac::model()->findByPk($certId);
        $cData = $cEquipPrac->getData();

        $html = "<h1>Отчет по практическим испытаниям оборудования:</h1><table>";

        $html .= "<tr><td>Организация:</td><td>" . $cData['orgName'] . "</td><td rowspan='16' class='map'>";
        $html .= "<img src='http://static-maps.yandex.ru/1.x/?size=420,420&amp;z=15&amp;l=map&amp;pt=".$cData['lon'].",".$cData['lat'].",pmwtm1' width='420'></td></tr>";
        $html .= "<tr><td>Член комиссии:</td><td>" . $cData['userName'] . "</td></tr>";
        $html .= "<tr><td>ФИО сварщика:</td><td>" . $cData['workerName'] . "</td></tr>";
        $html .= "<tr><td>Шифр аттестационого центра:</td><td>" . $cData['centerName'] . "</td></tr>";
        $html .= "<tr><td>Номер программы:</td><td>" . $cData['programName'] . "</td></tr>";
        $html .= "<tr><td>Вид соединения:</td><td>" . $cData['jointType'] . "</td></tr>";
        $html .= "<tr><td>Тип КСС:</td><td>" . $cData['kccType'] . "</td></tr>";
        $html .= "<tr><td>Размеры КСС:</td><td>" . $cData['kccSize'] . "</td></tr>";
        $html .= "<tr><td>Марка оборудования:</td><td>" . $cData['tradeMark'] . "</td></tr>";
        $html .= "<tr><td>Заводской номер:</td><td>" . $cData['factoryNum'] . "</td></tr>";
        $html .= "<tr><td>Инвертарный номер:</td><td>" . $cData['invNum'] . "</td></tr>";
        $html .= "<tr><td>Шифр способа сварки:</td><td>" . $cData['weldMethod'] . "</td></tr>";
        $html .= "<tr><td>Аттестация начата:</td><td>" . $cData['startDate'] . "</td></tr>";
        $html .= "<tr><td>Аттестация закончена:</td><td>" . $cData['endDate'] . "</td></tr>";
        $html .= "<tr><td>Общее время аттестации:</td><td>" . $cData['timeDiff'] . "</td></tr>";
        $html .= "<tr><td>Время горения дуги:</td><td>" . $cData['weldTime'] . "</td></tr>";
        $html .= "<tr><td>Время вспомогательных операций:</td><td>" . $cData['additionalTime'] . "</td></tr>";
        $html .= "<tr><td>Нормативное кол-во слоев:</td><td>" . $cData['layerNorm'] . "</td></tr>";
        $html .= "<tr><td>Фактичекое кол-во слоев:</td><td>" . $cData['layerCount'] . "</td></tr>";
        $html .= "<tr><td>GPS координаты (долгота, широта):</td><td>" . $cData['lon'] . ", " . $cData['lat'] . "</td></tr>";

        $html .= "</table>";

        $stylesheet = "table {font-size: 15px; font-family: Georgia, sans-serif;}";
        $stylesheet .= "table#params {font-size: 15px; margin-top: 20px;}";
        $stylesheet .= "table td.map {padding-left: 60px;}";
        $stylesheet .= "table#params tr.color {background-color: #dbeaf0;}";
        $stylesheet .= "table#params tr.color td {width: 11%; border: 1px solid #000;}";
        $stylesheet .= "table#params td {width: 11%; text-align: center;}";
        $stylesheet .= "table#params td.layer {text-align: center;  border: 1px solid #000; background-color: #f4f4f4; padding: 10px 0px;}";
        $stylesheet .= "h1 {font-size: 24px; text-align: center; display: block;}";

        //echo CParameter::getParameters($certId, 0);

        include(Yii::getPathOfAlias('webroot') . "/mpdf60/mpdf.php");

        $mpdf = new mPDF('utf-8', 'A4-L', '8', '', 10, 10, 7, 7, 10, 10); /*задаем формат, отступы и.т.д.*/
        $mpdf->charset_in = 'utf-8'; /*не забываем про русский*/

        $mpdf->list_indent_first_level = 0;
        $mpdf->WriteHTML($stylesheet,1);
        $mpdf->WriteHTML($html, 2); /*формируем pdf*/

        $html = "";
        $html .= "<table id='params' width='100%'><tr class='color'>";
        $html .= "<td>Начало</td>";
        $html .= "<td>Конец</td>";
        $html .= "<td>Время</td>";
        $html .= "<td>Позиция</td>";
        $html .= "<td>Uxx</td>";
        $html .= "<td>Uср</td>";
        $html .= "<td>Uоткл</td>";
        $html .= "<td>Iср</td>";
        $html .= "<td>Iоткл</td>";
        $html .= "</tr>";

        $parameters = CJSON::decode(CParameter::getParameters($certId, 2));
        foreach($parameters as $key => $layers)
        {
            if(intval($key) == 0)
                $html .= "<tr><td class='layer' colspan='9'>Прихватка</td></tr><tr>";
            else $html .= "<tr><td class='layer' colspan='9'>Слой номер $key</td></tr><tr>";


            foreach($layers as $layer) {
                $html .= "<tr>";
                $html .= "<td>" . $layer["startDate"] . "</td>";
                $html .= "<td>" . $layer["endDate"] . "</td>";
                $html .= "<td>" . $layer["workTime"] . "</td>";
                $html .= "<td>" . $layer["position"] . "</td>";
                $html .= "<td>" . $layer["maxU"] . "</td>";
                $html .= "<td>" . $layer["avrU"] . "</td>";
                $html .= "<td>" . $layer["stdU"] . "</td>";
                $html .= "<td>" . $layer["avrI"] . "</td>";
                $html .= "<td>" . $layer["stdI"] . "</td>";
                $html .= "</tr>";
            }


        }
        $html .= "</table>";
        $mpdf->WriteHTML($html, 2); /*формируем pdf*/

        return $mpdf->Output('mpdf.pdf', 'I');
    }
}