<?

class CEquipVac extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'equip_vac';
    }

    public static function getVac($certId)
    {
        $vacMinData = CEquipVac::model()->findAllBySql("
            SELECT * FROM `equip_vac` WHERE `certId` = $certId AND `mode` = 0 ORDER BY `i` ASC
        ");

        $vacNormData = CEquipVac::model()->findAllBySql("
            SELECT * FROM `equip_vac` WHERE `certId` = $certId AND `mode` = 1 ORDER BY `i` ASC
        ");

        $vacMaxData = CEquipVac::model()->findAllBySql("
            SELECT * FROM `equip_vac` WHERE `certId` = $certId AND `mode` = 2 ORDER BY `i` ASC
        ");

        return array(
            'minMode' => $vacMinData,
            'normMode' => $vacNormData,
            'maxMode' => $vacMaxData
        );
    }
}