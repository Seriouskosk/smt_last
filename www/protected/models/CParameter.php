<?

class CParameter extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'params';
    }

    public static  function getParameters($id, $type)
    {
        $id = empty($id) ? 0 : intval($id);
        $type = empty($type) ? 0 : intval($type);

        $query = Yii::app()->db->createCommand("select * from params where certId =" . $id . " and certType = " . $type . " order by layer asc");
        $entries = $query->queryAll();

        $parameters = array();
        foreach($entries as $entry)
        {
            $date = strtotime($entry['startDate']);
            $correctDate = date('d.m.Y в H:i:s', $date);

            $startDate = $correctDate;

            $date = strtotime($entry['endDate']);
            $correctDate = date('d.m.Y в H:i:s', $date);

            $endDate = $correctDate;

            $position = "Не указано";
            switch(intval($entry['position']))
            {
                case 1:
                    $position = 'Нижнее';
                    break;
                case 2:
                    $position = 'На спуск';
                    break;
                case 3:
                    $position = 'Потолочное';
                    break;
                case 4:
                    $position = 'На подъем';
                    break;
                case 5:
                    $position = 'Горизонтальное';
            }


            $parameters[intval($entry['layer'])][] = array(
				'id' => $entry['id'],
                'startDate' => $startDate,
                'endDate' => $endDate,
                'workTime' => $entry['workTime'],
                'position' => $position,
                'maxU' => $entry['maxU'],
                'avrU' => $entry['avrU'],
                'stdU' => $entry['stdU'],
                'avrI' => $entry['avrI'],
                'stdI' => $entry['stdI'],
                'regFileUrl' => $entry['regFileUrl'],
                'photo' => $entry['photo'],
                'processed' => $entry['processed']
            );
        }

        return CJSON::encode($parameters);
    }
}
