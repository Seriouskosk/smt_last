<?

class CSession extends ActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'authHistory';
    }

    public static function createDetskopSession($userId)
    {
        $session = new CSession;

        $session->userId = $userId;
        $session->dateTime = CText::now(CText::MYSQL_TIME_FORMAT);
        //Так как мы не можем точно отследить когда выходил пользователь
        $session->logoutTime = $session->dateTime;

        //Это значит, что пользователь входил с компьютера
        $session->deviceType = 0;

        //Адресом устройства будет IP компьютера
        $session->deviceAdress = CText::ip();
        $session->save();
    }

    public static function createApiSession($userId, $deviceAdress)
    {
        $session = new CSession;

        $session->userId = $userId;
        $session->dateTime = CText::now(CText::MYSQL_TIME_FORMAT);
        //$session->logoutTime = "0000-00-00 00:00:00"

        //Это значит, что пользователь входил с API
        $session->deviceType = 1;
        $session->deviceAdress = $deviceAdress;
        $session->save();
    }

    public static  function closeApiSession($userId)
    {
        $sql = "select * from authHistory " . 
            "where userId = :userId and logoutTime = '0000-00-00 00:00:00' " . 
            "order by dateTime desc";

        //Находим последнюю незакрытую сессию API этого пользователя
        $session = CSession::model()->findBySql($sql, [':userId' => $userId]);

        //Если нашли
        if($session !== null)
        {
            //Устанавливаем время выхода и сохраняем
            $session->logoutTime = CText::now(CText::MYSQL_TIME_FORMAT);
            $session->save();
        } else return;//Если не нашли выходим по тихому, пока ничего не натворили

        /*
         *  Немного защитимся от сбоев, если у пользователя есть незакрытые API сессии   
         *  то удалим их, чтобы не мешали.
        */
        $sql = "select * from authHistory " .
            "where userId = :userId and logoutTime = '0000-00-00 00:00:00'";

        $trobleSessions = CSession::model()->findAllBySql($sql, [':userId' => $userId]);
        foreach($trobleSessions as $troubleSession)
            //Удаляем ненужные сессии
            $troubleSession->delete();
    }

    public static function getAuthSessions($centerId, $start = 0, $offset = 10)
    {
        $start = empty($start) ? 0 : intval($start);
        $offset = empty($offset) ? self::ROWS_PER_PAGE : intval($offset);

        $sql = "select t.*, u.name from authHistory t " . 
            "left join users u on (t.userId = u.id) " . 
            "where t.userId in " . 
                "(select id from users where centerId = " . CText::esc($centerId) . ") " .
            "and t.logoutTime <> '0000-00-00 00:00:00' " .
            "order by t.dateTime desc " .
            "limit " . $start . "," . $offset;

        $sessions = Yii::app()->db->createCommand($sql)->queryAll();
        foreach($sessions as $key => $session){
            $sessions[$key]['dateTime'] = CText::dateTime($session['dateTime']);
            $sessions[$key]['logoutTime'] = CText::dateTime($session['logoutTime']);
        }

        return CJSON::encode($sessions);
    }

    public static function getAuthSessionsCount($centerId)
    {

        $sql = "select COUNT(*) as count from authHistory t " .
            "where t.userId in " . 
                "(select id from users where centerId = " . CText::esc($centerId) . ") " .
            "and t.logoutTime <> '0000-00-00 00:00:00' ";

        $data = Yii::app()->db->createCommand($sql)->queryRow();
        return CJSON::encode( ['count' => intval($data['count'])] );
    }
}