<?

class CWelder extends ActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'welder_att';
    }

    public function afterFind()
    {
        parent::afterFind();

        $this->isClosed = filter_var($this->isClosed, FILTER_VALIDATE_BOOLEAN);
        return true;
    }

    public function getData()
    {
        date_default_timezone_set('UTC');

        $user = User::model()->findByPk($this->userId);
        $center =  CCenter::model()->findByPk($this->centerId);

        if($user === null || $center === null) {
            header("Location: http://" . $_SERVER['SERVER_NAME'] . "/data/error/");
            die();
        }

        $userName = $user->name;
        $centerName = $center->name;

        $date = strtotime($this->startDate);
        $correctDate = date('d.m.Y в H:i:s', $date);

        $startDate = $correctDate;

        $date = strtotime($this->endDate);
        $correctDate = date('d.m.Y в H:i:s', $date);

        $endDate = $correctDate;

        $startDateTime = new DateTime($this->startDate);
        $endDateTime = new DateTime($this->endDate);

        $diff = $endDateTime->diff($startDateTime);
        $timeDiff = $diff->format('%H:%I:%S');

        //$timeAdditional = date('H:i:s', strtotime($timeDiff) - strtotime($this->time) + strtotime("1969-12-31 24:00:00"));

        $kccSize = "";
        if(floatval($this->dFirst) == 0)
            $kccSize .= "S" . $this->hFirst . "+";
        else $kccSize .= "D" . $this->dFirst . "x" . $this->hFirst . "+";

        if(floatval($this->dSecond) == 0)
            $kccSize .= "S" . $this->hSecond;
        else $kccSize .= "D" . $this->dSecond . "x" . $this->hSecond;

        $cParameters = CParameter::model()->findAll("certId = :certId", array(':certId' => $this->id));
        $weldTime = "00:00:00";

        foreach($cParameters as $cParameter)
        {
            $secs = strtotime($weldTime) - strtotime("00:00:00");
            $base = strtotime($cParameter->workTime);

            $weldTime = date("H:i:s", $base + $secs);
        }
		
		$timeAdditional = date('H:i:s', strtotime($timeDiff) - strtotime($weldTime) + strtotime("1969-12-31 24:00:00"));


        return array(
            'id' => $this->id,
            'workerName' => $this->workerName,
            'userName' => $userName,
            'centerName' => $centerName,
            'punktName' => $this->punktName,
            'workerStump' => $this->workerStump,
            'techCardNum' => $this->techCardNum,
            'weldMethod' => $this->weldMethod,
            'kccSize' => $kccSize,
            'material' => $this->material,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'timeDiff' => $timeDiff,
            'weldTime' => $weldTime,
            'additionalTime' => $timeAdditional,
            'jointType' => $this->jointType,
            'layerNorm' => $this->layerNorm,
            'layerCount' => $this->layerCount,
            'lon' => $this->lon,
            'lat' => $this->lat,
            'photoFirst' => $this->photoFirst,
            'photoSecond' => $this->photoSecond,
            'finishPhoto' => $this->finishPhoto
        );
    }

    public static function getLastByUserId($userId)
    {
        $sql = "select * from welder_att " .
            "where userId = :userId " .
            "order by id desc " .
            "limit 1";

        return CWelder::model()->findBySql($sql, [':userId' => $userId]);
    }

    public static function getById($id)
    {
        return CWelder::model()->findByPk($id);
    }

    public static function getWeldersData($centerId, $startPosition, $offset, $orderBy, $order,  $filter)
    {
        $userName = isset($filter['userName']) && !empty($filter['userName']) ? $filter['userName'] : null;
        $workerName = isset($filter['workerName']) && !empty($filter['workerName']) ? $filter['workerName'] : null;
        $punktName = isset($filter['punktName']) && !empty($filter['punktName']) ? $filter['punktName'] : null;
        $workerStump = isset($filter['workerStump']) && !empty($filter['workerStump']) ? $filter['workerStump'] : null;
        $weldMethod = isset($filter['weldMethod'])  && !empty($filter['weldMethod']) ? $filter['weldMethod'] : null;
        $material = isset($filter['material']) && !empty($filter['material']) ? $filter['material'] : null;
        $techCardNum = isset($filter['techCardNum']) && !empty($filter['techCardNum']) ? $filter['techCardNum'] : null;

        $startDate = isset($filter['startDate'])  && !empty($filter['startDate']) ? $filter['startDate'] : null;
        $endDate = isset($filter['endDate'])  && !empty($filter['endDate']) ? $filter['endDate'] : null;

        $query = "SELECT `welder_att`.*, `users`.`name` AS `userName` FROM `welder_att`
            LEFT JOIN `users` ON `users`.`id` = `welder_att`.`userId`
            WHERE `welder_att`.`centerId` = " . $centerId . " AND  `welder_att`.`isClosed` = 1 ";

        if(isset($userName))
            $query .= "AND `users`.`name` LIKE '%" . $userName . "%' ";

        if(isset($workerName))
            $query .= "AND `welder_att`.`workerName` LIKE '%" . $workerName . "%' ";

        if(isset($punktName))
            $query .= "AND `welder_att`.`punktName` LIKE '%" . $punktName . "%' ";

        if(isset($workerStump))
            $query .= "AND `welder_att`.`workerStump` LIKE '%" . $workerStump . "%' ";

        if(isset($weldMethod))
            $query .= "AND `welder_att`.`weldMethod` LIKE '%" . $weldMethod . "%' ";

        if(isset($material))
            $query .= "AND `welder_att`.`material` LIKE '%" . $material . "%' ";

        if(isset($techCardNum))
            $query .= "AND `welder_att`.`techCardNum` LIKE '%" . $techCardNum . "%' ";

        if(isset($startDate))
            $query .= "AND `welder_att`.`startDate` >= '" . $startDate . "  00:00:00' ";

        if(isset($endDate))
            $query .= "AND `welder_att`.`endDate` <= '" . $endDate . "  23:59:59' ";

        if(!Yii::app()->user->checkAccess(User::ROLE_MODER))
            $query .= "AND `welder_att`.`userId` = " . Yii::app()->user->getId() . " ";

        if(isset($orderBy) && !empty($orderBy))
            $query .= "ORDER BY `" . $orderBy . "` " . ($order == "true" ? "DESC " : "ASC ");

        $query .= "LIMIT $startPosition, $offset";

        //echo $query;
        $data = Yii::app()->db->createCommand($query)->queryAll();
        
        for($i = 0; $i < count($data); $i++) {

            $data[$i]['startDate'] = CText::dateTime($data[$i]['startDate']);
            $data[$i]['endDate'] = CText::dateTime($data[$i]['endDate']);
        }

        return CJSON::encode($data);
    }

    public static  function getWeldersDataCount($centerId, $filter)
    {
        $userName = isset($filter['userName']) && !empty($filter['userName']) ? $filter['userName'] : null;
        $workerName = isset($filter['workerName']) && !empty($filter['workerName']) ? $filter['workerName'] : null;
        $punktName = isset($filter['punktName']) && !empty($filter['punktName']) ? $filter['punktName'] : null;
        $workerStump = isset($filter['workerStump']) && !empty($filter['workerStump']) ? $filter['workerStump'] : null;
        $weldMethod = isset($filter['weldMethod'])  && !empty($filter['weldMethod']) ? $filter['weldMethod'] : null;
        $material = isset($filter['material']) && !empty($filter['material']) ? $filter['material'] : null;
        $techCardNum = isset($filter['techCardNum']) && !empty($filter['techCardNum']) ? $filter['techCardNum'] : null;

        $startDate = isset($filter['startDate'])  && !empty($filter['startDate']) ? $filter['startDate'] : null;
        $endDate = isset($filter['endDate'])  && !empty($filter['endDate']) ? $filter['endDate'] : null;

        $query = "SELECT COUNT(*) as `count` FROM `welder_att`
            LEFT JOIN `users` ON `users`.`id` = `welder_att`.`userId`
            WHERE `welder_att`.`centerId` = " . $centerId . " AND  `welder_att`.`isClosed` = 1 ";

        if(isset($userName))
            $query .= "AND `users`.`name` LIKE '%$userName%' ";

        if(isset($workerName))
            $query .= "AND `welder_att`.`workerName` LIKE '%$workerName%' ";

        if(isset($punktName))
            $query .= "AND `welder_att`.`punktName` LIKE '%$punktName%' ";

        if(isset($workerStump))
            $query .= "AND `welder_att`.`workerStump` LIKE '%$workerStump%' ";

        if(isset($weldMethod))
            $query .= "AND `welder_att`.`weldMethod` LIKE '%$weldMethod%' ";

        if(isset($material))
            $query .= "AND `welder_att`.`material` LIKE '%$material%' ";

        if(isset($techCardNum))
            $query .= "AND `welder_att`.`techCardNum` LIKE '%$techCardNum%' ";

        if(isset($startDate))
            $query .= "AND `welder_att`.`startDate` >= '$startDate  00:00:00' ";

        if(isset($endDate))
            $query .= "AND `welder_att`.`endDate` <= '$endDate  23:59:59' ";

        if(!Yii::app()->user->checkAccess(User::ROLE_MODER))
            $query .= "AND `welder_att`.`userId` = " . Yii::app()->user->getId() . " ";

        $count = Yii::app()->db->createCommand($query)->queryRow();
        return CJSON::encode(array(
            'count' => intval($count['count'])
        ));
    }

    public static function DeleteWeldersData($records)
    {
        try
        {
            if(!Yii::app()->user->checkAccess(User::ROLE_ADMIN))
                throw new Exception("У вас не хватает прав для совершения этого действия");

            foreach($records as $recordId)
            {
                $record = CWelder::model()->findByPk($recordId);
                $record->delete();

                CParameter::model()->deleteAll('certId = :certId', array(':certId' => $recordId));
            }

            return CJSON::encode(array(
                'success' => true
            ));
        }
        catch(Exception $ex)
        {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => $ex->getMessage()
            ));
        }
    }

    public static function renderPdf($certId)
    {
        $cWelder = CWelder::model()->findByPk($certId);
        $cData = $cWelder->getData();

        $html = "<h1>Отчет по аттестации сварщика</h1><table>";

        $html .= "<tr><td>ФИО сварщика:</td><td>" . $cData['workerName'] . "</td><td rowspan='16' class='map'>";
        $html .= "<img src='http://static-maps.yandex.ru/1.x/?size=420,420&amp;z=15&amp;l=map&amp;pt=".$cData['lon'].",".$cData['lat'].",pmwtm1' width='420'></td></tr>";
        $html .= "<tr><td>Член комиссии:</td><td>" . $cData['userName'] . "</td></tr>";
        $html .= "<tr><td>Шифр аттестационого центра:</td><td>" . $cData['centerName'] . "</td></tr>";
        $html .= "<tr><td>Номер аттестационого пункта:</td><td>" . $cData['punktName'] . "</td></tr>";
        $html .= "<tr><td>Клемо сварщика:</td><td>" . $cData['workerStump'] . "</td></tr>";
        $html .= "<tr><td>Номер тех карты:</td><td>" . $cData['techCardNum'] . "</td></tr>";
        $html .= "<tr><td>Вид соединения:</td><td>" . $cData['jointType'] . "</td></tr>";
        $html .= "<tr><td>Шифр способа:</td><td>" . $cData['weldMethod'] . "</td></tr>";
        $html .= "<tr><td>Размеры КСС:</td><td>" . $cData['kccSize'] . "</td></tr>";
        $html .= "<tr><td>Материал КСС:</td><td>" . $cData['material'] . "</td></tr>";
        $html .= "<tr><td>Аттестация начата:</td><td>" . $cData['startDate'] . "</td></tr>";
        $html .= "<tr><td>Аттестация закончена:</td><td>" . $cData['endDate'] . "</td></tr>";
        $html .= "<tr><td>Общее время аттестации:</td><td>" . $cData['timeDiff'] . "</td></tr>";
        $html .= "<tr><td>Время горения дуги:</td><td>" . $cData['weldTime'] . "</td></tr>";
        $html .= "<tr><td>Время вспомогательных операций:</td><td>" . $cData['additionalTime'] . "</td></tr>";
        $html .= "<tr><td>Нормативное кол-во слоев:</td><td>" . $cData['layerNorm'] . "</td></tr>";
        $html .= "<tr><td>Фактичекое кол-во слоев:</td><td>" . $cData['layerCount'] . "</td></tr>";
        $html .= "<tr><td>GPS координаты (долгота, широта):</td><td>" . $cData['lon'] . ", " . $cData['lat'] . "</td></tr>";

        $html .= "</table>";

        $stylesheet = "table {font-size: 15px; font-family: Georgia, sans-serif;}";
        $stylesheet .= "table#params {font-size: 15px; margin-top: 20px;}";
        $stylesheet .= "table td.map {padding-left: 60px;}";
        $stylesheet .= "table#params tr.color {background-color: #dbeaf0;}";
        $stylesheet .= "table#params tr.color td {width: 11%; border: 1px solid #000;}";
        $stylesheet .= "table#params td {width: 11%; text-align: center;}";
        $stylesheet .= "table#params td.layer {text-align: center;  border: 1px solid #000; background-color: #f4f4f4; padding: 10px 0px;}";
        $stylesheet .= "h1 {font-size: 24px; text-align: center; display: block;}";

        //echo CParameter::getParameters($certId, 0);

        include(Yii::getPathOfAlias('webroot') . "/mpdf60/mpdf.php");

        $mpdf = new mPDF('utf-8', 'A4-L', '8', '', 10, 10, 7, 7, 10, 10); /*задаем формат, отступы и.т.д.*/
        $mpdf->charset_in = 'utf-8'; /*не забываем про русский*/

        $mpdf->list_indent_first_level = 0;

        $mpdf->WriteHTML($stylesheet,1);
        $mpdf->WriteHTML($html, 2); /*формируем pdf*/

        $html = "";
        $html .= "<table id='params' width='100%'><tr class='color'>";
        $html .= "<td>Начало</td>";
        $html .= "<td>Конец</td>";
        $html .= "<td>Время</td>";
        $html .= "<td>Позиция</td>";
        $html .= "<td>Uxx</td>";
        $html .= "<td>Uср</td>";
        $html .= "<td>Uоткл</td>";
        $html .= "<td>Iср</td>";
        $html .= "<td>Iоткл</td>";
        $html .= "</tr>";

        $parameters = CJSON::decode(CParameter::getParameters($certId, 0));
        foreach($parameters as $key => $layers)
        {
            if(intval($key) == 0)
                $html .= "<tr><td class='layer' colspan='9'>Прихватка</td></tr><tr>";
            else $html .= "<tr><td class='layer' colspan='9'>Слой номер $key</td></tr><tr>";


            foreach($layers as $layer) {
                $html .= "<tr>";
                $html .= "<td>" . $layer["startDate"] . "</td>";
                $html .= "<td>" . $layer["endDate"] . "</td>";
                $html .= "<td>" . $layer["workTime"] . "</td>";
                $html .= "<td>" . $layer["position"] . "</td>";
                $html .= "<td>" . $layer["maxU"] . "</td>";
                $html .= "<td>" . $layer["avrU"] . "</td>";
                $html .= "<td>" . $layer["stdU"] . "</td>";
                $html .= "<td>" . $layer["avrI"] . "</td>";
                $html .= "<td>" . $layer["stdI"] . "</td>";
                $html .= "</tr>";
            }


        }
        $html .= "</table>";
        $mpdf->WriteHTML($html, 2); /*формируем pdf*/

        return $mpdf->Output('mpdf.pdf', 'I');
    }
}
