<?php

class Device extends ActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'devices';
    }

    public static function findByCode($code)
    {

        return Device::model()->find('code = :code and isActive = 1', array(':code' => $code, ));
    }

    public function isOnline()
    {
        //Берем пользователя, которые использует это устройство и онлайн
        $sql = "select COUNT(*) as count from users " .
            "where deviceInUse = " . $this->id . " and isOnline = 1";
        Yii::log($sql, 'info');

        $data = Yii::app()->db->createCommand($sql)->queryRow();
        //Если нашли, то устройство онлайн
        return intval($data['count']) > 0;
    }

    public static function addDevice($centerId, $code)
    {
        try
        {
            if(!Yii::app()->user->checkAccess(User::ROLE_ADMIN))
                throw new Exception("У вас не хватает прав для совершения этого действия");

            if(empty($code))
                throw new Exception("Вы не указали адрес устройтва");
                

            $code = CText::esc($code);
            $deviceExists = Device::model()->find('code = :code', array(':code' => $code));

            if(isset($deviceExists))
            {
                $deviceExists->centerId = $centerId;
                $deviceExists->isActive = 1;
                $deviceExists->save();
                
                return CJSON::encode(array(
                    'success' => true
                ));
                //$aCenter = CCenter::model()->findByPk($deviceExists->centerId);
                //throw new Exception("Уже с таким адресом уже зарегистрировано за " . $aCenter->name .".");
            }

            $device = new Device;

            $device->centerId = $centerId;
            $device->code = $code;

            $device->save();

            return CJSON::encode(array(
                'success' => true
            ));
        }
        catch(Exception $ex)
        {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => $ex->getMessage()
            ));
        }
    }

    public static function updateDevice($id, $params)
    {
        try
        {
            if(!Yii::app()->user->checkAccess(User::ROLE_ADMIN))
                throw new Exception("У вас не хватает прав для совершения этого действия");

            $device = Device::model()->findByPk($id);

            if(!isset($device))
                throw new Exception("Не существует объекта с указанным ID.");

            $device->centerId = $params['centerId'];
            $device->code = $params['code'];

            $device->save();

            return CJSON::encode(array(
                'success' => true,
                'deviceInfo' => $params
            ));
        }
        catch(Exception $ex)
        {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => $ex->getMessage()
            ));
        }
    }

    public static function getDevices($centerId, $start, $offset, $code)
    {
        $start = empty($start) ? 0 : intval($start);
        $offset = empty($offset) ? self::ROWS_PER_PAGE : intval($offset);

        $bounds = array(
            ':centerId' => $centerId,
            ':start' => $start,
            ':offset' => $offset
        );

        $sql = "select * from devices " .
            "where centerId = :centerId and isActive = 1 ";

        if(!empty($code)){
            $sql .= "and code like :code ";
            $bounds[':code'] = $code;
        }

        $sql .= "limit :start,:offset";

        $devices =  Device::model()->findAllBySql($sql, $bounds);

        $data = array();
        foreach($devices as $device)
            $data[] = array(
                'id' => $device->id,
                'code' =>  $device->code,
                'isOnline' => $device->isOnline()
            );
            

        return CJSON::encode($data);
    }

    public static function getDevicesCount($centerId, $code)
    {
        $query = Yii::app()->db->createCommand()
            ->select("COUNT(*) as count")
            ->from("devices");

        if(isset($code))
        {
            $centerId = Yii::app()->db->quoteValue($centerId);
            $query = $query->where(array('and', 'isActive = 1', array('and', "centerId = $centerId", array('like', 'code', "%$code%"))));
        }
        else  $query->where(array('and', 'centerId = :centerId',  'isActive = 1'), array(':centerId' => $centerId));

        $count = $query->queryRow();

        return CJSON::encode(array(
            'count' => intval($count['count'])
        ));
    }

    public static function DeleteDevices($records)
    {
        try
        {
            if(!Yii::app()->user->checkAccess(User::ROLE_ADMIN))
                throw new Exception("У вас не хватает прав для совершения этого действия");

            foreach($records as $recordId)
            {
                $record = Device::model()->findByPk($recordId);
                $record->isActive = 0;
                $record->save();
            }

            return CJSON::encode(array(
                'success' => true
            ));
        }
        catch(Exception $ex)
        {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => $ex->getMessage()
            ));
        }
    }
}