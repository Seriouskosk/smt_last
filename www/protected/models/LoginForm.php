<?php

class LoginForm extends CFormModel
{
	public $login;
	public $password;

	private $_identity;

	public function rules()
	{
		return array(
			//логин и парот обязательны
			array('login, password', 'required')
		);
	}

	public function login()
	{   
		//Если наш объект авторизации отсутствует, создаем его
		if($this->_identity === null)
		{
			//Обезопасим себя
			$username = CText::esc($this->login);
			$password = CText::esc($this->password);

			//Создаем объект и инициализируем его
			$this->_identity = new UserIdentity($this->login, $this->password);
			$this->_identity->authenticate();
		}

		//Если авторизация проходит успешно...
		if($this->_identity->errorCode === UserIdentity::ERROR_NONE)
		{
			//Ставим куки на это время
			$duration = 3600*24*30;

			//Говорим Yii создать объект WebUser
			Yii::app()->user->login($this->_identity, $duration);

            //Добавляем в историю посещений
            CSession::createDetskopSession(Yii::app()->user->getId());

			return true;
		}
		else return false;
	}
}
