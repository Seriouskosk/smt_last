<?

class Message extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'messages';
    }

    public static function sendMessage($userId, $message, $isAdmin)
    {
        try
        {
            if(Yii::app()->user->getIsGuest())
                throw new Exception("У вас не хватает прав для совершения этого действия");

            if($isAdmin === 'true' && !Yii::app()->user->checkAccess(User::ROLE_ADMIN))
                throw new Exception("У вас не хватает прав для совершения этого действия");

            $messageData = new Message();

            $messageData->userId = $userId;
            $messageData->message = $message;
            $messageData->isAdmin = $isAdmin;
            $messageData->date = date("Y-m-d H:i:s");

            $messageData->save();

            return CJSON::encode(array(
                'name' => User::model()->findByPk($userId)->name,
                'message' => $messageData->message,
                'isAdmin' => $messageData->isAdmin,
                'date' => $messageData->date
            ));
        }
        catch(Exception $ex)
        {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => $ex->getMessage()
            ));
        }
    }

    public static function getMessages($userId, $startPosition = 0, $offset = 10)
    {
        if(Yii::app()->user->getIsGuest()) {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => "Получать сообщения могут только авторизованные пользователи."
            ));
        }

        $user = User::model()->findByPk(Yii::app()->user->getId());
        if($user == null)
        {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => "Вы пытаетесь отправить сообщение несуществующему пользователю."
            ));
        }


        if($startPosition == null)
            $startPosition = 0;

        if($offset == null)
            $offset = 10;

        $query = "SELECT `messages`.*, `users`.`name` FROM `messages`
            LEFT JOIN `users` ON `users`.`id` = `messages`.`userId`
            WHERE `messages`.`userId` = $userId ORDER BY `messages`.`date` DESC LIMIT $startPosition, $offset";

        $entries = Yii::app()->db->createCommand($query)->queryAll();

        return CJSON::encode($entries);
    }

    public static function getDialogs()
    {
        if(!Yii::app()->user->checkAccess(User::ROLE_ADMIN)) {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => "Чтобы получить список диалогов вы должны обладать правами администратора."
            ));
        }

        $query = "SELECT `users`.`name`, `a`.`userId`, `a`.`isAdmin` FROM `messages` `a`
            LEFT JOIN `users` ON `users`.`id` = `a`.`userId`
            INNER JOIN  (SELECT `messages`.`userId`, MAX(`messages`.`id`) AS `maxId` from `messages` GROUP BY `messages`.`userId`) `b`
            ON `a`.`id` = `b`.`maxId` AND `a`.`userId` = `b`.`userId`
            ORDER BY `a`.`date` DESC";

        $dialogs = Yii::app()->db->createCommand($query)->queryAll();

        return CJSON::encode($dialogs);
    }

    public static function checkMessages($userId)
    {
        $user = User::model()->findByPk($userId);

        if(!isset($user))
            return CJSON::encode(array(
               'success' => false,
               'errorMessage' => "Неверный ID пользователя"
            ));

        $lastMessage = Message::findBySql("SELECT * FROM `messages` WHERE userId = :userId ORDER BY `date` DESC LIMIT 1",
            array(':userId' => $userId));

        if($lastMessage->isAdmin)
            return CJSON::encode(array(
                'success' => true,
                'newMessages' => true
            ));
        else return CJSON::encode(array(
            'success' => true,
            'newMessages' => false
         ));
    }
}