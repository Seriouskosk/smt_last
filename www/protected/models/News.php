<?

class News extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'news';
    }

    public static function addNews($title, $text)
    {
        try
        {
            if(!Yii::app()->user->checkAccess(User::ROLE_ADMIN))
                throw new Exception("У вас не хватает прав для совершения этого действия");

            $news = new News;

            $news->title = $title;
            $news->text = $text;
            $news->date = date('Y-m-d H:i:s');

            $news->save();

            return CJSON::encode(array(
                'success' => true
            ));
        }
        catch(Exception $ex)
        {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => $ex->getMessage()
            ));
        }
    }

    public static function updateNews($id, $params)
    {
        try
        {
            if(!Yii::app()->user->checkAccess(User::ROLE_ADMIN))
                throw new Exception("У вас не хватает прав для совершения этого действия");

            $news = News::model()->findByPk($id);

            if(!isset($news))
                throw new Exception("Не существует объекта с указанным ID.");

            $news->title = $params['title'];
            $news->text = $params['text'];

            $news->save();

            return CJSON::encode(array(
                'success' => true,
                'newsInfo' => $params
            ));
        }
        catch(Exception $ex)
        {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => $ex->getMessage()
            ));
        }
    }

    public static function deleteNews($newsToDelete)
    {
        try
        {
            if(!Yii::app()->user->checkAccess(User::ROLE_ADMIN))
                throw new Exception("У вас не хватает прав для совершения этого действия");

            foreach($newsToDelete as $id)
            {
                $news = News::model()->findByPk($id);

                if(!isset($news))
                    throw new Exception("Не существует объекта с указанным ID.");

                $news->delete();
            }

            return CJSON::encode(array(
                'success' => true
            ));
        }
        catch(Exception $ex)
        {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => $ex->getMessage()
            ));
        }
    }

    public static function getNews($offset = 0, $count = 1)
    {
        if(empty($offset))
            $offset = 0;

        if(empty($count))
            $count = 50;

        $news = News::model()->findAllBySql("SELECT * FROM `news` ORDER BY `date` DESC LIMIT $offset, $count");
        return CJSON::encode($news);
    }

    public static function getNewsCount()
    {
        $count = Yii::app()->db->createCommand()
            ->select('COUNT(*) as count')
            ->from('news')->queryRow();

        return CJSON::encode(array(
            'count' => intval($count['count'])
        ));
    }
}