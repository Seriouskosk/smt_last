<?php

class Organization extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'organizations';
    }

    public static function isOnline($id)
    {
        //$org = Organization::model()->findByPk($id);

        $cTechOrg = CTech::model()->findBySql("select * from `tech_att`
          where `orgId` = :orgId and isClosed = 0 limit 1", array(':orgId' => $id));

        if($cTechOrg != null)
            return true;

        $cEquipOrg = CEquipPrac::model()->findBySql("select * from `equip_kcc`
          where `pracId` in (select `id` from `equip_att` where `orgId` = :orgId) and `isClosed` = 0 limit 1", array(':orgId' => $id));

        if($cEquipOrg != null)
            return true;
        else return false;
        //return true;
    }

    public static function findByInn($inn)
    {
        return Organization::model()->find('inn = :inn', array(':inn' => $inn));
    }

    public static function addOrganization($centerId, $name, $inn, $adress, $phone)
    {
        try
        {
            if(Yii::app()->user->getIsGuest())
                throw new Exception("У вас не хватает прав для совершения этого действия");

            $orgExists = Organization::model()->findBySql("select * from `organizations` where `name` = '$name' or
              `inn` = '$inn' or `adress` = '$adress' limit 1");

            if(isset($orgExists))
                throw new Exception("Уже существует организация с таким ИНН, названием или адресом.");

            $organization = new Organization;


            $organization->centerId = $centerId;
            $organization->name = $name;
            $organization->inn = $inn;
            $organization->adress = $adress;
            $organization->phone = $phone;

            $organization->save();

            return CJSON::encode(array(
                'success' => true
            ));
        }
        catch(Exception $ex)
        {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => $ex->getMessage()
            ));
        }
    }

    public static function updateOrganization($id, $params)
    {
        try
        {
            if(!Yii::app()->user->checkAccess(User::ROLE_ADMIN))
                throw new Exception("У вас не хватает прав для совершения этого действия");

            $organization = Organization::model()->findByPk($id);

            if(!isset($organization))
                throw new Exception("Не существует объекта с указанным ID.");

            $organization->name = $params['name'];
            $organization->inn = $params['inn'];
            $organization->adress = $params['adress'];
            $organization->phone = $params['phone'];

            $organization->save();

            return CJSON::encode(array(
                'success' => true,
                'orgInfo' => $params
            ));
        }
        catch(Exception $ex)
        {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => $ex->getMessage()
            ));
        }
    }

    public static function getOrganizations($centerId, $startPosition, $offset, $name, $inn)
    {
        if(!isset($startPosition) || empty($startPosition))
            $startPosition = 0;

        if(!isset($offset) || empty($offset))
            $startPosition = 10;

        $query = "select * from `organizations` where `isActive` = 1 and `centerId` = $centerId ";

        if(isset($name) && !empty($name) && strlen(utf8_decode($name)))
            $query .= "and `name` like '%$name%' ";

        if(isset($inn) && !empty($inn) && strlen(utf8_decode($inn)))
            $query .= "and `inn` like '%$inn%' ";


        $query .= "limit $startPosition, $offset ";

        //echo $query;

        $entries = Yii::app()->db->createCommand($query)->queryAll();
        $organizations = array();

        foreach($entries as $entry)
            $organizations[] = array(
                'id' => $entry['id'],
                'name' => $entry['name'],
                'inn' => $entry['inn'],
                'adress' => $entry['adress'],
                'phone' => $entry['phone'],

                'isOnline' => Organization::isOnline($entry['id'])
            );

        return CJSON::encode($organizations);
    }

    public static function getOrganizationsCount($centerId, $name)
    {
        if(!isset($centerId) || empty($centerId))
            return CJSON::encode(array(
                'count' => 150
            ));

        $query = "select COUNT(*) as count from `organizations` where `isActive` = 1 and `centerId` = $centerId ";

        if(isset($name) && !empty($name) && strlen(utf8_decode($name)))
            $query .= "and `name` like '%$name%' ";

        if(isset($inn) && !empty($inn) && strlen(utf8_decode($inn)))
            $query .= "and `inn` like '%$inn%' ";
        
        $count = Yii::app()->db->createCommand($query)->queryRow();

        return CJSON::encode(array(
            'count' => intval($count['count'])
        ));
    }

    public static function DeleteOrganizations($records)
    {
        try
        {
            if(!Yii::app()->user->checkAccess(User::ROLE_ADMIN))
                throw new Exception("У вас не хватает прав для совершения этого действия");

            foreach($records as $recordId)
            {
                $record = Organization::model()->findByPk($recordId);
                $record->isActive = 0;
                $record->save();
            }

            return CJSON::encode(array(
                'success' => true
            ));
        }
        catch(Exception $ex)
        {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => $ex->getMessage()
            ));
        }
    }
}