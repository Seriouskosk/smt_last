<?

//Класс пользователя
class User extends CActiveRecord
{
    const ROLE_ADMIN = 'administrator';
    const ROLE_MODER = 'moderator';
    const ROLE_COMMISSION_MEMBER = 'commissionMember';
    const ROLE_USER = 'user';

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'users';
    }

    public function createApiSession(&$device)
    {
        //Создаем сессию
        CSession::createApiSession($this->id, $device->code);

        //Переводим пользователя в режим онлайн
        $this->isOnline = true;
        $this->deviceInUse = $device->id;
        $this->save();
    }

    public function closeApiSession()
    {
        CSession::closeApiSession($this->id);

        //Переводим пользователя в режим оффлайн
        $this->isOnline = false;
        $this->save();
    }

    public function afterFind()
    {
        parent::afterFind();

        $this->isOnline = filter_var($this->isOnline, FILTER_VALIDATE_BOOLEAN);
        $this->isActive = filter_var($this->isActive, FILTER_VALIDATE_BOOLEAN);

        return true;
    }
    
    public static function getById($id)
    {
        return User::model()->findByPk($id);
    }

    public static function addUser($centerId, $name, $login, $password, $level, $welders, $tech, $equip)
    {
        try
        {
            if(!Yii::app()->user->checkAccess(User::ROLE_ADMIN))
                throw new Exception("У вас не хватает прав для совершения этого действия");

            $userExists = User::model()->find('login = :login', array(':login' => $login));

            if(isset($userExists) && intval($userExists->isActive) == 1)
                throw new Exception("Уже существует пользователь с таким логином");

            //Регистрация на форуме
            //Yii::app()->phpBB->userAdd($login, $password, 'test@test.ru', 2);

            $user = new User;

            $user->centerId = $centerId;
            $user->name = $name;
            $user->login = $login;
            $user->password = $password;
            $user->level = $level;
            $user->isActive = 1;

            $user->date = date('Y-m-d H:i:s');
            $user->isOnline = 0;

            if(isset($welders) &&  $welders)
                $user->welders = 1;
            else $user->welders = 0;

            if(isset($tech) &&  $tech)
                $user->tech = 1;
            else $user->tech = 0;

            if(isset($equip) &&  $equip)
                $user->equip = 1;
            else $user->equip = 0;

            $user->save();

            //default user info
            $user_raw = array(
                'username' => $login,
                "password" => $password,
                "user_email" => '',
                "group_id" => "2",
                "user_timezone" => "2.00",
                "user_dst" => 0,
                "user_lang" => '',
                "user_type" => "0",
                "user_actkey" => "",
                "user_dateformat" => "|d M Y|, H:i",
                "user_style" => "1",
                "user_regdate" => time(),
                "user_colour" => "9E8DA7",
            );

            require_once('/home/smt/www/protected/extensions/phpBB/phpbbClass.php');

            $phpbb = new phpbbClass('/home/smt/www/forum/');

            $phpbb->user_add($user_raw);

            return CJSON::encode(array(
                'success' => true
            ));
        }
        catch(Exception $ex)
        {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => $ex->getMessage()
            ));
        }
    }

    public static function updateUser($id, $params)
    {
        try
        {
            if(!Yii::app()->user->checkAccess(User::ROLE_ADMIN))
                throw new Exception("У вас не хватает прав для совершения этого действия");

            $user = User::model()->findByPk($id);

            if(!isset($user))
                throw new Exception("Не существует объекта с указанным ID.");

            $user->centerId = $params['centerId'];
            $user->name = $params['name'];
            $user->login = $params['login'];
            $user->password = $params['password'];
            $user->level = $params['level'];

            if(isset($params['welders']) &&  $params['welders'])
                $user->welders = 1;
            else $user->welders = 0;

            if(isset($params['tech']) &&  $params['tech'])
                $user->tech = 1;
            else $user->tech = 0;

            if(isset($params['equip']) &&  $params['equip'])
                $user->equip = 1;
            else $user->equip = 0;

            $user->save();

            return CJSON::encode(array(
                'success' => true,
                'userInfo' => $params
            ));
        }
        catch(Exception $ex)
        {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => $ex->getMessage()
            ));
        }
    }

    public static function ActivateUsers($records)
    {
        try
        {
            if(!Yii::app()->user->checkAccess(User::ROLE_ADMIN))
                throw new Exception("У вас не хватает прав для совершения этого действия");

            foreach($records as $recordId)
            {
                $record = User::model()->findByPk($recordId);
                if($record == null)
                    throw new Exception("Вы пытаетесь активировать несуществующего пользователя id = " . $recordId);

                $record->isOnline = false;
                $record->isActive = 1;
                $record->save();
            }

            return CJSON::encode(array(
                'success' => true
            ));
        }
        catch(Exception $ex)
        {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => $ex->getMessage()
            ));
        }
    }

    public static function DeleteUsers($records)
    {
        try
        {
            if(!Yii::app()->user->checkAccess(User::ROLE_ADMIN))
                throw new Exception("У вас не хватает прав для совершения этого действия");

            foreach($records as $recordId)
            {
                $record = User::model()->findByPk($recordId);
                if($record == null)
                    throw new Exception("Вы пытаетесь удалить несуществующего пользователя id = " . $recordId);

                if(intval($record->isActive) == 0)
                    $record->isActive = 1;
                else {
                    $record->isOnline = 0;
                    $record->isActive = 0;
                }

                $record->save();
            }

            return CJSON::encode(array(
                'success' => true
            ));
        }
        catch(Exception $ex)
        {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => $ex->getMessage()
            ));
        }
    }

    public static function getUsersByRights($centerId, $startPosition, $offset, $adminRights, $username = "")
    {
        if($adminRights && !Yii::app()->user->checkAccess(User::ROLE_ADMIN)) 
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => "У вас нет доступа к данной инфомации"
            ));

        if (CCenter::model()->findByPk($centerId) === null) {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => "Аттестационного центра не существует"
            ));
        }

        if(empty($startPosition))
            $startPosition = 0;

        $sql = "select " . ($adminRights ? "*" : "id, name, isOnline, date, level, centerId, isActive") . " from users " .
            "where centerId = ". $centerId . " ";

            if(!empty($username))
                $sql .= "and name like '%" . $username . "%' ";

            $sql .= "limit " . $startPosition . ", " . $offset;

        $data = Yii::app()->db->createCommand($sql)->queryAll();

        return CJSON::encode($data);;
    }

    public static function getUsersByRightsCount($centerId, $username)
    {
        if (CCenter::model()->findByPk($centerId) === null) {
            return CJSON::encode(array(
                'success' => false,
                'errorMessage' => "Аттестационного центра не существует"
            ));
        }

        $sql = "SELECT COUNT(*) as `count` from `users`
                WHERE centerId = ". $centerId . " ";

            if(!empty($username))
                $sql .= "AND `name` LIKE '%" . $username . "%' ";

            //$sql .= "limit " . $startPosition . ", " . $offset;

        $data = Yii::app()->db->createCommand($sql)->queryRow();

        return CJSON::encode(array(
            'count' => intval($data['count'])
        ));
    }


}