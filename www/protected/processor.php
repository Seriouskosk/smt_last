<?php
date_default_timezone_set('UTC');
require_once('helpers/CText.php');

//fwrite(STDOUT, "Start processing...");

$dir = dirname(__FILE__).'/../upload/';
$dirCsv = $dir.'csv/';

$conn = new mysqli('localhost', 'smt', 'rw3uMDYTC-s.F.-@', 'smt_db');
if ($conn->connect_errno)
    die('Connection error: ' . $conn->connect_error());

$conn->set_charset('utf-8');

//Get the 3 last entries
$sql = "select id, regFileUrl from params where processed = 0 order by id asc limit 3";

$res = $conn->query($sql);
while($row = $res->fetch_assoc()) {
    file_put_contents(dirname(__FILE__) . '/processorlog', date('d.m.Y H:i:s') . " " . print_r($row, true) . "\n", FILE_APPEND);
	//fwrite(STDOUT, "Processing parameter with id = " . $row['id'] . "(" . $row['regFileUrl'] . ")");
	$zip = new ZipArchive();

	if($zipOpen = $zip->open($dir . $row['regFileUrl'])) {
		$fileName = $zip->getNameIndex(0);
		
		//Если файла нет в архиве, то не будем ничего обрабатывать
		if(empty($fileName))
		{
            file_put_contents(dirname(__FILE__) . '/processorlog', "В архиве не оказалось файлов \n", FILE_APPEND);

			$updateSql = "update params set processed = 1 where id = " . $row['id'];
			$conn->query($updateSql);
			die();
		}

		$extracted = $zip->extractTo($dirCsv, $fileName);
		$zip->close();	


		//Если извлечение не получилось, тоже не заморачиваемся
		if(!$extracted)	{
            file_put_contents(dirname(__FILE__) . '/processorlog', "Не удалось извлечь файлы \n", FILE_APPEND);

			$updateSql = "update params set processed = 1 where id = " . $row['id'];
			$conn->query($updateSql);
			die();
		}

		$newName = date('dmYHis') . CText::randString(5) . urldecode(CText::translate($fileName));
		rename($dirCsv.$fileName, $dirCsv.$newName);


		//VARIABLES
		$WorkSamplesCount = $OCSamplesCount = $Uxx = $Uavr = $Iavr = $Istd = $Ustd = $dt = 0;
		$WorkPointsBuffer = array();

		//CONSTANTS
		$minCurrent = 5; $minVoltage = 2.5; $maxVoltage = 50;
		$first = true; $second = false;
		$idx = 1;
		//Чтение файла
		if (($handle = fopen($dirCsv.$newName, "r")) !== false)
		    while (($data = fgetcsv($handle, 60, ";")) !== false) {

		        if($second) {
		            $second = false;
		            $dt = floatval(str_replace(",", ".", $data[2]));
		        }

		        if($first) {
		            $first = false;
		            $second = true;
		            continue;
		        }

		        $i = floatval(str_replace(",", ".", $data[0]));//сила тока
		        $u = floatval(str_replace(",", ".", $data[1]));//напряжение

		        if ($i > $minCurrent && $u > $minVoltage)
		        {
		            $WorkPointsBuffer[]= $i;
		            $WorkPointsBuffer[]= $u;
		            $WorkSamplesCount += 2;
		            
		            if($u > $maxVoltage) {
		            	$Uxx += $u;
		            	$OCSamplesCount++;
		            }
		        }
		    }
		//iceekey
		else {
            file_put_contents(dirname(__FILE__) . '/processorlog', "Файл " . $dirCsv.$newName . " не открывается на чтение \n", FILE_APPEND);
			//fwrite(STDOUT, "Can't open file to read -> " . $row['id']);
			return;
		}

		//Закроем файл
	    fclose($handle);

	    //Обработка файла
	    if($OCSamplesCount > 2)
	        $Uxx /= $OCSamplesCount;

	    //Если есть что обрабатывать
	    if($WorkSamplesCount > 2) {
	    	$pointsCount = $WorkSamplesCount / 2;
			//fwrite(STDOUT, "Count:  " . $pointsCount . ";;");

    		//Устанавливаем начальные значения
    		$tmpIavr = 0;
    		$tmpUavr = 0;

    		//Формируем сумму по точками
    		for ($i = 0; $i < $WorkSamplesCount; $i += 2) {
    			
    		    $tmpIavr += $WorkPointsBuffer[$i];
    		    $tmpUavr += $WorkPointsBuffer[$i + 1];

    		    // if($i >= 1000 && $i < 1010)
    		    // fwrite(STDOUT, " [" . $WorkPointsBuffer[$i] . "-" . $WorkPointsBuffer[$i + 1] . " =>" . $i. "] ");
    		}

    		//Добавляем к общему среднему среднее из чанка
    		$Iavr = ($tmpIavr / $pointsCount);// + ($tmpIavr / 1000);
    		$Uavr = ($tmpUavr / $pointsCount);// + ($tmpUavr / 1000);

	    	//Теперь, когда мы просчитали средние значения, вычислим отклонение

	    	//fwrite(STDOUT, "SUMI:  " . $tmpIavr . ";;");


    		//Устанавливаем начальные значения
    		$tmpIstd = 0;
    		$tmpUstd = 0;

    		//Формируем сумму по точкам
    		for ($i = 0; $i < $WorkSamplesCount; $i += 2) {
    			$tmpIstd += ($WorkPointsBuffer[$i] - $Iavr) * ($WorkPointsBuffer[$i] - $Iavr);
    			$tmpUstd += ($WorkPointsBuffer[$i + 1] - $Uavr) * ($WorkPointsBuffer[$i + 1] - $Uavr);
    		}

    		//Добавляем к общему стандартному отклонению
    		$Istd = $tmpIstd / $pointsCount;// + $Istd * $pointsCount / 1000;
    		$Ustd = $tmpUstd / $pointsCount;// + $Ustd * $pointsCount / 1000;
	    	

	    	$Istd = sqrt($Istd);
	    	$Ustd = sqrt($Ustd);

		}
		
    	//Вычисляем время сварки
        $input = intval($WorkSamplesCount / 2) * intval($dt);
        $input = floor($input / 1000);

        $hours = floor($input / 3600);
        $input -= 3600 * $hours;

        $min = floor($input / 60);
        $input -= 60 * $min;

        $seconds = $input;
        //$cParameter->workTime = (intval($hours / 10) == 0 ? "0" : "") . $hours . ":" . (intval($min / 10) == 0 ? "0" : "") . $min . ":" . (intval($seconds / 10) == 0 ? "0" : "") . $seconds;
        $workTime = (intval($hours / 10) == 0 ? "0" : "") . $hours . ":" . (intval($min / 10) == 0 ? "0" : "") . $min . ":" . (intval($seconds / 10) == 0 ? "0" : "") . $seconds;
        
        $updateSql = "update params set " .
        	"stdI = " . $Istd . ", " .
        	"stdU = " . $Ustd . ", " .
        	"avrI = " . $Iavr . ", " .
        	"avrU = " . $Uavr . ", " .
        	"workTime = '" . $workTime . "', " .
        	"processed = 1, " .
        	"regFile = '" . $newName . "', " .
        	"maxU = " . $Uxx . " " .
        	"where id = " . $row['id'];

        $conn->query($updateSql);
	}
}

$conn->close();