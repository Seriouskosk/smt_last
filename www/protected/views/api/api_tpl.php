<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <link rel="stylesheet" href="<?=CUrl::get('css/api.css')?>">
    <link rel="stylesheet" href="<?=CUrl::get('css/font-awesome.min.css')?>">
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
</head>
<body>
<?if(!Yii::app()->user->checkAccess(User::ROLE_ADMIN)):?>
    <div class="container">
        <h1 class="page-header">Внимание! Ошибка доступа.</h1>

        <p>Чтобы получить доступ к API войдите на сайт под учетной записью администратора и снова посетите страницу.</p>
    </div>
<?else:?>

<script>
    var lastResponse = null;

    $(document).ready(function() {
        $(".live").submit(function() {
            $('.response').each(function() {
                $(this).remove();
            });

            var form = $(this);
            var postData = new FormData($(this)[0]);
            var response = null;

            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: postData, // serializes the form's elements.
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function(){
                    if(lastResponse != null)
                        lastResponse.remove();

                    form.find('button').first().after('<span class="response"><strong>Ожидание ответа сервера...</strong></span>');
                    response = form.find('.response').first();
                },
                success: function(data)
                {
                    if(response != null)
                        response.remove();

                    var obj = $.parseJSON(data);

                    form.find('button').first().after('<span class="response"><samp>' + JSON.stringify(obj) + '</samp></span>');
                    lastResponse = form.find('.response').first();
                }
            });

            return false; // avoid to execute the actual submit of the form.
        });
    });
</script>

<div id="api-wrapper">
    <div id="sidebar">
        <div class="list-group">
          <a class="list-group-item" href="#api.welcome">Описание &nbsp;<i class="fa fa-chevron-right pull-right"></i></a>
          <a class="list-group-item" href="#api.auth">Метод <code>/api/auth</code> &nbsp;<i class="fa fa-chevron-right pull-right"></i></a>
          <a class="list-group-item" href="#api.addCWelder">Метод <code>/api/addCWelder</code> &nbsp;<i class="fa fa-chevron-right pull-right"></i></a>
          <a class="list-group-item" href="#api.addCTech">Метод <code>/api/addCTech</code> &nbsp;<i class="fa fa-chevron-right pull-right"></i></a>
          <a class="list-group-item" href="#api.addCEquip">Метод <code>/api/addCEquip</code> &nbsp;<i class="fa fa-chevron-right pull-right"></i></a>
          <a class="list-group-item" href="#api.addCEquipSpec">Метод <code>/api/addCEquipSpec</code> &nbsp;<i class="fa fa-chevron-right pull-right"></i></a>
          <a class="list-group-item" href="#api.addCEquipPrac">Метод <code>/api/addCEquipPrac</code> &nbsp;<i class="fa fa-chevron-right pull-right"></i></a>
          <a class="list-group-item" href="#api.addParameter">Метод <code>/api/addParameter</code> &nbsp;<i class="fa fa-chevron-right pull-right"></i></a>
          <a class="list-group-item" href="#api.finishCWelder">Метод <code>/api/finishCWelder</code> &nbsp;<i class="fa fa-chevron-right pull-right"></i></a>
          <a class="list-group-item" href="#api.finishCTech">Метод <code>/api/finishCTech</code> &nbsp;<i class="fa fa-chevron-right pull-right"></i></a>
          <a class="list-group-item" href="#api.finishCEquip">Метод <code>/api/finishCEquip</code> &nbsp;<i class="fa fa-chevron-right pull-right"></i></a>
          <a class="list-group-item" href="#api.finishCEquipPrac">Метод <code>/api/finishCEquipPrac</code> &nbsp;<i class="fa fa-chevron-right pull-right"></i></a>
          <a class="list-group-item" href="#api.checkOrganization">Метод <code>/api/checkOrganization</code> &nbsp;<i class="fa fa-chevron-right pull-right"></i></a>
          <a class="list-group-item" href="#api.addCEquipVac">Метод <code>/api/addCEquipVac</code> &nbsp;<i class="fa fa-chevron-right pull-right"></i></a>
          <a class="list-group-item" href="#api.logout">Метод <code>/api/logout</code> &nbsp;<i class="fa fa-chevron-right pull-right"></i></a>
        </div>
    </div>
    <div id="content" class="container-fluid">
        <div class="container-fluid">
            <a name="api.welcome" class="anchor"></a>
            <h1 class="page-header" style="font-size:32px;">СМТ API <small>ver. 0.2.1</small></h1>

            <p>API для проекта SMT созданы для удаленной работой с сервером. Все параметры посылаются методом <code>POST</code>. В случае, если
            метод принимает файлы в качестве агрументов, следует использовать заголовок <code>multipart/form-data</code>.</p>

            <p>Если параметр не отослан, он принимает значение пустой строки. Рекомендуется отправлять все параметры, исключение - 
                фотографии, но и в этом случае желательно явно отправлять пустую строку.</p>

            <p>Версия API 0.2.1 полноснтью совместима с предыдущей версией, в ней убран параметр <code>equipAdress</code> из метода
              <code>/api/logout</code> и переписан код, для производительности и стабильности. Так же в новой версии API
              файлы регистограмм не обрабатываются сразу, это делает специальный скрипт, который периодически запускается на сервере
              и обрабатывает добавленные файлы. Снижается нагрузка на сервер и время работы API. Скрипт считает параметры по 
              специальным алгоритмам, которые исключают возможность ошибок переполнения и нехватки памяти. <b>Стоит учитывать, что
              файлы регистограмм будут обработаны не сразу, поэтому придется подождать несколько минут, пока 
              скрипт успеет их обработать, прежде чем результаты появятся на сервере.</b><p>

            <p>В этой версии API так же пересмотрена политика хранения файлов. Теперь имена шифруются и дополняются хешами
              в пользу уникальности и предотвращение создания недопустимых URL путей.</p>

            <p><b>Изменена политика отправки файлов регистограмм.</b> Архив с файлом и файл внутри него могут иметь произвольные имена.
              Например, архив может всегда иметь название <code>archive.zip</code>, а файл внутри <code>data.csv</code>, сервер
              самостоятельно присвоит им уникальные имена.</p>
        </div>

        <div class="container-fluid method">
            <a name="api.auth" class="anchor"></a>
            <h2>Метод <code>/api/auth</code></h2>
            <h3>Описание</h3>
            <p>Авторизует пользователя. Не оставляет COOKIE.</p>

            <h3>Возвращает:</h3>
            <dl class="dl-horizontal">
              <dt><code>success:boolean</code></dt>
              <dd>Успеность выполнения запроса</dd>
            </dl>

            <dl class="dl-horizontal">
              <dt><code>errorMessage:string</code></dt>
              <dd>Описывает ошибку, если <code>success == false</code></dd>
            </dl>

            <dl class="dl-horizontal">
              <dt><code>userId:int</code></dt>
              <dd>ID пользователя</dd>
            </dl>

            <h3>Возможные ошибки в <code>errorMessage</code>:</h3>
                <ul>
                    <li>Отсутствует имя пользователя или пароль</li>
                    <li>Отсутствует адрес устройства</li>
                    <li>Пользователя с таким логином и паролем не существует</li>
                    <li>Устройства с таким адресом не существует</li>
                    <li>Устройство не принадлежит вашему юридическому лицу</li>
                </ul>
            <h3>Протестировать метод:</h3>
            <div class="panel">
                <div class="panel-body method">
                    <form class="pure-form live" action="/api/auth" method="post">
                        <div class="form-group">
                           <label><code>login:string</code> - логин пользователя в системе</label>
                           <input name="login" type="text" class="form-control" placeholder="login"> 
                        </div>

                        <div class="form-group">
                           <label><code>password:string</code> - пароль пользователя в системе</label>
                           <input name="password" type="text" class="form-control" placeholder="password">
                        </div>

                        <div class="form-group">
                           <label><code>equipAddr:string</code> - MAC адрес устройства</label>
                           <input name="equipAddr" type="text" class="form-control" placeholder="equipAddr">
                        </div>

                        <button type="submit" class="btn btn-default">Запрос на сервер</button>
                    </form>
                </div>  
            </div>
        </div>

        <div class="container-fluid method">
            <a name="api.addCWelder" class="anchor"></a>
            <h2>Метод <code>/api/addCWelder</code></h2>
            <h3>Описание</h3>
            <p>Создает новую аттестацию сварщика. </p>
            <p>Метод просматривает все аттестации пользователя, если есть незакрытая аттестация,
              то возвращается ID незакрытой аттестации. Таким образом пользователь может продолжить работу в случае утери
              соединения, ошибки программы или сервера.</p>

            <h3>Возвращает:</h3>
            <dl class="dl-horizontal">
              <dt><code>success:boolean</code></dt>
              <dd>Успеность выполнения запроса</dd>
            </dl>

            <dl class="dl-horizontal">
              <dt><code>errorMessage:string</code></dt>
              <dd>Описывает ошибку, если <code>success == false</code></dd>
            </dl>

            <dl class="dl-horizontal">
              <dt><code>certId:int</code></dt>
              <dd>ID аттестации</dd>
            </dl>

            <h3>Возможные ошибки в <code>errorMessage</code>:</h3>
                <ul>
                    <li>Не указан ID пользователя</li>
                    <li>Не указано имя работника</li>
                    <li>Не указано клеймо сварщика</li>
                    <li>Не указан тип сварки</li>
                    <li>Не указан способ соединения</li>
                    <li>Не указан материал</li>
                    <li>Не указан первый диаметр</li>
                    <li>Не указан второй диаметр</li>
                    <li>Не указана первая толщина</li>
                    <li>Не указана вторая толщина</li>
                    <li>Не указана широта</li>
                    <li>Не указана долгота</li>
                    <li>Не указан номер тех. карты</li>
                    <li>Не указано название пункта проведения аттестации</li>
                    <li>Не указано нормативное количество слоев</li>
                    <li>Не указана дата начала аттестации</li>
                    <li>Тип соединения указан неверно</li>
                    <li>Пользователя с указанным ID не существует</li>
                    <li>Создавать аттестации могут только авторизованные пользователи</li>
                </ul>
            <h3>Протестировать метод:</h3>
            <div class="panel">
                <div class="panel-body method">
                    <form class="pure-form live" action="/api/addCWelder" method="post">
                        <div class="form-group">
                           <label><code>userId:int</code> - ID пользователя</label>
                           <input name="userId" type="text" class="form-control" placeholder="userId"> 
                        </div>

                        <div class="form-group">
                           <label><code>workerName:string</code> - ФИО сварщика</label>
                           <input name="workerName" type="text" class="form-control" placeholder="workerName">
                        </div>

                        <div class="form-group">
                           <label><code>workerStump:string</code> - клеймо сварщика</label>
                           <input name="workerStump" type="text" class="form-control" placeholder="workerStump">
                        </div>

                        <div class="form-group">
                           <label><code>jointType:string</code> - тип соединения (например: "Лист/Труба")</label>
                           <input name="jointType" type="text" class="form-control" placeholder="jointType">
                        </div>

                        <div class="form-group">
                           <label><code>weldMethod:string</code> - тип сварки</label>
                           <input name="weldMethod" type="text" class="form-control" placeholder="weldMethod">
                        </div>

                        <div class="form-group">
                           <label><code>material:string</code> - материал</label>
                           <input name="material" type="text" class="form-control" placeholder="material">
                        </div>

                        <div class="form-group">
                           <label><code>dFirst:float</code> - первый диаметр</label>
                           <input name="dFirst" type="text" class="form-control" placeholder="dFirst">
                        </div>

                        <div class="form-group">
                           <label><code>dSecond:float</code> - второй диаметр</label>
                           <input name="dSecond" type="text" class="form-control" placeholder="dSecond">
                        </div>

                        <div class="form-group">
                           <label><code>hFirst:float</code> - первая толщина</label>
                           <input name="hFirst" type="text" class="form-control" placeholder="hFirst">
                        </div>

                        <div class="form-group">
                           <label><code>hSecond:float</code> - вторая толщина</label>
                           <input name="hSecond" type="text" class="form-control" placeholder="hSecond">
                        </div>


                        <div class="form-group">
                           <label><code>lon:double</code> - широта (координаты)</label>
                           <input name="lon" type="text" class="form-control" placeholder="lon">
                        </div>


                        <div class="form-group">
                           <label><code>lat:double</code> - долгота (координаты)</label>
                           <input name="lat" type="text" class="form-control" placeholder="lat">
                        </div>


                        <div class="form-group">
                           <label><code>techCardNum:string</code> - номер тех. карты</label>
                           <input name="techCardNum" type="text" class="form-control" placeholder="techCardNum">
                        </div>


                        <div class="form-group">
                           <label><code>punktName:string</code> - нормативное количество слоев</label>
                           <input name="punktName" type="text" class="form-control" placeholder="punktName">
                        </div>


                        <div class="form-group">
                           <label><code>startDate:datetime</code> - дата и время начала аттестации (в формате MySQL)</label>
                           <input name="startDate" type="text" class="form-control" placeholder="startDate">
                        </div>

                        <div class="form-group">
                           <label><code>layerNorm:int</code> - нормативное количество слоев</label>
                           <input name="layerNorm" type="text" class="form-control" placeholder="layerNorm">
                        </div>

                        <button type="submit" class="btn btn-default">Запрос на сервер</button>
                    </form>
                </div>  
            </div>
        </div>

        <div class="container-fluid method">
            <a name="api.addCTech" class="anchor"></a>
            <h2>Метод <code>/api/addCTech</code></h2>
            <h3>Описание</h3>
            <p>Создает новую аттестацию технологий.</p>
            <p>Метод просматривает все аттестации пользователя, если есть незакрытая аттестация,
              то возвращается ID незакрытой аттестации. Таким образом пользователь может продолжить работу в случае утери
              соединения, ошибки программы или сервера.</p>

            <h3>Возвращает:</h3>
            <dl class="dl-horizontal">
              <dt><code>success:boolean</code></dt>
              <dd>Успеность выполнения запроса</dd>
            </dl>

            <dl class="dl-horizontal">
              <dt><code>errorMessage:string</code></dt>
              <dd>Описывает ошибку, если <code>success == false</code></dd>
            </dl>

            <dl class="dl-horizontal">
              <dt><code>certId:int</code></dt>
              <dd>ID аттестации</dd>
            </dl>

            <h3>Возможные ошибки в <code>errorMessage</code>:</h3>
                <ul>
                    <li>Не указан ID пользователя</li>
                    <li>Не указан ID организации</li>
                    <li>Не указано имя работника</li>
                    <li>Не указано клеймо сварщика</li>
                    <li>Не указан тип КСС</li>
                    <li>Не указан способ соединения</li>
                    <li>Не указано имя программы</li>
                    <li>Не указан первый диаметр</li>
                    <li>Не указан второй диаметр</li>
                    <li>Не указана первая толщина</li>
                    <li>Не указана вторая толщина</li>
                    <li>Не указана широта</li>
                    <li>Не указана долгота</li>
                    <li>Не указано нормативное количество слоев</li>
                    <li>Не указана дата начала аттестации</li>
                    <li>Тип соединения указан неверно</li>
                    <li>Пользователя с указанным ID не существует</li>
                    <li>Создавать аттестации могут только авторизованные пользователи</li>
                </ul>
            <h3>Протестировать метод:</h3>
            <div class="panel">
                <div class="panel-body method">
                    <form class="pure-form live" action="/api/addCTech" method="post">
                        <div class="form-group">
                           <label><code>userId:int</code> - ID пользователя</label>
                           <input name="userId" type="text" class="form-control" placeholder="userId"> 
                        </div>

                        <div class="form-group">
                           <label><code>orgId:int</code> - ID организации</label>
                           <input name="orgId" type="text" class="form-control" placeholder="orgId">
                        </div>

                        <div class="form-group">
                           <label><code>workerName:string</code> - ФИО сварщика</label>
                           <input name="workerName" type="text" class="form-control" placeholder="workerName">
                        </div>

                        <div class="form-group">
                           <label><code>workerStump:string</code> - клеймо сварщика</label>
                           <input name="workerStump" type="text" class="form-control" placeholder="workerStump">
                        </div>

                        <div class="form-group">
                           <label><code>jointType:string</code> - тип соединения (например: "Лист/Труба")</label>
                           <input name="jointType" type="text" class="form-control" placeholder="jointType">
                        </div>

                        <div class="form-group">
                           <label><code>kccType:string</code> - тип КСС соединения</label>
                           <input name="kccType" type="text" class="form-control" placeholder="kccType">
                        </div>

                        <div class="form-group">
                           <label><code>dFirst:float</code> - первый диаметр</label>
                           <input name="dFirst" type="text" class="form-control" placeholder="dFirst">
                        </div>

                        <div class="form-group">
                           <label><code>dSecond:float</code> - второй диаметр</label>
                           <input name="dSecond" type="text" class="form-control" placeholder="dSecond">
                        </div>

                        <div class="form-group">
                           <label><code>hFirst:float</code> - первая толщина</label>
                           <input name="hFirst" type="text" class="form-control" placeholder="hFirst">
                        </div>

                        <div class="form-group">
                           <label><code>hSecond:float</code> - вторая толщина</label>
                           <input name="hSecond" type="text" class="form-control" placeholder="hSecond">
                        </div>


                        <div class="form-group">
                           <label><code>lon:double</code> - широта (координаты)</label>
                           <input name="lon" type="text" class="form-control" placeholder="lon">
                        </div>


                        <div class="form-group">
                           <label><code>lat:double</code> - долгота (координаты)</label>
                           <input name="lat" type="text" class="form-control" placeholder="lat">
                        </div>

                        <div class="form-group">
                           <label><code>programName:string</code> - название программы</label>
                           <input name="programName" type="text" class="form-control" placeholder="programName">
                        </div>

                        <div class="form-group">
                           <label><code>startDate:datetime</code> - дата начала аттестации (в формате MySQL)</label>
                           <input name="startDate" type="text" class="form-control" placeholder="startDate">
                        </div>

                        <div class="form-group">
                           <label><code>layerNorm:int</code> - нормативное количество слоев</label>
                           <input name="layerNorm" type="text" class="form-control" placeholder="layerNorm">
                        </div>

                        <button type="submit" class="btn btn-default">Запрос на сервер</button>
                    </form>
                </div>  
            </div>
        </div>

        <div class="container-fluid method">
            <a name="api.addCEquip" class="anchor"></a>
            <h2>Метод <code>/api/addCEquip</code></h2>
            <h3>Описание</h3>
            <p>Создает новую аттестацию оборудования.</p>
            <p>Метод просматривает все аттестации пользователя, если есть незакрытая аттестация <b>с параметрами, аналогичными
              введенным</b>, то есть должны совпадать <code>orgId</code>, <code>tradeMark</code>, <code>factoryNum</code>,
              <code>invNum</code>, <code>weldMethod</code>, <code>programName</code>,
              то возвращается ID незакрытой аттестации. Таким образом пользователь может продолжить работу в случае утери
              соединения, ошибки программы или сервера.</p>

            <h3>Возвращает:</h3>
            <dl class="dl-horizontal">
              <dt><code>success:boolean</code></dt>
              <dd>Успеность выполнения запроса</dd>
            </dl>

            <dl class="dl-horizontal">
              <dt><code>errorMessage:string</code></dt>
              <dd>Описывает ошибку, если <code>success == false</code></dd>
            </dl>

            <dl class="dl-horizontal">
              <dt><code>certId:int</code></dt>
              <dd>ID аттестации</dd>
            </dl>

            <dl class="dl-horizontal">
              <dt><code>status:boolean</code></dt>
              <dd>Указывает статус специальных испытаний. <code>true</code> -  специальные испытания выполнены, <code>false</code> - не выполнены.</dd>
            </dl>

            <h3>Возможные ошибки в <code>errorMessage</code>:</h3>
                <ul>
                    <li>Не указан ID пользователя</li>
                    <li>Не указан ID организации</li>
                    <li>Не указана марка оборудования</li>
                    <li>Не указан заводской номер</li>
                    <li>Не указан инвертарный номер</li>
                    <li>Не указан тип сварки</li>
                    <li>Не указано имя программы</li>
                    <li>Пользователя с указанным ID не существует</li>
                    <li>Работать с аттестациями могут только авторизованные пользователи</li>
                </ul>
            <h3>Протестировать метод:</h3>
            <div class="panel">
                <div class="panel-body method">
                    <form class="pure-form live" action="/api/addCEquip" method="post">
                        <div class="form-group">
                           <label><code>userId:int</code> - ID пользователя</label>
                           <input name="userId" type="text" class="form-control" placeholder="userId"> 
                        </div>

                        <div class="form-group">
                           <label><code>orgId:int</code> - ID организации</label>
                           <input name="orgId" type="text" class="form-control" placeholder="orgId">
                        </div>

                        <div class="form-group">
                           <label><code>factoryNum:string</code> - заводской номер</label>
                           <input name="factoryNum" type="text" class="form-control" placeholder="factoryNum">
                        </div>

                        <div class="form-group">
                           <label><code>tradeMark:string</code> - марка оборудования</label>
                           <input name="tradeMark" type="text" class="form-control" placeholder="tradeMark">
                        </div>


                        <div class="form-group">
                           <label><code>invNum:string</code> - инвертарный номер</label>
                           <input name="invNum" type="text" class="form-control" placeholder="invNum">
                        </div>

                        <div class="form-group">
                           <label><code>weldMethod:string</code> - метод сварки</label>
                           <input name="weldMethod" type="text" class="form-control" placeholder="weldMethod">
                        </div>

                        <div class="form-group">
                           <label><code>programName:string</code> - название программы</label>
                           <input name="programName" type="text" class="form-control" placeholder="programName">
                        </div>

                        <button type="submit" class="btn btn-default">Запрос на сервер</button>
                    </form>
                </div>  
            </div>
        </div>

        <div class="container-fluid method">
            <a name="api.addCEquipSpec" class="anchor"></a>
            <h2>Метод <code>/api/addCEquipSpec</code></h2>
            <h3>Описание</h3>
            <p>Создает специальные испытания в рамках аттестации оборудования. <b>Внимание!</b> Если вызвать это метод несколько раз с одним ID
              испытаний оборудования, вы потеряете результаты предыдущих испытаний.</p>

            <h3>Возвращает:</h3>
            <dl class="dl-horizontal">
              <dt><code>success:boolean</code></dt>
              <dd>Успеность выполнения запроса</dd>
            </dl>

            <dl class="dl-horizontal">
              <dt><code>errorMessage:string</code></dt>
              <dd>Описывает ошибку, если <code>success == false</code></dd>
            </dl>

            <h3>Возможные ошибки в <code>errorMessage</code>:</h3>
                <ul>
                    <li>Не указан ID аттестации оборудования</li>
                    <li>Не указана дата начала аттестации</li>
                    <li>Не указана дата окончания аттестации</li>
                    <li>Не указана долгота (координаты)</li>
                    <li>Не указана широта (координаты)</li>
                    <li>Не указано минимальное напряжение</li>
                    <li>Не указана минимальная сила тока</li>
                    <li>Не указано максимальное напряжение</li>
                    <li>Не указана максимальная сила тока</li>
                    <li>Не указано напряжение</li>
                    <li>Не указана сила тока</li>
                    <li>Аттестации оборудования с указанным ID не существует</li>
                </ul>
            <h3>Протестировать метод:</h3>
            <div class="panel">
                <div class="panel-body method">
                    <form class="pure-form live" action="/api/addCEquipSpec" method="post">
                        <div class="form-group">
                           <label><code>certId:int</code> - ID аттестации оборудования</label>
                           <input name="certId" type="text" class="form-control" placeholder="certId"> 
                        </div>

                        <div class="form-group">
                           <label><code>startDate:datetime</code> - дата и время начала аттестации (в формате MySQL)</label>
                           <input name="startDate" type="text" class="form-control" placeholder="startDate">
                        </div>

                        <div class="form-group">
                           <label><code>endDate:datetime</code> - дата и время окончания аттестации (в формате MySQL)</label>
                           <input name="endDate" type="text" class="form-control" placeholder="endDate">
                        </div>

                        <div class="form-group">
                           <label><code>lon:double</code> - широта (координаты)</label>
                           <input name="lon" type="text" class="form-control" placeholder="lon">
                        </div>


                        <div class="form-group">
                           <label><code>lat:double</code> - долгота (координаты)</label>
                           <input name="lat" type="text" class="form-control" placeholder="lat">
                        </div>

                        <div class="form-group">
                           <label><code>minU:float</code></label>
                           <input name="minU" type="text" class="form-control" placeholder="minU">
                        </div>
                        
                        <div class="form-group">
                           <label><code>minI:float</code></label>
                           <input name="minI" type="text" class="form-control" placeholder="minI">
                        </div>

                        <div class="form-group">
                           <label><code>maxU:float</code></label>
                           <input name="maxU" type="text" class="form-control" placeholder="maxU">
                        </div>

                        <div class="form-group">
                           <label><code>maxI:float</code></label>
                           <input name="maxI" type="text" class="form-control" placeholder="maxI">
                        </div>

                        <div class="form-group">
                           <label><code>normU:float</code></label>
                           <input name="normU" type="text" class="form-control" placeholder="normU">
                        </div>

                        <div class="form-group">
                           <label><code>normI:float</code></label>
                           <input name="normI" type="text" class="form-control" placeholder="normI">
                        </div>

                        <button type="submit" class="btn btn-default">Запрос на сервер</button>
                    </form>
                </div>  
            </div>
        </div>

        <div class="container-fluid method">
            <a name="api.addCEquipPrac" class="anchor"></a>
            <h2>Метод <code>/api/addCEquipPrac</code></h2>
            <h3>Описание</h3>
            <p>Создает практические испытания в рамках аттестации оборудования.</p>

            <h3>Возвращает:</h3>
            <dl class="dl-horizontal">
              <dt><code>success:boolean</code></dt>
              <dd>Успеность выполнения запроса</dd>
            </dl>

            <dl class="dl-horizontal">
              <dt><code>pracId:int</code></dt>
              <dd>ID практических испытаний</dd>
            </dl>

            <dl class="dl-horizontal">
              <dt><code>errorMessage:string</code></dt>
              <dd>Описывает ошибку, если <code>success == false</code></dd>
            </dl>

            <h3>Возможные ошибки в <code>errorMessage</code>:</h3>
                <ul>
                    <li>Не указан ID аттестации оборудования</li>
                    <li>Не указано имя работника</li>
                    <li>Не указано клеймо сварщика</li>
                    <li>Не указан тип КСС</li>
                    <li>Не указан способ соединения</li>
                    <li>Не указан первый диаметр</li>
                    <li>Не указан второй диаметр</li>
                    <li>Не указана первая толщина</li>
                    <li>Не указана вторая толщина</li>
                    <li>Не указана широта</li>
                    <li>Не указана долгота</li>
                    <li>Не указано нормативное количество слоев</li>
                    <li>Не указана дата начала аттестации</li>
                    <li>Тип соединения указан неверно</li>
                    <li>Пользователя с указанным ID не существует</li>
                    <li>Создавать аттестации могут только авторизованные пользователи</li>
                </ul>
            <h3>Протестировать метод:</h3>
            <div class="panel">
                <div class="panel-body method">
                    <form class="pure-form live" action="/api/addCEquipPrac" method="post">
                        <div class="form-group">
                           <label><code>pracId:int</code> - ID аттестации оборудования</label>
                           <input name="pracId" type="text" class="form-control" placeholder="pracId"> 
                        </div>

                        <div class="form-group">
                           <label><code>workerName:string</code> - ФИО сварщика</label>
                           <input name="workerName" type="text" class="form-control" placeholder="workerName">
                        </div>

                        <div class="form-group">
                           <label><code>workerStump:string</code> - клеймо сварщика</label>
                           <input name="workerStump" type="text" class="form-control" placeholder="workerStump">
                        </div>

                        <div class="form-group">
                           <label><code>jointType:string</code> - тип соединения (например: "Лист/Труба")</label>
                           <input name="jointType" type="text" class="form-control" placeholder="jointType">
                        </div>

                        <div class="form-group">
                           <label><code>kccType:string</code> - тип КСС соединения</label>
                           <input name="kccType" type="text" class="form-control" placeholder="kccType">
                        </div>

                        <div class="form-group">
                           <label><code>dFirst:float</code> - первый диаметр</label>
                           <input name="dFirst" type="text" class="form-control" placeholder="dFirst">
                        </div>

                        <div class="form-group">
                           <label><code>dSecond:float</code> - второй диаметр</label>
                           <input name="dSecond" type="text" class="form-control" placeholder="dSecond">
                        </div>

                        <div class="form-group">
                           <label><code>hFirst:float</code> - первая толщина</label>
                           <input name="hFirst" type="text" class="form-control" placeholder="hFirst">
                        </div>

                        <div class="form-group">
                           <label><code>hSecond:float</code> - вторая толщина</label>
                           <input name="hSecond" type="text" class="form-control" placeholder="hSecond">
                        </div>


                        <div class="form-group">
                           <label><code>lon:double</code> - широта (координаты)</label>
                           <input name="lon" type="text" class="form-control" placeholder="lon">
                        </div>


                        <div class="form-group">
                           <label><code>lat:double</code> - долгота (координаты)</label>
                           <input name="lat" type="text" class="form-control" placeholder="lat">
                        </div>

                        <div class="form-group">
                           <label><code>startDate:datetime</code> - дата и время начала аттестации (в формате MySQL)</label>
                           <input name="startDate" type="text" class="form-control" placeholder="startDate">
                        </div>

                        <div class="form-group">
                           <label><code>layerNorm:int</code> - нормативное количество слоев</label>
                           <input name="layerNorm" type="text" class="form-control" placeholder="layerNorm">
                        </div>

                        <button type="submit" class="btn btn-default">Запрос на сервер</button>
                    </form>
                </div>  
            </div>
        </div>

        <div class="container-fluid method">
            <a name="api.addParameter" class="anchor"></a>
            <h2>Метод <code>/api/addParameter</code></h2>
            <h3>Описание</h3>
            <p>Добавляет слой в конкретную аттестацию. Параметр <code>certType</code> может принимать значения <code>0</code>, <code>1</code>,
              и <code>2</code> для занесения слоя для аттестации сварщиков, технологий и оборудования соответсвенно. При неправильном
              указании параметра слой не отобразится в аттестации.</p>
            <p>Параметр <code>position</code> может принимать значения <code>от 1 до 5</code>: 
              <ul>
                <li>Нижнее (1)</li>
                <li>На спуск (2)</li>
                <li>Потолочное (3)</li>
                <li>На подъем (4)</li>
                <li>Горизонтальное (5)</li>
              </ul>
            </p>

            <p><b>Изменена политика отправки файлов регистограмм.</b> Архив с файлом и файл внутри него могут иметь произвольные имена.
              Например, архив может всегда иметь название <code>archive.zip</code>, а файл внутри <code>data.csv</code>, сервер
              самостоятельно присвоит им уникальные имена. Файлы регистограмм обрабатываются периодически во времени с некоторым
              промежутком (~раз в 2 минуты). Поэтому не стоит ожидать, что слой обработается мгновенно. Чтобы избежать перегрузок
              скрипт обрабатывает по 3 файла регистограм за раз, поэтому если будет загружено много файлов за короткий промежуток времени,
              то обработка может затянутся.</p>

            <h3>Возвращает:</h3>
            <dl class="dl-horizontal">
              <dt><code>success:boolean</code></dt>
              <dd>Успеность выполнения запроса</dd>
            </dl>

            <dl class="dl-horizontal">
              <dt><code>errorMessage:string</code></dt>
              <dd>Описывает ошибку, если <code>success == false</code></dd>
            </dl>

            <h3>Возможные ошибки в <code>errorMessage</code>:</h3>
                <ul>
                    <li>Не указан ID аттестации</li>
                    <li>Не указан тип аттестации</li>
                    <li>Не указан номер слоя</li>
                    <li>Не указана позиция</li>
                    <li>Не указано время начала сварки</li>
                    <li>Не указано время окончания сварки</li>
                    <li>Тип аттестации указан неверно (допускаются значения 0-2)</li>
                    <li>Позиция указана неверно (допускаются значения 1-5)</li>
                    <li>Файл регистограммы обязатлен</li>
                </ul>
            <h3>Протестировать метод:</h3>
            <div class="panel">
                <div class="panel-body method">
                    <form class="pure-form live" enctype="multipart/form-data" action="/api/addParameter" method="post">
                        <div class="form-group">
                           <label><code>certId:int</code> - ID аттестации оборудования</label>
                           <input name="certId" type="text" class="form-control" placeholder="certId"> 
                        </div>

                        <div class="form-group">
                           <label><code>certType:int</code> - тип аттестации</label>
                           <input name="certType" type="text" class="form-control" placeholder="certType"> 
                        </div>

                        <div class="form-group">
                           <label><code>layer:int</code> - номер слоя</label>
                           <input name="layer" type="text" class="form-control" placeholder="layer"> 
                        </div>

                        <div class="form-group">
                           <label><code>position:int</code> - позиция слоя</label>
                           <input name="position" type="text" class="form-control" placeholder="position"> 
                        </div>

                        <div class="form-group">
                           <label><code>startDate:datetime</code> - дата и время начала сварки (в формате MySQL)</label>
                           <input name="startDate" type="text" class="form-control" placeholder="startDate">
                        </div>

                        <div class="form-group">
                           <label><code>endDate:datetime</code> - дата и время окончания сварки (в формате MySQL)</label>
                           <input name="endDate" type="text" class="form-control" placeholder="endDate">
                        </div>

                        <div class="form-group">
                          <label><code>regFile</code> - файл регистограммы</label>
                          <input name="regFile" type="file" class="form-control" placeholder="regFile">
                        </div>

                        <div class="form-group">
                          <label><code>photo</code> - фотография</label>
                          <input name="photo" type="file" class="form-control" placeholder="photo">
                        </div>

                        <button type="submit" class="btn btn-default">Запрос на сервер</button>
                    </form>
                </div>  
            </div>
        </div>

        <div class="container-fluid method">
            <a name="api.finishCWelder" class="anchor"></a>
            <h2>Метод <code>/api/finishCWelder</code></h2>
            <h3>Описание</h3>
            <p>Завершает аттестацию сварщика. <b>После применения этого метода аттестация становится видна на сайте.</b></p>

            <h3>Возвращает:</h3>
            <dl class="dl-horizontal">
              <dt><code>success:boolean</code></dt>
              <dd>Успеность выполнения запроса</dd>
            </dl>

            <dl class="dl-horizontal">
              <dt><code>errorMessage:string</code></dt>
              <dd>Описывает ошибку, если <code>success == false</code></dd>
            </dl>

            <h3>Возможные ошибки в <code>errorMessage</code>:</h3>
                <ul>
                    <li>Не указан ID аттестации</li>
                    <li>Не указана дата окончания аттестации</li>
                    <li>Не указано время проведения аттестации</li>
                    <li>Не указано количество слоев</li>
                    <li>Аттестации сварщика с таким ID не существует</li>
                </ul>
            <h3>Протестировать метод:</h3>
            <div class="panel">
                <div class="panel-body method">
                    <form class="pure-form live" enctype="multipart/form-data" action="/api/finishCWelder" method="post">
                        <div class="form-group">
                           <label><code>certId:int</code> - ID аттестации</label>
                           <input name="certId" type="text" class="form-control" placeholder="certId"> 
                        </div>

                        <div class="form-group">
                           <label><code>layerCount:int</code> - количество слоев</label>
                           <input name="layerCount" type="text" class="form-control" placeholder="layerCount"> 
                        </div>

                        <div class="form-group">
                           <label><code>time:time</code> - время аттестации (в формате MySQL)</label>
                           <input name="time" type="text" class="form-control" placeholder="time"> 
                        </div>

                        <div class="form-group">
                           <label><code>endDate:datetime</code> - дата и время окончания сварки (в формате MySQL)</label>
                           <input name="endDate" type="text" class="form-control" placeholder="endDate">
                        </div>

                        <div class="form-group">
                          <label><code>photoFirst</code> - первая фотография</label>
                          <input name="photoFirst" type="file" class="form-control" placeholder="photoFirst">
                        </div>

                        <div class="form-group">
                          <label><code>photoSecond</code> - вторая фотография</label>
                          <input name="photoSecond" type="file" class="form-control" placeholder="photoSecond">
                        </div>

                        <div class="form-group">
                          <label><code>finishPhoto</code> - итоговая фотография</label>
                          <input name="finishPhoto" type="file" class="form-control" placeholder="finishPhoto">
                        </div>

                        <button type="submit" class="btn btn-default">Запрос на сервер</button>
                    </form>
                </div>  
            </div>
        </div>

        <div class="container-fluid method">
            <a name="api.finishCTech" class="anchor"></a>
            <h2>Метод <code>/api/finishCTech</code></h2>
            <h3>Описание</h3>
            <p>Завершает аттестацию технологий. <b>После применения этого метода аттестация становится видна на сайте.</b></p>

            <h3>Возвращает:</h3>
            <dl class="dl-horizontal">
              <dt><code>success:boolean</code></dt>
              <dd>Успеность выполнения запроса</dd>
            </dl>

            <dl class="dl-horizontal">
              <dt><code>errorMessage:string</code></dt>
              <dd>Описывает ошибку, если <code>success == false</code></dd>
            </dl>

            <h3>Возможные ошибки в <code>errorMessage</code>:</h3>
                <ul>
                    <li>Не указан ID аттестации</li>
                    <li>Не указана дата окончания аттестации</li>
                    <li>Не указано время проведения аттестации</li>
                    <li>Не указано количество слоев</li>
                    <li>Аттестации сварщика с таким ID не существует</li>
                </ul>
            <h3>Протестировать метод:</h3>
            <div class="panel">
                <div class="panel-body method">
                    <form class="pure-form live" enctype="multipart/form-data" action="/api/finishCTech" method="post">
                        <div class="form-group">
                           <label><code>certId:int</code> - ID аттестации</label>
                           <input name="certId" type="text" class="form-control" placeholder="certId"> 
                        </div>

                        <div class="form-group">
                           <label><code>layerCount:int</code> - количество слоев</label>
                           <input name="layerCount" type="text" class="form-control" placeholder="layerCount"> 
                        </div>

                        <div class="form-group">
                           <label><code>time:time</code> - время аттестации (в формате MySQL)</label>
                           <input name="time" type="text" class="form-control" placeholder="time"> 
                        </div>

                        <div class="form-group">
                           <label><code>endDate:datetime</code> - дата и время окончания сварки (в формате MySQL)</label>
                           <input name="endDate" type="text" class="form-control" placeholder="endDate">
                        </div>

                        <div class="form-group">
                          <label><code>photoFirst</code> - первая фотография</label>
                          <input name="photoFirst" type="file" class="form-control" placeholder="photoFirst">
                        </div>

                        <div class="form-group">
                          <label><code>photoSecond</code> - вторая фотография</label>
                          <input name="photoSecond" type="file" class="form-control" placeholder="photoSecond">
                        </div>

                        <div class="form-group">
                          <label><code>finishPhoto</code> - итоговая фотография</label>
                          <input name="finishPhoto" type="file" class="form-control" placeholder="finishPhoto">
                        </div>

                        <button type="submit" class="btn btn-default">Запрос на сервер</button>
                    </form>
                </div>  
            </div>
        </div>

        <div class="container-fluid method">
            <a name="api.finishCEquip" class="anchor"></a>
            <h2>Метод <code>/api/finishCEquip</code></h2>
            <h3>Описание</h3>
            <p>Завершает аттестацию оборудования. <b>После применения этого метода в аттестацию больше нельзя
              добавлять практические испытания.</b></p>

            <h3>Возвращает:</h3>
            <dl class="dl-horizontal">
              <dt><code>success:boolean</code></dt>
              <dd>Успеность выполнения запроса</dd>
            </dl>

            <dl class="dl-horizontal">
              <dt><code>errorMessage:string</code></dt>
              <dd>Описывает ошибку, если <code>success == false</code></dd>
            </dl>

            <h3>Возможные ошибки в <code>errorMessage</code>:</h3>
                <ul>
                    <li>Аттестации оборудрвания с указанным ID не существует</li>
                </ul>
            <h3>Протестировать метод:</h3>
            <div class="panel">
                <div class="panel-body method">
                    <form class="pure-form live" enctype="multipart/form-data" action="/api/finishCEquip" method="post">
                        <div class="form-group">
                           <label><code>certId:int</code> - ID аттестации</label>
                           <input name="certId" type="text" class="form-control" placeholder="certId"> 
                        </div>

                        <button type="submit" class="btn btn-default">Запрос на сервер</button>
                    </form>
                </div>  
            </div>
        </div>

        <div class="container-fluid method">
            <a name="api.finishCEquipPrac" class="anchor"></a>
            <h2>Метод <code>/api/finishCEquipPrac</code></h2>
            <h3>Описание</h3>
            <p>Завершает практические испытания. <b>После применения этого метода практические испытания становятся видны на сайте.</b></p>

            <h3>Возвращает:</h3>
            <dl class="dl-horizontal">
              <dt><code>success:boolean</code></dt>
              <dd>Успеность выполнения запроса</dd>
            </dl>

            <dl class="dl-horizontal">
              <dt><code>errorMessage:string</code></dt>
              <dd>Описывает ошибку, если <code>success == false</code></dd>
            </dl>

            <h3>Возможные ошибки в <code>errorMessage</code>:</h3>
                <ul>
                    <li>Не указан ID аттестации</li>
                    <li>Не указана дата окончания аттестации</li>
                    <li>Не указано время проведения аттестации</li>
                    <li>Не указано количество слоев</li>
                    <li>Аттестации сварщика с таким ID не существует</li>
                </ul>
            <h3>Протестировать метод:</h3>
            <div class="panel">
                <div class="panel-body method">
                    <form class="pure-form live" enctype="multipart/form-data" action="/api/finishCEquipPrac" method="post">
                        <div class="form-group">
                           <label><code>pracId:int</code> - ID практических испытаний</label>
                           <input name="pracId" type="text" class="form-control" placeholder="pracId"> 
                        </div>

                        <div class="form-group">
                           <label><code>layerCount:int</code> - количество слоев</label>
                           <input name="layerCount" type="text" class="form-control" placeholder="layerCount"> 
                        </div>

                        <div class="form-group">
                           <label><code>time:time</code> - время аттестации (в формате MySQL)</label>
                           <input name="time" type="text" class="form-control" placeholder="time"> 
                        </div>

                        <div class="form-group">
                           <label><code>endDate:datetime</code> - дата и время окончания сварки (в формате MySQL)</label>
                           <input name="endDate" type="text" class="form-control" placeholder="endDate">
                        </div>

                        <div class="form-group">
                          <label><code>photoFirst</code> - первая фотография</label>
                          <input name="photoFirst" type="file" class="form-control" placeholder="photoFirst">
                        </div>

                        <div class="form-group">
                          <label><code>photoSecond</code> - вторая фотография</label>
                          <input name="photoSecond" type="file" class="form-control" placeholder="photoSecond">
                        </div>

                        <div class="form-group">
                          <label><code>finishPhoto</code> - итоговая фотография</label>
                          <input name="finishPhoto" type="file" class="form-control" placeholder="finishPhoto">
                        </div>

                        <button type="submit" class="btn btn-default">Запрос на сервер</button>
                    </form>
                </div>  
            </div>
        </div>

        <div class="container-fluid method">
            <a name="api.checkOrganization" class="anchor"></a>
            <h2>Метод <code>/api/checkOrganization</code></h2>
            <h3>Описание</h3>
            <p>Проверяет на существование органицию.</b></p>

            <h3>Возвращает:</h3>
            <dl class="dl-horizontal">
              <dt><code>success:boolean</code></dt>
              <dd>Успеность выполнения запроса</dd>
            </dl>

            <dl class="dl-horizontal">
              <dt><code>errorMessage:string</code></dt>
              <dd>Описывает ошибку, если <code>success == false</code></dd>
            </dl>

            <dl class="dl-horizontal">
              <dt><code>orgId:int</code></dt>
              <dd>ID организации</dd>
            </dl>

            <dl class="dl-horizontal">
              <dt><code>orgName:string</code></dt>
              <dd>Название организации</dd>
            </dl>

            <h3>Возможные ошибки в <code>errorMessage</code>:</h3>
                <ul>
                    <li>ИНН не указан</li>
                    <li>Организации с указанным ИНН не существует</li>
                </ul>
            <h3>Протестировать метод:</h3>
            <div class="panel">
                <div class="panel-body method">
                    <form class="pure-form live" action="/api/checkOrganization" method="post">
                        <div class="form-group">
                           <label><code>inn:string</code> - ИНН организации</label>
                           <input name="inn" type="text" class="form-control" placeholder="inn"> 
                        </div>

                        <button type="submit" class="btn btn-default">Запрос на сервер</button>
                    </form>
                </div>  
            </div>
        </div>

        <div class="container-fluid method">
            <a name="api.addCEquipVac" class="anchor"></a>
            <h2>Метод <code>/api/addCEquipVac</code></h2>
            <h3>Описание</h3>
            <p>Добавляет точку ВАХ для опреденной аттестации оборудования. Параметр <code>mode</code> может принимать значения
              <code>0</code>, <code>1</code> и <code>2</code> для режимов MIN, NORM и MAX соответсвенно.</p>

            <h3>Возвращает:</h3>
            <dl class="dl-horizontal">
              <dt><code>success:boolean</code></dt>
              <dd>Успеность выполнения запроса</dd>
            </dl>

            <dl class="dl-horizontal">
              <dt><code>errorMessage:string</code></dt>
              <dd>Описывает ошибку, если <code>success == false</code></dd>
            </dl>

            <h3>Возможные ошибки в <code>errorMessage</code>:</h3>
                <ul>
                    <li>Не указан ID аттестации</li>
                    <li>Не указан режим (mode)</li>
                    <li>Не указано значение напряжения</li>
                    <li>Не указано значение силы тока</li>
                </ul>
            <h3>Протестировать метод:</h3>
            <div class="panel">
                <div class="panel-body method">
                    <form class="pure-form live" enctype="multipart/form-data" action="/api/addCEquipVac" method="post">
                        <div class="form-group">
                           <label><code>certId:int</code> - ID аттестации оборудования</label>
                           <input name="certId" type="text" class="form-control" placeholder="certId"> 
                        </div>

                        <div class="form-group">
                           <label><code>mode:int</code> - режим</label>
                           <input name="mode" type="text" class="form-control" placeholder="mode"> 
                        </div>

                        <div class="form-group">
                           <label><code>u:float</code> - напряжение</label>
                           <input name="u" type="text" class="form-control" placeholder="u"> 
                        </div>

                        <div class="form-group">
                           <label><code>i:float</code> - сила тока</label>
                           <input name="i" type="text" class="form-control" placeholder="i"> 
                        </div>

                        <button type="submit" class="btn btn-default">Запрос на сервер</button>
                    </form>
                </div>  
            </div>
        </div>

        <div class="container-fluid method">
            <a name="api.logout" class="anchor"></a>
            <h2>Метод <code>/api/logout</code></h2>
            <h3>Описание</h3>
            <p>Закрывает сессию пользователя. Переводит его в режим оффлайн.</b></p>

            <h3>Возвращает:</h3>
            <dl class="dl-horizontal">
              <dt><code>success:boolean</code></dt>
              <dd>Успеность выполнения запроса</dd>
            </dl>

            <dl class="dl-horizontal">
              <dt><code>errorMessage:string</code></dt>
              <dd>Описывает ошибку, если <code>success == false</code></dd>
            </dl>

            <h3>Возможные ошибки в <code>errorMessage</code>:</h3>
                <ul>
                    <li>Не указан ID пользователя</li>
                    <li>Пользователя с указанным ID не существует</li>
                </ul>
            <h3>Протестировать метод:</h3>
            <div class="panel">
                <div class="panel-body method">
                    <form class="pure-form live" action="/api/logout" method="post">
                        <div class="form-group">
                           <label><code>userId:int</code> - ID пользователя</label>
                           <input name="userId" type="text" class="form-control" placeholder="userId"> 
                        </div>

                        <button type="submit" class="btn btn-default">Запрос на сервер</button>
                    </form>
                </div>  
            </div>
        </div>
    </div>
</div>
<?endif;?>

</body>
</html>