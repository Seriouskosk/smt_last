<?php ?> 
<? $this->setPageTitle('Юридические лица');?>
<div class="navbar navbar-default navbar-fixed-top">
  <div class="container">

    <div class="navbar-collapse collapse" id="navbar-main">
      <ul class="nav navbar-nav">
        <li class="">
          <a href="/certification/all" id="themes"><i class="fa fa-user"></i> Юридические лица</a>
        </li> 
      </ul>
       <ul class="nav navbar-nav navbar-right">
        <li>
            <a href="/messages"><i class="fa fa-weixin"></i> Обратная связь</a>
        </li>
        <? if(Yii::app()->user->checkAccess(User::ROLE_ADMIN)): ?>
        <li class="dropdown" ng-controller="ModalNews">

            <script type="text/ng-template" id="myModalContent2.html">
                <div ng-controller="addNewsCtrl" class="container app">
                    <h3>Добавить новость</h3>
                    <input ng-model="titlenews" type="text" class="form-control">
                    <div text-angular="text-angular" name="htmlcontent" ng-model="htmlcontent" ta-disabled='disabled'></div>
                    <h4>Добавить картинку</h4>
                    <p class="help-block">Ждите пока появится в редакторе</p>
                    <input type='file' id='uplodaingImage' class=''/>
                    <br>
                    <button class="btn btn-default" ng-click="addNewsAjax();cancel()"><p><i class="fa fa-book"></i> Добавить новость</p></button>
                    <button class="btn btn-default" ng-click="cancel()"><p><i class="fa fa-book"></i> Отменить</p></button>
                    <br><br>
                </div>
            </script>
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes3"><i class="fa fa-book"></i> Новости <span class="caret"></span></a>
            <ul class="dropdown-menu" aria-labelledby="themes3">
                <li>
                    <a href="#" ng-click="open('lg');"><i class="fa fa-book"></i> Добавить новость</a>
                </li>
                <li>
                    <a href="<?=Yii::app()->createUrl("main/news")?>"><i class="fa fa-pencil"></i> Управление новостями</a>
                </li>
            </ul>

        </li>
        <? endif ?>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes3"><i class="fa fa-bars"></i> Меню <span class="caret"></span></a>
            <ul class="dropdown-menu" aria-labelledby="themes3">
                <li>
                    <a href="<?=Yii::app()->createUrl("main/logout")?>"><i class="fa fa-sign-out"></i> <?=Yii::app()->topMenu->getUsername()?> (выход)</a>
                </li>
                <li>
		            <a href="/forum"><i class="fa fa-comments-o"></i> Перейти на форум</a>
		        </li>  
            </ul> 
        </li>
      </ul>
    </div>
  </div>
</div>

<div id="wrapper" ng-controller="ajaxCtrlCenters">
    <?if(!Yii::app()->user->getIsGuest()):?>
        <div id="chatWithAdmin"  ng-controller="chat" ng-class="{'button': !isActive, 'button active': isActive}">
            <div class="chatLeftSide">
                <div class="scrollerChat" ng-init="getMessage(<?=$userId?>)">
                    <table class="table table-striped ">
                        <tr ng-repeat="message in messages | orderBy:predicate:!reverse">
                            <td>
                                <div class="topData">
                                    <div class="chatName"><b>{{message.name}}</b></div>
                                    <div class="chatData">{{message.date}}</div>
                                </div>
                                <div class="message">{{message.message}}</div>
                            </td>
                        </tr>
                    </table>
                </div>
                <textarea ng-model="ta_model" name="" id="" class="wide" rows="5"></textarea>
                <button ng-click="sendMessage(<?=$userId?>)" class="btn btn-default pull-right">Отправить</button>
            </div>
            <div class="chatRightSide"
                 ng-init="isActive = false"
                 ng-click="isActive = !isActive">
                <div class="bl"></div>
                <div class="vertical-text">Связь с администрацией</div>
            </div>
        </div>
    <?endif;?>
	 
	<div id="workPanel">
	 
	    <div class="">
	        <script type="text/ng-template" id="myModalContent.html">
	      		<div>
		            <div class="modal-header">
		                <h3 class="modal-title">Добавить юридическое лицо</h3>
		            </div>
		            <div class="modal-body"  ng-init="scope = { isDisabled1: false, isDisabled2: false, isDisabled3: false }">
		                <input type="text" ng-model="score1" class="inputCSS2" name="name"  placeholder="Название" required/>
		                <input type="text" ng-model="score2" ng-pattern="/^-?[0-9]\d*(\.\d+)?$/" class="inputCSS2" name="phone"  placeholder="Телефон" required/>
		                <input type="text" ng-model="city" class="inputCSS2" name="city"  placeholder="Город" required/>
		                <input type="text" ng-model="score3" class="inputCSS2" name="adress"  placeholder="Адрес" required/>
		                <input type="text" ng-model="score4" ng-pattern="/^-?[0-9]\d*(\.\d+)?$/" class="inputCSS2" name="coord_x"  placeholder="Координата X" required/>
		                <input type="text" ng-model="score5" ng-pattern="/^-?[0-9]\d*(\.\d+)?$/" class="inputCSS2" name="coord_y"  placeholder="Координата Y" required/>
		                <div>
		                	<input type="checkbox" ng-model="score6" id="cb1"  required/><label class="att" for="cb1">Аттестация персонала</label>
		                	<label class="w110" for="tb1"></label>
		                	<input id="tb1" type="text" ng-model="score7" value="" placeholder="СВР - 10АЦ" class="inputCSS2 w110 pl10"  disabled="{{scope.isDisabled1}}" ng-disabled="!(!!score6)" required/>
		                </div>
		                <div>
		                	<input type="checkbox" ng-model="score8" id="cb2" required/><label class="att" for="cb2">Аттестация оборудования</label>
		                	<label class="w110" for="tb2">АЦСО - </label>
		                	<input id="tb2" type="text" ng-model="score9" value="" placeholder="102" class="inputCSS2 w110 pl10" disabled="{{scope.isDisabled2}}" ng-disabled="!(!!score8)" required/> 

		                </div>
		                <div>
		                	<input type="checkbox" ng-model="score10" id="cb3" required/><label class="att" for="cb3">Аттестация технологий</label>
		                	<label class="w110" for="tb3">АЦСT - </label>
		                	<input id="tb3" type="text" ng-model="score11" value="" placeholder="118" class="inputCSS2 w110 pl10" disabled="{{scope.isDisabled3}}" ng-disabled="!(!!score10)" required/> 
		                </div>
		            </div>
		            <div class="modal-footer">
		                <button type="submit" id="add_acenter" name="add_acenter" class="btn btn-default" ng-click="addCentersAjax();">Добавить</button>
		                <button class="btn btn-warning" ng-click="cancel()">Отменить</button>
		            </div>
		        </div>
	        </script>
	        <script type="text/ng-template" id="ModalAccept.html">
	      		<div class="text-center">
		            <div class="modal-header">
		                <h3 class="modal-title">Подтвердите удаление</h3>
		            </div>
		            <div class="modal-footer text-center">
		                <button type="submit" id="remove_center" name="add_acenter" class="btn btn-default" ng-click="deleteFunction();">Удалить</button>
		                <button class="btn btn-warning" ng-click="cancel()">Отменить</button>
		            </div>
		        </div>
	        </script>
            <? if(Yii::app()->user->checkAccess(User::ROLE_ADMIN)): ?>
	        <button class="btn btn-default" ng-click="open('')"><p><i class="fa fa-pencil-square-o"></i> Добавить новое юридическое лицо</p></button>
            <? endif; ?>
	    </div>
	</div>
    <div class="history"> 
   		<h3>Перечень юридических лиц, зарегистрированных в системе</h3>
    </div>
    <div>
        <div id="resultListAccess-1" class="dib" ng-init="getCount(); fetch()">
            <div id="fullSearch" class="dib">

                <h4>Поиск:</h4>
                <div class="pull-left">
                    <p>
                        <label for="input1item">Город&nbsp;</label><input id="input1item" ng-change="inputChange()" class="inputCSS3" ng-model="input1item" type="text"/>
                    </p>
                    <p>
                        <label for="input2item">Название организации&nbsp;</label><input id="input2item" ng-change="inputChange()" class="inputCSS3" ng-model="input2item" type="text"/>
                    </p>
                    <p>
                        <label for="input3item">Код&nbsp;</label><input id="input3item" ng-change="inputChange()" class="inputCSS3" ng-model="input3item" type="text"/>
                    </p>
                </div>

                <? if(Yii::app()->user->checkAccess(User::ROLE_ADMIN)): ?>
                <div class="pull-right">
                    <button ng-disabled="!isDisabled" ng-click="toDeleteURL()" class="btn btn-default">
                        Удалить
                    </button>
                </div>
                <? endif; ?>
            </div>
	        <table class="attestation table table-hover">
	            <tr>
	                <th class="tableNumber">Номер</th>
	                <th>Город</th>
	                <th>Название</th>
	                <th>Статус</th>
                    <? if(Yii::app()->user->checkAccess(User::ROLE_ADMIN)): ?> 
                    <th>Удаление</th>
                    <th>Редактирование</th>
                    <? endif; ?>
	            </tr>
	            <tr ng-repeat="attestation in attestations">
	                <td class="tableNumber">{{attestation.id}}</td>
	                <td>{{attestation.city}}</td>
	                <td><a href="<?=Yii::app()->createUrl("certification/center/")?>/{{attestation.id}}"> {{attestation.name}}</a></td>
	                <td class="toggle-{{attestation.isOnline}}"><i class="fa fa-toggle-{{attestation.isOnline}}"></i> {{attestation.isOnline}}</td>
                    <? if(Yii::app()->user->checkAccess(User::ROLE_ADMIN)): ?> 
                    <td class="tableNumber"><input type="checkbox" ng-change="toDelete()" ng-model="selection.ids[attestation.id]" /></td>
                    <td class="tableNumber"><i class="fa fa-pencil" ng-click="toUpdate('',attestation)"></i></td>
                    <? endif; ?>	                
	            </tr>
	        </table>

            <? if(Yii::app()->user->checkAccess(User::ROLE_ADMIN)): ?>
            <div class="pull-right">
                <button ng-disabled="!isDisabled" ng-click="toDeleteURL()" class="btn btn-default">
                    Удалить
                </button>
            </div>
            <? endif; ?>
	        <div class="spinner text-center main_col" ng-show="loading">
			  <div class="spinner-container container1">
			    <div class="circle1"></div>
			    <div class="circle2"></div>
			    <div class="circle3"></div>
			    <div class="circle4"></div>
			  </div>
			  <div class="spinner-container container2">
			    <div class="circle1"></div>
			    <div class="circle2"></div>
			    <div class="circle3"></div>
			    <div class="circle4"></div>
			  </div>
			  <div class="spinner-container container3">
			    <div class="circle1"></div>
			    <div class="circle2"></div>
			    <div class="circle3"></div>
			    <div class="circle4"></div>
			  </div>
	        </div>
	    </div>
		<div class="text-center">
			<pagination ng-model="bigCurrentPage" total-items="bigTotalItems"  max-size="maxSize" class="pagination-sm" boundary-links="true" num-pages="numPages" ng-change="pageChanged()"></pagination>
		</div>
	</div>
</div>
