<?php ?> 

<? $this->setPageTitle('Юридическое лицо');?>
<div class="navbar navbar-default navbar-fixed-top">
  <div class="container"> 
    <div class="navbar-collapse collapse" id="navbar-main">
      <ul class="nav navbar-nav">
        <? if(Yii::app()->user->checkAccess(User::ROLE_COMMISSION_MEMBER)): ?>
        <li class="">
          <a href="/certification/" id="themes"><i class="fa fa-user"></i> Юридические лица</a>
        </li>
        <?endif;?>

        <? if(Yii::app()->user->checkAccess(User::ROLE_MODER)): ?>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes2"><i class="fa fa-cog"></i> Функционал центра <span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="themes2">
            <li><a href="/certification/center/<?=$center['id'] ?>"><i class="fa fa-home"></i> Юридическое лицо</a></li>
            <li><a href="/certification/users/<?=$center['id'] ?>"><i class="fa fa-user"></i> Члены аттестационной комиссии</a></li>
            <li><a href="/certification/devices/<?=$center['id'] ?>"><i class="fa fa-cog"></i> Устройства</a></li>
            <li><a href="/certification/organizations/<?=$center['id'] ?>"><i class="fa fa-bank"></i> Организации</a></li>
          </ul>
        </li>  
        <?endif;?>
        
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes3"><i class="fa fa-file-text"></i> Результаты регистрации <span class="caret"></span></a>
            <ul class="dropdown-menu" aria-labelledby="themes3">
                <? if (Yii::app()->user->userAccess['welders']):?>
                <li><a href="/data/welders/<?=$center['id'] ?>"><i class="fa fa-user"></i> Аттестация персонала</a></li>
                <? endif;?>
                <? if (Yii::app()->user->userAccess['tech']):?>
                <li><a href="/data/tech/<?=$center['id'] ?>"><i class="fa fa-cog"></i> Аттестация сварочных технологий</a></li>
                <? endif;?>
                <? if (Yii::app()->user->userAccess['equip']):?>
                <li><a href="/data/equip/<?=$center['id'] ?>"><i class="fa fa-mobile"></i> Аттестация сварочного оборудования</a></li>
                <? endif;?>
            </ul> 
        </li>  
      </ul>  
       <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes3"><i class="fa fa-bars"></i> Меню <span class="caret"></span></a>
            <ul class="dropdown-menu" aria-labelledby="themes3">
                <li>
                    <a href="<?=Yii::app()->createUrl("main/logout")?>"><i class="fa fa-sign-out"></i> <?=Yii::app()->topMenu->getUsername()?> (выход)</a>
                </li> 
                <li>
                    <a href="/forum"><i class="fa fa-comments-o"></i> Перейти на форум</a>
                </li>  
            </ul> 
        </li>  
      </ul> 
    </div>
  </div>
</div>

<div id="wrapper"> 

    <?if(!Yii::app()->user->getIsGuest()):?>
        <div id="chatWithAdmin"  ng-controller="chat" ng-class="{'button': !isActive, 'button active': isActive}">
            <div class="chatLeftSide">
                <div class="scrollerChat" ng-init="getMessage(<?=$userId?>)">
                    <table class="table table-striped ">
                        <tr ng-repeat="message in messages | orderBy:predicate:!reverse">
                            <td>
                                <div class="topData">
                                    <div class="chatName"><b>{{message.name}}</b></div>
                                    <div class="chatData">{{message.date}}</div>
                                </div>
                                <div class="message">{{message.message}}</div>
                            </td>
                        </tr>
                    </table>
                </div>
                <textarea ng-model="ta_model" name="" id="" class="wide" rows="5"></textarea>
                <button ng-click="sendMessage(<?=$userId?>)" class="btn btn-default pull-right">Отправить</button>
            </div>
            <div class="chatRightSide"
                 ng-init="isActive = false"
                 ng-click="isActive = !isActive">
                <div class="bl"></div>
                <div class="vertical-text">Связь с администрацией</div>
            </div>
        </div>
    <?endif;?>


    <div class="history"><br>

        <h3>Юридическое лицо</h3>

	    <p>Название юридического лица: <b><?=$center['name']?></b></p>
        <p>Адрес: <b>г. <?=$center['city']?>, <?=$center['adress']?></b></p>
	    <p>Телефон: <b><?=$center['phone']?></b></p>
	 
        <h3>Перечень аттестационных центров, входящих в состав юридического лица</h3>
        <p><b><?=$center['weldersCode']?></b></p>
        <? if($center['equipCode']):?>
        <p><b>АЦСО-<?=$center['equipCode']?></b></p>
        <? endif; ?>
        <? if($center['equipCode']):?>
        <p><b>АЦСТ-<?=$center['techCode']?></b></p>
        <? endif; ?>
        <h3>История посещений</h3>


    </div>

   <div ng-controller="historyAjax">
      <? if(Yii::app()->user->checkAccess(User::ROLE_ADMIN)): ?>
       <div id="resultListAccess-10" ng-init="getCount(<?= $center['id'] ?>); fetch()">
           <table class="attestation table table-hover">
               <tbody>
                   <tr>
                       <th class="tableNumber">ID</th>
                       <th>ФИО</th>
                       <th>Статус входа</th>
                       <th>Время входа в систему</th>
                       <th>Время входа из системы</th>
                   </tr>
                    <tr ng-if="emptyArr==true">
                        <td colspan="20" >
                            Ничего не найдено
                        </td>
                    </tr>
                   <tr ng-repeat="stor in stors">
                       <td class="tableNumber">{{stor.id}}</td>
                       <td>{{stor.name}}</td>
                       <td ng-if="stor.device=='Компьютера'">{{stor.device}}</td>
                       <td ng-if="stor.device=='СПО'">{{stor.deviceAdress}}</td>
                       <td>{{stor.dateTime}}</td>
                       <td ng-if="stor.device=='СПО'">{{stor.logoutTime}}</td>
                   </tr>

               </tbody>
           </table>

           <div class="spinner text-center main_col" ng-show="loading">
               <div class="spinner-container container1">
                   <div class="circle1"></div>
                   <div class="circle2"></div>
                   <div class="circle3"></div>
                   <div class="circle4"></div>
               </div>
               <div class="spinner-container container2">
                   <div class="circle1"></div>
                   <div class="circle2"></div>
                   <div class="circle3"></div>
                   <div class="circle4"></div>
               </div>
               <div class="spinner-container container3">
                   <div class="circle1"></div>
                   <div class="circle2"></div>
                   <div class="circle3"></div>
                   <div class="circle4"></div>
               </div>
           </div>
       </div>
		<div class="text-center">
			<pagination ng-model="bigCurrentPage" total-items="bigTotalItems" max-size="maxSize" class="pagination-sm" boundary-links="true" num-pages="numPages" ng-change="pageChanged()"></pagination>
		</div>
        <? endif; ?>
    </div>
</div>


<script type="text/ng-template" id="ModalAccept.html">
    <div class="text-center">
        <div class="modal-header">
            <h3 class="modal-title">Подтвердите удаление</h3>
        </div>
        <div class="modal-footer text-center">
            <button type="submit" id="remove_center" name="add_acenter" class="btn btn-default" ng-click="deleteFunction();">Удалить</button>
            <button class="btn btn-warning" ng-click="cancel()">Отменить</button>
        </div>
    </div>
</script>