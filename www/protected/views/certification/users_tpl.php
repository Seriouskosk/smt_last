<?php ?>

<? $this->setPageTitle('Члены аттестационной комиссии');?>
<div class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-collapse collapse" id="navbar-main">
      <ul class="nav navbar-nav">
        <? if(Yii::app()->user->checkAccess(User::ROLE_COMMISSION_MEMBER)): ?>
        <li class="">
          <a href="/certification/" id="themes"><i class="fa fa-user"></i> Юридические лица</a>
        </li>
        <?endif;?>

        <? if(Yii::app()->user->checkAccess(User::ROLE_MODER)): ?>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes2"><i class="fa fa-cog"></i> Функционал центра <span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="themes2">
            <li><a href="/certification/center/<?=$centerId ?>"><i class="fa fa-home"></i> Юридическое лицо</a></li>
            <li><a href="/certification/users/<?=$centerId ?>"><i class="fa fa-user"></i> Члены аттестационной комиссии</a></li>
            <li><a href="/certification/devices/<?=$centerId ?>"><i class="fa fa-cog"></i> Устройства</a></li>
            <li><a href="/certification/organizations/<?=$centerId ?>"><i class="fa fa-bank"></i> Организации</a></li>
          </ul>
        </li>
        <?endif;?>

        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes3"><i class="fa fa-file-text"></i> Результаты регистрации <span class="caret"></span></a>
            <ul class="dropdown-menu" aria-labelledby="themes3">
                <? if (Yii::app()->user->userAccess['welders']):?>
                <li><a href="/data/welders/<?=$centerId ?>"><i class="fa fa-user"></i> Аттестация персонала</a></li>
                <? endif;?>
                <? if (Yii::app()->user->userAccess['tech']):?>
                <li><a href="/data/tech/<?=$centerId ?>"><i class="fa fa-cog"></i> Аттестация сварочных технологий</a></li>
                <? endif;?>
                <? if (Yii::app()->user->userAccess['equip']):?>
                <li><a href="/data/equip/<?=$centerId ?>"><i class="fa fa-mobile"></i> Аттестация сварочного оборудования</a></li>
                <? endif;?>
            </ul>
        </li>
      </ul>
       <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes3"><i class="fa fa-bars"></i> Меню <span class="caret"></span></a>
            <ul class="dropdown-menu" aria-labelledby="themes3">
                <li>
                    <a href="<?=Yii::app()->createUrl("main/logout")?>"><i class="fa fa-sign-out"></i> <?=Yii::app()->topMenu->getUsername()?> (выход)</a>
                </li>
                <li>
                    <a href="/forum"><i class="fa fa-comments-o"></i> Перейти на форум</a>
                </li>
            </ul>
        </li>
      </ul>
    </div>
  </div>
</div>

<div id="wrapper" ng-controller="ajaxCtrlUsers">

    <?if(!Yii::app()->user->getIsGuest()):?>
        <div id="chatWithAdmin"  ng-controller="chat" ng-class="{'button': !isActive, 'button active': isActive}">
            <div class="chatLeftSide">
                <div class="scrollerChat" ng-init="getMessage(<?=$userId?>)">
                    <table class="table table-striped ">
                        <tr ng-repeat="message in messages | orderBy:predicate:!reverse">
                            <td>
                                <div class="topData">
                                    <div class="chatName"><b>{{message.name}}</b></div>
                                    <div class="chatData">{{message.date}}</div>
                                </div>
                                <div class="message">{{message.message}}</div>
                            </td>
                        </tr>
                    </table>
                </div>
                <textarea ng-model="ta_model" name="" id="" class="wide" rows="5"></textarea>
                <button ng-click="sendMessage()" class="btn btn-default pull-right">Отправить</button>
            </div>
            <div class="chatRightSide"
                 ng-init="isActive = false"
                 ng-click="isActive = !isActive">
                <div class="bl"></div>
                <div class="vertical-text">Связь с администрацией</div>
            </div>
        </div>
    <?endif;?>

    <div id="workPanel"> 

        <div class="mt10">
                <? if(Yii::app()->user->checkAccess(User::ROLE_ADMIN)): ?>

            <script type="text/ng-template" id="myModalContent.html"> 
                <div>
                    <div class="modal-header">
                        <h3 class="modal-title">Добавить члена аттестационной комиссии</h3>
                    </div>
                    <div class="modal-body">
                        <input ng-model="score1" ng-required="true" class="inputCSS2" type="text" name="name"  placeholder="Фамилия, Имя, Отчество"/>
                        <input ng-model="score4" ng-required="true" class="inputCSS2" type="number" min="1" max="4" name="level"  placeholder="Уровень (1-4)"/>
                        <input ng-model="score2" ng-required="true" class="inputCSS2" type="text" name="login"  placeholder="Логин"/>
                        <input ng-model="score3" ng-required="true" class="inputCSS2" type="text" name="password"  placeholder="Пароль"/>
                        <? if($certTypes['welders']): ?>
                        <div>
                            <input type="checkbox" ng-model="score6" id="cb1"  required/><label class="att" for="cb1">Аттестация персонала</label>
                        </div>
                        <? endif; ?>
                        <? if($certTypes['tech']): ?>
                        <div>
                            <input type="checkbox" ng-model="score8" id="cb2" required/><label class="att" for="cb2">Аттестация технологий</label>
                        </div>
                        <? endif; ?>
                        <? if($certTypes['equip']): ?>
                        <div>
                            <input type="checkbox" ng-model="score10" id="cb3" required/><label class="att" for="cb3">Аттестация оборудования</label>
                        </div>
                        <? endif; ?>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="add_acenter" name="add_user" class="btn btn-default" ng-click="addUserAjax(<?= $centerId?>); ">Добавить</button>
                        <button class="btn btn-warning" ng-click="cancel()">Отменить</button>
                    </div> 
                </div>
            </script>
            <button class="btn btn-default" ng-click="open()"><p><i class="fa fa-pencil-square-o"></i> Добавить члена аттестационной комиссии</p></button>

        <?endif;?>
            <div class="dib pull-right toSL">
                <label for="findInCenters">Поиск по имени члена аттестационной комиссии </label> <input id="findInCenters" type="text" ng-change="inputChange()" ng-model="toFindInResult" class="inputCSS3">
            </div>
        </div>
    </div>

    <div class="history"> 
        <h3>Члены аттестационной комиссии </h3> 
    </div>

    <div>
	    <div id="resultListAccess-2" class="dib" ng-init="initId(<?= $centerId?>);getLogin(<?=Yii::app()->user->checkAccess(User::ROLE_ADMIN) ? '1' : '0' ?>);getCount(<?= $centerId?>); fetch()">

            <? if(Yii::app()->user->checkAccess(User::ROLE_ADMIN)): ?>
            <div class="pull-right">
                <button ng-click="toDeleteURL()" class="btn btn-default">
                    Сменить статус блока
                </button>
            </div>
            <table  class="attestation table table-hover">
                <tr>
                    <th class="tableNumber">Номер</th>
                    <th>ФИО</th>
                    <th>Дата регистрации</th>
                    <th>Статус</th>
                    <? if(Yii::app()->user->checkAccess(User::ROLE_ADMIN)): ?>
                    <th>Уровень</th>
                    <th>Логин</th>
                    <th>Пароль</th>
                    <th>Блокирование</th>
                    <th>Редактирование</th>
                    <? endif; ?>
                </tr>
                <tr ng-repeat="attestation in attestations">
                    <td class="tableNumber">{{attestation.id}}</td>
                    <td>{{attestation.name}}</td>
                    <td>{{attestation.date}}</td>
                    <td class="toggle-{{attestation.isOnline}}"><i class="fa fa-toggle-{{attestation.isOnline}}"></i> {{attestation.isOnlineMessage}}</td>
                    <? if(Yii::app()->user->checkAccess(User::ROLE_ADMIN)): ?>
                    <td class="text-center">{{attestation.level}}</td>
                    <td>{{attestation.login}}</td>
                    <td>{{attestation.password}}</td>
                    <td class="text-center"><input type="checkbox" ng-checked="attestation.isActive" ng-change="toDelete()" ng-model="selection.ids[attestation.id]" /></td>
                    <td class="tableNumber"><i class="fa fa-pencil" ng-click="toUpdate('',attestation)"></i></td>
                    <? endif; ?>
                </tr>
            </table>
            <div class="pull-right">
                <button ng-click="toDeleteURL()" class="btn btn-default">
                    Сменить статус блока
                </button>
            </div>
            <?else:?>

            <table  class="attestation table table-hover">
                <tr>
                    <th class="tableNumber">Номер</th>
                    <th>ФИО</th>
                    <th>Дата регистрации</th>
                    <th>Статус</th>
                    <? if(Yii::app()->user->checkAccess(User::ROLE_ADMIN)): ?>
                    <th>Уровень</th>
                    <th>Логин</th>
                    <th>Пароль</th>
                    <th>Блокирование</th>
                    <th>Редактирование</th>
                    <? endif; ?>
                </tr>
                <tr ng-repeat="attestation in attestations" ng-if="attestation.isActive==0">
                    <td class="tableNumber">{{attestation.id}}</td>
                    <td>{{attestation.name}}</td>
                    <td>{{attestation.date}}</td>
                    <td class="toggle-{{attestation.isOnline}}"><i class="fa fa-toggle-{{attestation.isOnline}}"></i> {{attestation.isOnline}}</td>
                    <? if(Yii::app()->user->checkAccess(User::ROLE_ADMIN)): ?>
                    <td class="text-center">{{attestation.level}}</td>
                    <td>{{attestation.login}}</td>
                    <td>{{attestation.password}}</td>
                    <td class="text-center"><input type="checkbox" ng-checked="attestation.isActive" ng-change="toDelete()" ng-model="selection.ids[attestation.id]" /></td>
                    <td class="tableNumber"><i class="fa fa-pencil" ng-click="toUpdate('',attestation)"></i></td>
                    <? endif; ?>
                </tr>
            </table>
            <?endif;?>

            <div class="spinner text-center main_col" ng-show="loading">
              <div class="spinner-container container1">
                <div class="circle1"></div>
                <div class="circle2"></div>
                <div class="circle3"></div>
                <div class="circle4"></div>
              </div>
              <div class="spinner-container container2">
                <div class="circle1"></div>
                <div class="circle2"></div>
                <div class="circle3"></div>
                <div class="circle4"></div>
              </div>
              <div class="spinner-container container3">
                <div class="circle1"></div>
                <div class="circle2"></div>
                <div class="circle3"></div>
                <div class="circle4"></div>
              </div>
            </div>
	    </div>
        <div class="text-center">
            <pagination ng-model="bigCurrentPage" total-items="bigTotalItems" max-size="maxSize" class="pagination-sm" boundary-links="true" num-pages="numPages" ng-change="pageChanged()"></pagination>
        </div>
    </div>
</div>

<script type="text/ng-template" id="ModalAccept.html">
    <div class="text-center">
        <div class="modal-header">
            <h3 class="modal-title">Подтвердите изменение статуса блокирования</h3>
        </div>
        <div class="modal-footer text-center">
            <button type="submit" id="remove_center" name="add_acenter" class="btn btn-default" ng-click="deleteFunction();">Изменить</button>
            <button class="btn btn-warning" ng-click="cancel()">Отменить</button>
        </div>
    </div>
</script>