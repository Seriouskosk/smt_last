<? $this->setPageTitle('Практические испытания');?>

<div class="navbar navbar-default navbar-fixed-top">
  <div class="container"> 
    <div class="navbar-collapse collapse" id="navbar-main">
      <ul class="nav navbar-nav">
        <? if(Yii::app()->user->checkAccess(User::ROLE_COMMISSION_MEMBER)): ?>
        <li class="">
          <a href="/certification/" id="themes"><i class="fa fa-user"></i> Юридические лица</a>
        </li>
        <?endif;?>



        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes2"><i class="fa fa-cog"></i> Функционал центра <span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="themes2">

            <? if(Yii::app()->user->checkAccess(User::ROLE_MODER)): ?>
            <li><a href="/certification/center/<?=$centerId ?>"><i class="fa fa-home"></i> Юридическое лицо</a></li>
            <li><a href="/certification/users/<?=$centerId ?>"><i class="fa fa-user"></i> Члены аттестационной комиссии</a></li>
            <li><a href="/certification/devices/<?=$centerId ?>"><i class="fa fa-cog"></i> Устройства</a></li>
            <?endif;?>
            <li><a href="/certification/organizations/<?=$centerId ?>"><i class="fa fa-bank"></i> Организации</a></li>
          </ul>
        </li>

        
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes3"><i class="fa fa-file-text"></i> Результаты регистрации <span class="caret"></span></a>
            <ul class="dropdown-menu" aria-labelledby="themes3">
                <? if (Yii::app()->user->userAccess['welders']):?>
                <li><a href="/data/welders/<?=$centerId ?>"><i class="fa fa-user"></i> Аттестация персонала</a></li>
                <? endif;?>
                <? if (Yii::app()->user->userAccess['tech']):?>
                <li><a href="/data/tech/<?=$centerId ?>"><i class="fa fa-cog"></i> Аттестация сварочных технологий</a></li>
                <? endif;?>
                <? if (Yii::app()->user->userAccess['equip']):?>
                <li><a href="/data/equip/<?=$centerId ?>"><i class="fa fa-mobile"></i> Аттестация сварочного оборудования</a></li>
                <? endif;?>
            </ul> 
        </li>  
      </ul>  
       <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes3"><i class="fa fa-bars"></i> Меню <span class="caret"></span></a>
            <ul class="dropdown-menu" aria-labelledby="themes3">
                <li>
                    <a href="<?=Yii::app()->createUrl("main/logout")?>"><i class="fa fa-sign-out"></i> <?=Yii::app()->topMenu->getUsername()?> (выход)</a>
                </li> 
                <li>
                    <a href="/forum"><i class="fa fa-comments-o"></i> Перейти на форум</a>
                </li>  
            </ul> 
        </li>  
      </ul> 
    </div>
  </div>
</div> 

<div id="wrapper"> 
    <?if(!Yii::app()->user->getIsGuest()):?>
    <div id="chatWithAdmin"  ng-controller="chat" ng-class="{'button': !isActive, 'button active': isActive}">
        <div class="chatLeftSide">
            <div class="scrollerChat" ng-init="getMessage(<?=$userId?>)">
                <table class="table table-striped ">  
                    <tr ng-repeat="message in messages | orderBy:predicate:!reverse">
                        <td>
                            <div class="topData">
                                <div class="chatName"><b>{{message.name}}</b></div>
                                <div class="chatData">{{message.date}}</div>
                            </div>
                            <div class="message">{{message.message}}</div>
                        </td>
                    </tr>
                </table>
            </div>
            <textarea ng-model="ta_model" name="" id="" class="wide" rows="5"></textarea>
            <button ng-click="sendMessage(<?=$userId?>)" class="btn btn-default pull-right">Отправить</button>
        </div>
        <div class="chatRightSide" 
       ng-init="isActive = false"
       ng-click="isActive = !isActive">
            <div class="bl"></div>
            <div class="vertical-text">Связь с администрацией</div>
        </div>
    </div>
    <?endif;?>

        <div id="workPanel">
            <div class="mt10">

                <div class="dib" ng-controller="ModalOrganizationsCtrl">
                    <script type="text/ng-template" id="myModalContent.html"> 
                        <div ng-controller="ModalOrganizationsAjaxCtrl">
                            <div class="modal-header">
                                <h3 class="modal-title">Добавить новую организацию</h3>
                            </div>
                            <div class="modal-body">
                                <input ng-model="score1" class="inputCSS2" type="text" name="name"  placeholder="Название"/>
                                <input ng-model="score2" class="inputCSS2" type="text" name="login"  placeholder="Адрес"/>
                                <input ng-model="score3" class="inputCSS2" type="text" name="password"  placeholder="Телефон"/>
                                <input ng-model="score4" class="inputCSS2" type="text" name="level"  placeholder="ИНН"/>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" id="add_acenter" name="add_user" class="btn btn-default" ng-click="addOrganizationAjax(<?= $centerId?>); cancel()">Добавить</button>
                                <button class="btn btn-warning" ng-click="cancel()">Отменить</button>
                            </div> 
                        </div>
                    </script>
                    <button class="btn btn-default" ng-click="open()"><p><i class="fa fa-pencil-square-o"></i> Добавить новую организацию</p></button>
                </div>



            </div> 
        </div>


		<div class="history"> 
	        <h3>Аттестация сварочного оборудования - Практические испытания</h3> 
	    </div>


	<div>
		<div id="mainbg">
            <div  id="resultListAccess-7" class="pull-left">
            <table class="attestation  table table-hover">
            	<tr>
            		<td>Организация</td><td><?=$cData['orgName']?></td>
            	</tr>
            	<tr>
            		<td>Член комиссии</td><td><?=$cData['userName']?></td>
            	</tr>
            	<tr>
            		<td>Шифр аттестационого центра</td><td><?=$cData['centerName']?></td>
            	</tr>
            	<tr>
            		<td>Марка оборудования</td><td><?=$cData['tradeMark']?></td>
            	</tr>
            	<tr>
            		<td>Заводской номер</td><td><?=$cData['factoryNum']?></td>
            	</tr>
            	<tr>
            		<td>Инвертарный номер</td><td><?=$cData['ivnNum']?></td>
            	</tr> 
            	<tr>
            		<td>Шифр способа сварки</td><td><?=$cData['weldMethod']?></td>
            	</tr>
                <tr>
                    <td>Номер программы</td><td><?=$cData['programName']?></td>
                </tr>
                <?if(!empty($cData['photoFirst'])):?>
                <tr>
                    <td>Фото комиссии</td><td><a target="_blank" href="/upload/photos/<?=$cData['photoFirst']?>">ссылка</a></td>
                </tr>
                <?endif;?>
                <?if(!empty($cData['photoSecond'])):?>
                <tr>
                    <td>Фото сборки КСС</td><td><a target="_blank" href="/upload/photos/<?=$cData['photoSecond']?>">ссылка</a></td>
                </tr>
                <?endif;?>
                <?if(!empty($cData['finishPhoto'])):?>
                <tr>
                    <td>Фото готового КСС</td><td><a target="_blank" href="/upload/photos/<?=$cData['finishPhoto']?>">ссылка</a></td>
                </tr>
                <?endif;?>
            </table>
            </div>
		<div class="pull-left" ng-controller="ajaxCtrlPracticalResult"> 
			<div id="resultListAccess-9" ng-init="getcount(<?=$certId ?>);fetch(<?=$certId ?>,2)">
	            <table class="attestation table table-hover">
	                <tr> 
	                    <th>Шифр КСС</th>
	                    <th>Размеры КСС</th>
	                    <th>Тип стыка</th>
	                    <th>ФИО сварщика</th>
	                    <th>Клеймо</th>
	                    <th>Начало регистрации</th>
	                    <th>Заверешенные регистрации</th>
	                </tr>
                    <tr ng-if="emptyArr==true">
                        <td colspan="20" >
                            Ничего не найдено
                        </td>
                    </tr>
	                <tr ng-repeat="data in datas track by $index">
	                    <td><a href="./{{data.id}}">{{data.kccType}}</a></td>
                        <td ng-if="data.jointType=='Труба/Труба'">D{{data.dFirst}}x{{data.hFirst}} + D{{data.dSecond}}x{{data.hSecond}}</td>
                        <td ng-if="data.jointType=='Труба/Лист'">D{{data.dFirst}}x{{data.hFirst}} + S{{data.hSecond}}</td>
                        <td ng-if="data.jointType=='Лист/Лист'">S{{data.hFirst}} + S{{data.hSecond}}</td>
	                    <td>{{data.jointType}}</td>
	                    <td>{{data.workerName}}</td>
	                    <td>{{data.workerStump}}</td>
	                    <td>{{data.startDate}}</td>
	                    <td>{{data.endDate}}</td> 
	                </tr>
	            </table>
			</div> 
		</div>
	</div>
</div>