 <? $this->setPageTitle('Просмотр графика');?>

<div class="navbar navbar-default navbar-fixed-top">
  <div class="container"> 
    <div class="navbar-collapse collapse" id="navbar-main">
      <ul class="nav navbar-nav">
        <? if(Yii::app()->user->checkAccess(User::ROLE_COMMISSION_MEMBER)): ?>
        <li class="">
          <a href="/certification/" id="themes"><i class="fa fa-user"></i> Юридические лица</a>
        </li>
        <?endif;?>



        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes2"><i class="fa fa-cog"></i> Функционал центра <span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="themes2">

            <? if(Yii::app()->user->checkAccess(User::ROLE_MODER)): ?>
            <li><a href="/certification/center/<?=$centerId ?>"><i class="fa fa-home"></i> Юридическое лицо</a></li>
            <li><a href="/certification/users/<?=$centerId ?>"><i class="fa fa-user"></i> Члены аттестационной комиссии</a></li>
            <li><a href="/certification/devices/<?=$centerId ?>"><i class="fa fa-cog"></i> Устройства</a></li>
            <?endif;?>
            <li><a href="/certification/organizations/<?=$centerId ?>"><i class="fa fa-bank"></i> Организации</a></li>
          </ul>
        </li>

        
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes3"><i class="fa fa-file-text"></i> Результаты регистрации <span class="caret"></span></a>
            <ul class="dropdown-menu" aria-labelledby="themes3">
                <? if (Yii::app()->user->userAccess['welders']):?>
                <li><a href="/data/welders/<?=$centerId ?>"><i class="fa fa-user"></i> Аттестация персонала</a></li>
                <? endif;?>
                <? if (Yii::app()->user->userAccess['tech']):?>
                <li><a href="/data/tech/<?=$centerId ?>"><i class="fa fa-cog"></i> Аттестация сварочных технологий</a></li>
                <? endif;?>
                <? if (Yii::app()->user->userAccess['equip']):?>
                <li><a href="/data/equip/<?=$centerId ?>"><i class="fa fa-mobile"></i> Аттестация сварочного оборудования</a></li>
                <? endif;?>
            </ul> 
        </li>  
      </ul>  
       <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes3"><i class="fa fa-bars"></i> Меню <span class="caret"></span></a>
            <ul class="dropdown-menu" aria-labelledby="themes3">
                <li>
                    <a href="<?=Yii::app()->createUrl("main/logout")?>"><i class="fa fa-sign-out"></i> <?=Yii::app()->topMenu->getUsername()?> (выход)</a>
                </li> 
                <li>
                    <a href="/forum"><i class="fa fa-comments-o"></i> Перейти на форум</a>
                </li>  
            </ul> 
        </li>  
      </ul> 
    </div>
  </div>
</div>
  <script src="http://yastatic.net/jquery/2.1.1/jquery.min.js"></script>
  <script src="http://code.highcharts.com/stock/highstock.js"></script> 
  <script src="http://code.highcharts.com/stock/modules/exporting.js"></script>
  <script>
  $.get('/upload/csv/<?=$regFile?>', function(data) {

      var start = +new Date();
      var options = {
          credits: {
              enabled: false
          },

          chart: {
              zoomType: 'x'
          },


          rangeSelector: {
              enabled: true,
              buttons: [
                 {
                  type: 'all',
                  text: '1:1'
              }],
              inputStyle: {
                  display: 'none',
              },
              labelStyle: {
                  display: 'none',
              },
          },

          legend:{
              enabled: true,
              verticalAlign: 'top'
          },

          yAxis: {
              title: {
                  text: 'Единиц',
                  margin: 30,
              },
              labels:{
                align: 'right'
              }
          },
          xAxis: {
              type: 'linear',
          },

          title: {
              text: ''
          },

          subtitle: {
              text: '' // dummy text to reserve space for dynamic subtitle
          },

          exporting: {
              buttons: {
                  contextButton: {
                      enabled: false
                  },
                  exportButton: {
                      text: 'Экспорт картинки',
                      // Use only the download related menu items from the default context button
                      menuItems: Highcharts.getOptions().exporting.buttons.contextButton.menuItems.splice(2)
                  },
                  printButton: {
                      text: 'Распечатать',
                      onclick: function () {
                          this.print();
                      }
                  }
              }
          }, 
          series: []
      }
      // Split the lines
      var lines = data.split('\n');
      console.log(lines);
      var series = [{
                  name: '',
              data: [],
              pointStart: <?= $startDate ?> * 1000,
              pointInterval: 10000,
              tooltip: {
          valueDecimals: 5,
                  valueSuffix: ' Ампер'
      }
  }, {
      name: '',
      data: [],
      pointStart: <?= $startDate ?> * 1000,
          pointInterval: 100000,
          tooltip: {
      valueDecimals: 5,
              valueSuffix: ' Вольт'
  }
  }]
  var categories = {
      categories: []
  };

  // Iterate over the lines and add categories or series
  $.each(lines, function(lineNo, line) {
      var items = line.split(';');

      // header line containes categories

      // the rest of the lines contain data with their name in the first
      // position
      if (lineNo == 0) {
          $.each(items, function(itemNo, item) {
              if (itemNo < 2) {
                  // console.log(itemNo, item);
                  series[itemNo].name = item;
              } else {
                  // console.log(itemNo, item);
              }
          });
      } else {
          $.each(items, function(itemNo, item) {
              if (itemNo != 2) { //items -
                  // console.log(items);
                  // var pushItem =[];
                  // parseFloat(items[0].replace(",",".")),
                  // pushItem.push( );
                  // console.log(pushItem);
                  series[itemNo].data.push(parseFloat(items[itemNo].replace(",", ".")));
              } else if (itemNo == 2 && lineNo == 1) {
                  categories.categories.push(parseFloat(items[itemNo].replace(",", ".")) * 1000);
                  series[0].pointInterval = parseFloat(items[itemNo].replace(",", ".")) ;
                  series[1].pointInterval = parseFloat(items[itemNo].replace(",", ".")) ;
                  console.log(series.pointInterval);
                  console.log(series);
                  //                console.log(parseFloat(items[itemNo].replace(",","."))*1000);
              }

          });
      };
  });


  if (series[0].data.length > series[1].data.length) { //Кастыль, в случае если кол-во точек не одинаковое в CSV, возможен баг на последней итерации
      delete series[0].data[series[0].data.length - 1];
  }
  if (series[0].data.length < series[1].data.length) {
      delete series[1].data[series[1].data.length - 1];
  }

  console.log(series);

  // console.log(series);
  // console.log(categories);
  options.series = series;
  options.categories = categories.categories;
  options.lang = {
    downloadJPEG: "Скачать в JPEG",
    downloadPNG: "Скачать в PNG",
    downloadPDF: "Скачать в PDF",
    downloadSVG: "Скачать в SVG",
    printChart: "Распечатать график",
    }
  // console.log(options.series);
  // console.log(options.categories);
  // Create the chart

  console.log(options);

  $('#container').highcharts('StockChart', options);
  });

  </script> 
  
  <div id="container" style="height: 400px; min-width: 310px">

      <div class="spinner text-center main_col"  >
          <div class="spinner-container container1">
              <div class="circle1"></div>
              <div class="circle2"></div>
              <div class="circle3"></div>
              <div class="circle4"></div>
          </div>
          <div class="spinner-container container2">
              <div class="circle1"></div>
              <div class="circle2"></div>
              <div class="circle3"></div>
              <div class="circle4"></div>
          </div>
          <div class="spinner-container container3">
              <div class="circle1"></div>
              <div class="circle2"></div>
              <div class="circle3"></div>
              <div class="circle4"></div>
          </div>
      </div>
  </div>
