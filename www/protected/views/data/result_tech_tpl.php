<? $this->setPageTitle('Аттестация технологии');?>

<div class="navbar navbar-default navbar-fixed-top">
  <div class="container"> 
    <div class="navbar-collapse collapse" id="navbar-main">
      <ul class="nav navbar-nav">
        <? if(Yii::app()->user->checkAccess(User::ROLE_COMMISSION_MEMBER)): ?>
        <li class="">
          <a href="/certification/" id="themes"><i class="fa fa-user"></i> Юридические лица</a>
        </li>
        <?endif;?>



        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes2"><i class="fa fa-cog"></i> Функционал центра <span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="themes2">

            <? if(Yii::app()->user->checkAccess(User::ROLE_MODER)): ?>
            <li><a href="/certification/center/<?=$centerId ?>"><i class="fa fa-home"></i> Юридическое лицо</a></li>
            <li><a href="/certification/users/<?=$centerId ?>"><i class="fa fa-user"></i> Члены аттестационной комиссии</a></li>
            <li><a href="/certification/devices/<?=$centerId ?>"><i class="fa fa-cog"></i> Устройства</a></li>
            <?endif;?>
            <li><a href="/certification/organizations/<?=$centerId ?>"><i class="fa fa-bank"></i> Организации</a></li>
          </ul>
        </li>

        
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes3"><i class="fa fa-file-text"></i> Результаты регистрации <span class="caret"></span></a>
            <ul class="dropdown-menu" aria-labelledby="themes3">
                <? if (Yii::app()->user->userAccess['welders']):?>
                <li><a href="/data/welders/<?=$centerId ?>"><i class="fa fa-user"></i> Аттестация персонала</a></li>
                <? endif;?>
                <? if (Yii::app()->user->userAccess['tech']):?>
                <li><a href="/data/tech/<?=$centerId ?>"><i class="fa fa-cog"></i> Аттестация сварочных технологий</a></li>
                <? endif;?>
                <? if (Yii::app()->user->userAccess['equip']):?>
                <li><a href="/data/equip/<?=$centerId ?>"><i class="fa fa-mobile"></i> Аттестация сварочного оборудования</a></li>
                <? endif;?>
            </ul> 
        </li>  
      </ul>  
       <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes3"><i class="fa fa-bars"></i> Меню <span class="caret"></span></a>
            <ul class="dropdown-menu" aria-labelledby="themes3">
                <li>
                    <a href="<?=Yii::app()->createUrl("main/logout")?>"><i class="fa fa-sign-out"></i> <?=Yii::app()->topMenu->getUsername()?> (выход)</a>
                </li> 
                <li>
                    <a href="/forum"><i class="fa fa-comments-o"></i> Перейти на форум</a>
                </li>  
            </ul> 
        </li>  
      </ul> 
    </div>
  </div>
</div> 

<div id="wrapper">

    <?if(!Yii::app()->user->getIsGuest()):?>
        <div id="chatWithAdmin"  ng-controller="chat" ng-class="{'button': !isActive, 'button active': isActive}">
            <div class="chatLeftSide">
                <div class="scrollerChat" ng-init="getMessage(<?=$userId?>)">
                    <table class="table table-striped ">
                        <tr ng-repeat="message in messages | orderBy:predicate:!reverse">
                            <td>
                                <div class="topData">
                                    <div class="chatName"><b>{{message.name}}</b></div>
                                    <div class="chatData">{{message.date}}</div>
                                </div>
                                <div class="message">{{message.message}}</div>
                            </td>
                        </tr>
                    </table>
                </div>
                <textarea ng-model="ta_model" name="" id="" class="wide" rows="5"></textarea>
                <button ng-click="sendMessage(<?=$userId?>)" class="btn btn-default pull-right">Отправить</button>
            </div>
            <div class="chatRightSide"
                 ng-init="isActive = false"
                 ng-click="isActive = !isActive">
                <div class="bl"></div>
                <div class="vertical-text">Связь с администрацией</div>
            </div>
        </div>
    <?endif;?>

        <div id="workPanel">

                <div class="dib" ng-controller="ModalOrganizationsCtrl">
                    <script type="text/ng-template" id="myModalContent.html"> 
                        <div ng-controller="ModalOrganizationsAjaxCtrl">
                            <div class="modal-header">
                                <h3 class="modal-title">Добавить новую организацию</h3>
                            </div>
                            <div class="modal-body">
                                <input ng-model="score1" class="inputCSS2" type="text" name="name"  placeholder="Название"/>
                                <input ng-model="score2" class="inputCSS2" type="text" name="login"  placeholder="Адрес"/>
                                <input ng-model="score3" class="inputCSS2" type="text" name="password"  placeholder="Телефон"/>
                                <input ng-model="score4" class="inputCSS2" type="text" name="level"  placeholder="ИНН"/>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" id="add_acenter" name="add_user" class="btn btn-default" ng-click="addOrganizationAjax(<?= $centerId?>); cancel()">Добавить</button>
                                <button class="btn btn-warning" ng-click="cancel()">Отменить</button>
                            </div> 
                        </div>
                    </script>
                    <button class="btn btn-default" ng-click="open()"><p><i class="fa fa-pencil-square-o"></i> Добавить новую организацию</p></button>
                </div>

			<a href="/data/pdftech?certId=<?= $certId ?>" type="submit" class="btn btn-default"><p><i class="fa fa-download"></i> Скачать отчёт</p></a>

        </div>

 
		<div class="history"> 
	        <h3>Аттестация технологии</h3> 
	    </div> 

	<div class="dib">
		<div class="pull-left" id="resultListAccess-6">
            <table class="attestation  table table-hover">
            	<tr>
            		<td>Организация</td><td><?=$cData['orgName']?></td>
            	</tr>
            	<tr>
            		<td>Член комиссии</td><td><?=$cData['userName']?></td>
            	</tr>
            	<tr>
            		<td>Шифр аттестационого центра</td><td><?=$cData['centerName']?></td>
            	</tr>
            	<tr>
            		<td>Номер программы</td><td><?=$cData['programName']?></td>
            	</tr>
            	<tr>
            		<td>Шифр КСС</td><td><?=$cData['kccType']?></td>
            	</tr>
            	<tr>
            		<td>Вид соединения</td><td><?=$cData['jointType']?></td>
            	</tr>
            	<tr>
            		<td>Размеры КСС</td><td><?=$cData['kccSize']?></td>
            	</tr>
            	<tr>
            		<td>ФИО сварщика</td><td><?=$cData['workerName']?></td>
            	</tr>
            	<tr>
            		<td>Клемо сварщика</td><td><?=$cData['workerStump']?></td>
            	</tr> 
            	<tr>
            		<td>Аттестация начата:</td><td><?=$cData['startDate']?></td>
            	</tr>
            	<tr>
            		<td>Аттестация закончена:</td><td><?=$cData['endDate']?></td>
            	</tr>
            	<tr>
            		<td>Общее время аттестации:</td><td><?=$cData['timeDiff']?></td>
            	</tr>
            	<tr>
            		<td>Время горения дуги:</td><td><?=$cData['weldTime']?></td>
            	</tr>
            	<tr>
            		<td>Время вспомогательных операций:</td><td><?=$cData['additionalTime']?></td>
            	</tr>
            	<tr>
            		<td>Нормативное кол-во слоев:</td><td><?=$cData['layerNorm']?></td>
            	</tr>
            	<tr>
            		<td>Фактичекое кол-во слоев:</td><td><?=$cData['layerCount']?></td>
            	</tr>

                <?if(!empty($cData['photoFirst'])):?>
                <tr>
                    <td>Фото комиссии</td><td><a target="_blank" href="/upload/photos/<?=$cData['photoFirst']?>">ссылка</a></td>
                </tr>
                <?endif;?>
                <?if(!empty($cData['photoSecond'])):?>
                <tr>
                    <td>Фото сборки КСС</td><td><a target="_blank" href="/upload/photos/<?=$cData['photoSecond']?>">ссылка</a></td>
                </tr>
                <?endif;?>
                <?if(!empty($cData['finishPhoto'])):?>
                <tr>
                    <td>Фото готового КСС</td><td><a target="_blank" href="/upload/photos/<?=$cData['finishPhoto']?>">ссылка</a></td>
                </tr>
                <?endif;?>
            </table>
            <div class="rightColInStal" style="width: 460px; height: 500px; float: left;">
            		<h3>Место проведения аттестации</h3> 
            		Долгота: <?=$cData['lon']?> | Широта: <?=$cData['lat']?>
					<div id="map" style="width: 100%; height: 360px;"></div>
            		<script>
						var myMap; 

						// Дождёмся загрузки API и готовности DOM.
						ymaps.ready(init); 

						function init () {
						    var myMap = new ymaps.Map("map", {
						            center: [<?=$cData['lat']?>, <?=$cData['lon']?>],
						            zoom: 11,
							        controls: ['smallMapDefaultSet']
						        }),

						        // Создаем метку с помощью вспомогательного класса.
						        myPlacemark1 = new ymaps.Placemark([<?=$cData['lat']?>, <?=$cData['lon']?>], {
						            // Свойства.
						            // Содержимое иконки, балуна и хинта.
						            iconContent: '1',
						        }, {
						            // Опции.
						            // Стандартная фиолетовая иконка.
						            preset: 'twirl#violetIcon'
						        });
					    // Добавляем все метки на карту.
				    	myMap.geoObjects
				        	.add(myPlacemark1);
					    }

            		</script> 
			</div> 
		<br>
		<div ng-controller="ajaxCtrlWelderResult" class="pull-left" > 
			<div ng-init="fetch(<?= $certId ?>,1)"	>
	            <table class="attestation-2 table table-hover">
					<tbody>
						<tr>
							<th>Начало регистрации</th>
							<th>Завершение регистрации</th>
							<th>Время сварки</th>
							<th>Положение</th>
							<th>Uхх</th>
							<th>Uср</th>
							<th>Uоткл</th>
							<th>Iср</th>
							<th>Iоткл</th>
                            <th>Фото</th>
							<th>Фаил</th>
						</tr>
					</tbody>
					<tbody ng-repeat="cycles in datas">
						<tr>
							<th colspan="15" style="text-align: center" ng-if="$index==0">Прихватка</th>
							<th colspan="15" style="text-align: center" ng-if="$index!=0">Слой № {{$index}}</th>
						</tr>
						<tr ng-repeat="data in cycles" ng-if="data.processed==1">
                            <td>{{data.startDate}}</td>
                            <td>{{data.endDate}}</td>
							<td>{{data.workTime}}</td>
							<td>{{data.position}}</td>
							<td>{{data.maxU}}</td>
							<td>{{data.avrU}}</td>
							<td>{{data.stdU}}</td>
							<td>{{data.avrI}}</td>
							<td>{{data.stdI}}</td>
                            <td ng-if="data.photo!=''"><a target="_blank" href="/upload/photos/{{data.photo}}">Фото</a></td>
                            <td ng-if="data.photo==''"></td>
							<td><a href="./<?= $certId ?>/{{data.id}}">Файл регистограммы</a></td>
						</tr>
                        <tr ng-if="emptyArr==true">
                            <td colspan="20" >
                                Ничего не найдено
                            </td>
                        </tr>
                        <tr ng-repeat="data in cycles" ng-if="data.processed==0">
                            <td colspan="15" class="text-center">Слой находится в обработке</td>
                        </tr>
					</tbody>
	            </table>
			</div> 
		</div>
	</div>
</div>

<script type="text/ng-template" id="ModalAccept.html">
    <div class="text-center">
        <div class="modal-header">
            <h3 class="modal-title">Подтвердите удаление</h3>
        </div>
        <div class="modal-footer text-center">
            <button type="submit" id="remove_center" name="add_acenter" class="btn btn-default" ng-click="deleteFunction();">Удалить</button>
            <button class="btn btn-warning" ng-click="cancel()">Отменить</button>
        </div>
    </div>
</script>