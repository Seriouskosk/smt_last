<? $this->setPageTitle('Аттестация сварочных технологий');?>

<div class="navbar navbar-default navbar-fixed-top">
  <div class="container"> 
    <div class="navbar-collapse collapse" id="navbar-main">
      <ul class="nav navbar-nav">
        <? if(Yii::app()->user->checkAccess(User::ROLE_COMMISSION_MEMBER)): ?>
        <li class="">
          <a href="/certification/" id="themes"><i class="fa fa-user"></i> Юридические лица</a>
        </li>
        <?endif;?>



        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes2"><i class="fa fa-cog"></i> Функционал центра <span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="themes2">

            <? if(Yii::app()->user->checkAccess(User::ROLE_MODER)): ?>
            <li><a href="/certification/center/<?=$centerId ?>"><i class="fa fa-home"></i> Юридическое лицо</a></li>
            <li><a href="/certification/users/<?=$centerId ?>"><i class="fa fa-user"></i> Члены аттестационной комиссии</a></li>
            <li><a href="/certification/devices/<?=$centerId ?>"><i class="fa fa-cog"></i> Устройства</a></li>
            <?endif;?>
            <li><a href="/certification/organizations/<?=$centerId ?>"><i class="fa fa-bank"></i> Организации</a></li>
          </ul>
        </li>


        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes3"><i class="fa fa-file-text"></i> Результаты регистрации <span class="caret"></span></a>
            <ul class="dropdown-menu" aria-labelledby="themes3">
                <? if (Yii::app()->user->userAccess['welders']):?>
                <li><a href="/data/welders/<?=$centerId ?>"><i class="fa fa-user"></i> Аттестация персонала</a></li>
                <? endif;?>
                <? if (Yii::app()->user->userAccess['tech']):?>
                <li><a href="/data/tech/<?=$centerId ?>"><i class="fa fa-cog"></i> Аттестация сварочных технологий</a></li>
                <? endif;?>
                <? if (Yii::app()->user->userAccess['equip']):?>
                <li><a href="/data/equip/<?=$centerId ?>"><i class="fa fa-mobile"></i> Аттестация сварочного оборудования</a></li>
                <? endif;?>
            </ul> 
        </li>  
      </ul>  
       <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes3"><i class="fa fa-bars"></i> Меню <span class="caret"></span></a>
            <ul class="dropdown-menu" aria-labelledby="themes3">
                <li>
                    <a href="<?=Yii::app()->createUrl("main/logout")?>"><i class="fa fa-sign-out"></i> <?=Yii::app()->topMenu->getUsername()?> (выход)</a>
                </li> 
                <li>
                    <a href="/forum"><i class="fa fa-comments-o"></i> Перейти на форум</a>
                </li>  
            </ul> 
        </li>  
      </ul> 
    </div>
  </div>
</div> 


<div id="wrapper">
    <?if(!Yii::app()->user->getIsGuest()):?>
        <div id="chatWithAdmin"  ng-controller="chat" ng-class="{'button': !isActive, 'button active': isActive}">
            <div class="chatLeftSide">
                <div class="scrollerChat" ng-init="getMessage(<?=$userId?>);">
                    <table class="table table-striped ">
                        <tr ng-repeat="message in messages | orderBy:predicate:!reverse">
                            <td>
                                <div class="topData">
                                    <div class="chatName"><b>{{message.name}}</b></div>
                                    <div class="chatData">{{message.date}}</div>
                                </div>
                                <div class="message">{{message.message}}</div>
                            </td>
                        </tr>
                    </table>
                </div>
                <textarea ng-model="ta_model" name="" id="" class="wide" rows="5"></textarea>
                <button ng-click="sendMessage(<?=$userId?>)" class="btn btn-default pull-right">Отправить</button>
            </div>
            <div class="chatRightSide"
                 ng-init="isActive = false"
                 ng-click="isActive = !isActive">
                <div class="bl"></div>
                <div class="vertical-text">Связь с администрацией</div>
            </div>
        </div>
    <?endif;?>
	    <div id="workPanel">

            <div class="dib" ng-controller="ajaxCtrlOrganizations">
                <script type="text/ng-template" id="myModalContent.html">
                    <div class="modal-header">
                        <h3 class="modal-title">Добавить новую организацию</h3>
                    </div>
                    <div class="modal-body">
                        <input ng-model="score1" class="inputCSS2" type="text" name="name" placeholder="Название"/>
                        <input ng-model="score2" class="inputCSS2" type="text" name="login" placeholder="Адрес"/>
                        <input ng-model="score3" class="inputCSS2" type="text" name="password" placeholder="Телефон"/>
                        <input ng-model="score4" class="inputCSS2" type="text" name="level" placeholder="ИНН"/>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="add_acenter" name="add_user" class="btn btn-default"
                                ng-click="addOrganizationAjax(<?= $centerId?>);">Добавить
                        </button>
                        <button class="btn btn-warning" ng-click="cancel()">Отменить</button>
                    </div>
                </script>

        <? if (Yii::app()->user->checkAccess(User::ROLE_ADMIN) ||
              (Yii::app()->user->checkAccess(User::ROLE_MODER) && !Yii::app()->user->checkAccess(User::ROLE_COMMISSION_MEMBER)) ||
              (Yii::app()->user->checkAccess(User::ROLE_USER) && !Yii::app()->user->checkAccess(User::ROLE_MODER))): ?>

        <button  class="btn btn-default" ng-click="open()"><p><i class="fa fa-pencil-square-o"></i> Добавить новую организацию</p></button>

        <? endif; ?>

            </div>
	    </div>

		<div class="history"> 
	        <h3>Аттестация сварочных технологий</h3> 
	    </div>


		<div ng-controller="ajaxTechs">
			<div id="resultListAccess-5" class="dib " ng-init="getCId(<?= $centerId?>);  toDesc()">

				<div id="fullSearch" class="dib" >
					<h4>Сортировка:</h4> 
					<select ng-model="sort" ng-change="sortable()" ng-options="liveLabel.name for liveLabel in liveLabels">
						<option value="">-- Выберите фильтр --</option>
					</select>


                    <label for="cb1">По убыванию </label><input id="cb1" class="cbpr" type="checkbox" ng-model="toDescModel" ng-change="toDesc()" ng-true-value="true" ng-false-value="false" ng-checked='true'>
                    <label for="cb2">Учитывать дату </label><input id="cb2" class="cbpr" type="checkbox" ng-model="toDataModel" ng-change="timeDataToCtrl()" ng-true-value="enable" ng-false-value="disabled">

                    <div class="dropdown dib">
                        <button ng-init="isActive = false" ng-click="isActive = !isActive" class="btn btn-default {{toDataModel}}">
                            Выберите дату
                        </button>

                        <div class="dropdown-menu" role="menu" ng-class="{'button': !isActive, 'button active': isActive}">
                            <button type="button" class="close2" ng-click="isActive = !isActive;">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <div date-range start="dataFrom" end="dataEnd"></div>
                            <button class="btn btn-default pull-right" ng-click="isActive = !isActive; timeDataToCtrl()"  >Подтверидть дату</button>
                        </div>
                    </div>

					<h4>Поиск:</h4>
					<div class="pull-left">
						<p>
							<label for="searchInput2item">ФИО члена комисии</label>
							<input class="inputCSS3" ng-model="sI2i" ng-change="sortable()" type="text" id="searchInput2item">
						</p>
						<p>
							<label for="searchInput3item">Название организации</label>
							<input class="inputCSS3" ng-model="sI3i" ng-change="sortable()" type="text" id="searchInput3item">
						</p>
						<p>
							<label for="searchInput4item">Программа №</label>
							<input class="inputCSS3" ng-model="sI4i" ng-change="sortable()" type="text" id="searchInput4item">
						</p>
						<p>
							<label for="searchInput5item">Шифр КСС</label>
							<input class="inputCSS3" ng-model="sI5i" ng-change="sortable()" type="text" id="searchInput5item">
						</p> 
					</div> 

					<div class="pull-left data-filter text-right">
					    <button ng-click="refresh()" class="btn btn-default"><i class="fa fa-refresh"></i> Сбросить</button>
                        <? if(Yii::app()->user->checkAccess(User::ROLE_ADMIN)): ?>
                        <button ng-disabled="!isDisabled" ng-click="toDeleteURL()" class="btn btn-default mt10">
                            Удалить
                        </button>
                        <? endif;?>
					</div>

				</div>

				<h3>Результат</h3>

			<? if(Yii::app()->user->checkAccess(User::ROLE_ADMIN)): ?>
	            <table class="attestation table table-hover">
	                <tr>
	                    <th>ФИО члена комисии</th>
	                    <th>Название организации</th>
	                    <th>Программа №</th>
	                    <th>Шифр КСС</th>
	                    <th>Дата аттестации</th> 
	                    <th class="text-center">Удаление</th>
                    </tr>

                    <tr ng-if="emptyArr==true">
                        <td colspan="20" >
                            Ничего не найдено
                        </td>
                    </tr>
	                <tr ng-repeat="data in datas">
	                    <td><a href="/data/tech/<?=$centerId ?>/{{data.id}}">{{data.userName}}</a></td>
	                    <td>{{data.orgName}}</td>
	                    <td>{{data.programName}}</td>
	                    <td>{{data.kccType}}</td>
	                    <td>{{data.endDate}}</td>
	                    <td class="text-center"><input type="checkbox" ng-change="toDelete()" ng-model="selection.ids[data.id]" /></td>
	                </tr>
	            </table>
                <div class="pull-right">
                    <button ng-disabled="!isDisabled" ng-click="toDeleteURL()" class="btn btn-default">
                        Удалить
                    </button>
                </div>
			<? else: ?>
	            <table class="attestation table table-hover">
                    <tr ng-if="emptyArr==true">
                        <td colspan="20" >
                            Ничего не найдено
                        </td>
                    </tr>
	                <tr>
	                    <th>ФИО члена комисии</th>
	                    <th>Название организации</th>
	                    <th>Программа №</th>
	                    <th>Шифр КСС</th>
	                    <th>Дата аттестации</th>
	                </tr>
	                <tr ng-repeat="data in datas ">
	                    <td><a href="/data/tech/<?=$centerId ?>/{{data.id}}">{{data.userName}}</a></td>
	                    <td>{{data.orgName}}</td>
	                    <td>{{data.programName}}</td>
	                    <td>{{data.kccType}}</td>
	                    <td>{{data.endDate}}</td>
	                </tr>
	            </table>
			<? endif;?>
		        <div class="spinner text-center main_col" ng-show="loading">
				  <div class="spinner-container container1">
				    <div class="circle1"></div>
				    <div class="circle2"></div>
				    <div class="circle3"></div>
				    <div class="circle4"></div>
				  </div>
				  <div class="spinner-container container2">
				    <div class="circle1"></div>
				    <div class="circle2"></div>
				    <div class="circle3"></div>
				    <div class="circle4"></div>
				  </div>
				  <div class="spinner-container container3">
				    <div class="circle1"></div>
				    <div class="circle2"></div>
				    <div class="circle3"></div>
				    <div class="circle4"></div>
				  </div>
		        </div>
			</div>
			<div class="text-center">
				<pagination ng-model="bigCurrentPage" total-items="bigTotalItems" max-size="maxSize" class="pagination-sm"
				 boundary-links="true" num-pages="numPages" ng-change="pageChanged()"></pagination>
			</div>
		</div> 
    </div>

    <script type="text/ng-template" id="ModalAccept.html">
        <div class="text-center">
            <div class="modal-header">
                <h3 class="modal-title">Подтвердите удаление</h3>
            </div>
            <div class="modal-footer text-center">
                <button type="submit" id="remove_center" name="add_acenter" class="btn btn-default" ng-click="deleteFunction();">Удалить</button>
                <button class="btn btn-warning" ng-click="cancel()">Отменить</button>
            </div>
        </div>
    </script>