<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.5.0/pure-min.css">
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
</head>
<body>
    <?php echo $content; ?>
</body>
</html>