<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"> 	
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic&subset=cyrillic,latin' rel='stylesheet' type='text/css'>
    <link href="<?=Yii::app()->homeUrl?>css/bootstrap.css" rel="stylesheet">
    <link href="<?=Yii::app()->homeUrl?>css/bootstrap-theme.css" rel="stylesheet">
    <link href="<?=Yii::app()->homeUrl?>css/font-awesome.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src='https://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js'></script> 
    <script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>

    <script src="http://cdnjs.cloudflare.com/ajax/libs/angular-i18n/1.2.15/angular-locale_ru-ru.js"></script>
    <script src='https://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular-sanitize.min.js'></script>     
    <script src="<?=Yii::app()->homeUrl?>js/ui-bootstrap-tpls-0.11.2.js"></script>
    <script src="http://vitalets.github.io/checklist-model/checklist-model.js"></script>
    <script src='<?=Yii::app()->homeUrl?>js/textAngular.min.js'></script>
    <script src="<?=Yii::app()->homeUrl?>js/angular-charts.min.js"></script> 
    <script src="http://api-maps.yandex.ru/2.1/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
    <script src="<?=Yii::app()->homeUrl?>js/controller.js"></script>
    <script src="<?=Yii::app()->homeUrl?>js/registrate/ajaxEquips.js"></script>
    <script src="<?=Yii::app()->homeUrl?>js/registrate/ajaxOrganizations.js"></script>
    <script src="<?=Yii::app()->homeUrl?>js/registrate/ajaxTechs.js"></script>
    <script src="<?=Yii::app()->homeUrl?>js/registrate/ajaxWelders.js"></script>
    <script src="<?=Yii::app()->homeUrl?>js/registrate/ajaxUsers.js"></script>
    <script src="<?=Yii::app()->homeUrl?>js/datePicker.js"></script>
    <script src="<?=Yii::app()->homeUrl?>js/datePickerUtils.js"></script>
    <script src="<?=Yii::app()->homeUrl?>js/dateRange.js"></script>
    <script src="<?=Yii::app()->homeUrl?>js/input.js"></script> 
    <script src="<?=Yii::app()->homeUrl?>js/line-chart.min.js"></script> 
    

</head>
<body ng-app="ui" ng-controller="globalVar">

    <div class="alertBlock" ng-controller="AlertsCtrl">
      <alert ng-repeat="alert in alerts" type="{{alert.type}}" close="closeAlert($index)">{{alert.msg}}</alert>
    </div>

    <?php echo $content; ?>
</body>
</html>