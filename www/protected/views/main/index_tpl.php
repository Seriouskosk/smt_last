<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Аторизация';
?>
 

<?if(isset($loginError)):?>
    <div ng-controller="AlertDemoCtrl">
      <alert class="alertBlock" ng-repeat="alert in alerts" type="{{alert.type}}" close="closeAlert($index)">{{alert.msg}}</alert>
    </div>
<?endif;?>


<div id="enterSite" class=" ">
    <div class="dib">
        <div class="enterColumn">
            <form id="enterPanel" action="<?=Yii::app()->createUrl("main/login")?>" method="POST">
                <h2 class="enterTitle">Вход в аттестационный центр</h2>

                <div class="icon-1">
                    <i class="fa fa-user"></i>
                </div>
                <div class="icon-2">
                    <i class="fa fa-lock"></i>
                </div>
		        <input id="login" type="text" name="LoginForm[login]" placeholder="Имя пользователя"/>
		        <input id="password" type="password" name="LoginForm[password]" placeholder="Пароль"/>

                <div class="bottomEnterPanel">
                    <div class="actionWithForm">
                        <a href="/forum"><i class="fa fa-comments-o"></i><p>Перейти на форум</p></a>
                    </div>
                    <div class="enterInSite">
                        <button type="submit" name="auth" class="btn btn-default"><p><i class="fa fa-sign-in"></i> Войти</p></button>
                    </div>
                </div>
            </form>

        </div>
        <div class="newsColumn" ng-init="updateDate(); fetch()" ng-controller="PaginationNewsCtrl">
            <h2 class="news">Новости</h2>
                <hr>
                <div ng-repeat="data in datas">
                    <h1 class="news-title">{{ data.title }}</h1>
                    <article ng-bind-html="data.text">
                    </article>
                    <div class="news-bottom">
                        <h5 class="news-time"><i class="fa fa-calendar-o"></i> {{ data.date }}</h5>
                    </div>
                    <hr ng-if="!$last"/>
                </div>
            
                <div class="spinner text-center main_col" ng-show="loading">
                  <div class="spinner-container container1">
                    <div class="circle1"></div>
                    <div class="circle2"></div>
                    <div class="circle3"></div>
                    <div class="circle4"></div>
                  </div>
                  <div class="spinner-container container2">
                    <div class="circle1"></div>
                    <div class="circle2"></div>
                    <div class="circle3"></div>
                    <div class="circle4"></div>
                  </div>
                  <div class="spinner-container container3">
                    <div class="circle1"></div>
                    <div class="circle2"></div>
                    <div class="circle3"></div>
                    <div class="circle4"></div>
                  </div>
                </div>

            <a ng-click="viewAll()" class="pull-right" href="#allNews">Посмотреть все записи</a>

            <div class="paginationCtrl">
                <pagination items-per-page="4" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" boundary-links="true" num-pages="numPages" ng-change="pageChanged()" ></pagination>
            </div>
        </div>
     </div>
</div>


