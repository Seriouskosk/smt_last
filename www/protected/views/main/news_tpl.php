<?php ?>
<? $this->setPageTitle('Новости');?>
<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-collapse collapse" id="navbar-main">
            <ul class="nav navbar-nav">
                <li class="">
                    <a href="/certification/all" id="themes"><i class="fa fa-user"></i> Юридические лица</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="/messages"><i class="fa fa-weixin"></i> Обратная связь</a>
                </li>
                <li class="dropdown" ng-controller="ModalNews">
                    <script type="text/ng-template" id="myModalContent2.html">
                        <div ng-controller="addNewsCtrl" class="container app">
                            <h3>Добавить новость</h3>
                            <input ng-model="titlenews" type="text" class="form-control">
                            <div text-angular="text-angular" name="htmlcontent" ng-model="htmlcontent" ta-disabled='disabled'></div>
                            <h4>Добавить картинку</h4>
                            <p class="help-block">Ждите пока появится в редакторе</p>
                            <input type='file' id='uplodaingImage' class=''/>
                            <br>
                            <button class="btn btn-default" ng-click="addNewsAjax();cancel()"><p><i class="fa fa-book"></i> Добавить новость</p></button>
                            <button class="btn btn-default" ng-click="cancel()"><p><i class="fa fa-book"></i> Отменить</p></button>
                            <br><br>
                        </div>
                    </script>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes3"><i class="fa fa-book"></i> Новости <span class="caret"></span></a>
                    <ul class="dropdown-menu" aria-labelledby="themes3">
                        <li>
                            <a href="#" ng-click="open('lg');"><i class="fa fa-book"></i> Добавить новость</a>
                        </li>
                        <li>
                            <a href="<?=Yii::app()->createUrl("main/news")?>"><i class="fa fa-pencil"></i> Управление новостями</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes3"><i class="fa fa-bars"></i> Меню <span class="caret"></span></a>
                    <ul class="dropdown-menu" aria-labelledby="themes3">
                        <li>
                            <a href="<?=Yii::app()->createUrl("main/logout")?>"><i class="fa fa-sign-out"></i> <?=Yii::app()->topMenu->getUsername()?> (выход)</a>
                        </li>
                        <li>
                            <a href="/forum"><i class="fa fa-comments-o"></i> Перейти на форум</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>


<div id="wrapper" ng-controller="ajaxCtrlNews">

    <div id="workPanel">

        <div class="">
            <script type="text/ng-template" id="ModalAccept.html">
                <div class="text-center">
                    <div class="modal-header">
                        <h3 class="modal-title">Подтвердите удаление</h3>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="submit" id="remove_center" name="add_acenter" class="btn btn-default" ng-click="deleteFunction();">Удалить</button>
                        <button class="btn btn-warning" ng-click="cancel()">Отменить</button>
                    </div>
                </div>
            </script>
            <script type="text/ng-template" id="redactorModal.html">
                <div class="container app">
                    <h3>Редактирование новости</h3>
                    <input ng-model="titlenews" type="text" class="form-control">
                    <div text-angular="text-angular" name="htmlcontent" ng-model="htmlcontent" ta-disabled='disabled'></div>
                    <h4>Добавить картинку</h4>
                    <p class="help-block">Ждите пока появится в редакторе</p>
                    <input type='file' id='uplodaingImage' class=''/>
                    <br>
                    <button class="btn btn-default" ng-click="updateAjaxNews();cancel()"><p><i class="fa fa-book"></i> Отредактировать</p></button>
                    <button class="btn btn-default" ng-click="cancel()"><p><i class="fa fa-book"></i> Отменить</p></button>
                    <br><br>
                </div>
            </script>
            <script type="text/ng-template" id="addNewsModal.html">
                <div ng-controller="addNewsCtrl" class="container app">
                    <h3>Добавить новость</h3>
                    <input ng-model="titlenews" type="text" class="form-control">
                    <div text-angular="text-angular" name="htmlcontent" ng-model="htmlcontent" ta-disabled='disabled'></div>
                    <h4>Добавить картинку</h4>
                    <p class="help-block">Ждите пока появится в редакторе</p>
                    <input type='file' id='uplodaingImage' class=''/>
                    <br>
                    <button class="btn btn-default" ng-click="addNewsAjax();cancel()"><p><i class="fa fa-book"></i> Добавить новость</p></button>
                    <button class="btn btn-default" ng-click="cancel()"><p><i class="fa fa-book"></i> Отменить</p></button>
                    <br><br>
                </div>
            </script>

            <button class="btn btn-default" ng-click="open('lg')"><p><i class="fa fa-pencil-square-o"></i> Добавить новость</p></button>
        </div>
    </div>
    <div class="history">
        <h3>Перечень юридических лиц, зарегистрированных в системе</h3>
    </div>
    <div>
        <div id="resultListAccess-10" class="dib">

            <div class="pull-right">
                <button ng-disabled="!isDisabled" ng-click="toDeleteURL()" class="btn btn-default">
                    Удалить
                </button>
            </div>
            <table class="attestation table table-hover">
                <tr>
                    <th class="tableNumber">Номер</th>
                    <th>Название</th>
                    <th>Дата публикации</th>
                    <? if(Yii::app()->user->checkAccess(User::ROLE_ADMIN)): ?>
                    <th>Удаление</th>
                    <th>Редактирование</th>
                    <? endif; ?>
                </tr>
                <tr ng-repeat="new in news">
                    <td class="tableNumber">{{new.id}}</td>
                    <td>{{new.title}}</td>
                    <td>{{new.date}}</td>
                    <? if(Yii::app()->user->checkAccess(User::ROLE_ADMIN)): ?>
                    <td class="tableNumber"><input type="checkbox" ng-change="toDelete()" ng-model="selection.ids[new.id]"/></td>
                    <td class="tableNumber"><i class="fa fa-pencil" ng-click="toUpdate('lg',new)"></i></td>
                    <? endif; ?>
                </tr>
            </table>

            <div class="pull-right">
                <button ng-disabled="!isDisabled" ng-click="toDeleteURL()" class="btn btn-default">
                    Удалить
                </button>
            </div>
            <div class="spinner text-center main_col" ng-show="loading">
                <div class="spinner-container container1">
                    <div class="circle1"></div>
                    <div class="circle2"></div>
                    <div class="circle3"></div>
                    <div class="circle4"></div>
                </div>
                <div class="spinner-container container2">
                    <div class="circle1"></div>
                    <div class="circle2"></div>
                    <div class="circle3"></div>
                    <div class="circle4"></div>
                </div>
                <div class="spinner-container container3">
                    <div class="circle1"></div>
                    <div class="circle2"></div>
                    <div class="circle3"></div>
                    <div class="circle4"></div>
                </div>
            </div>
        </div>
        <div class="text-center">
            <pagination ng-model="bigCurrentPage" total-items="bigTotalItems"  max-size="maxSize" class="pagination-sm" boundary-links="true" num-pages="numPages" ng-change="pageChanged()"></pagination>
        </div>
    </div>
</div>
