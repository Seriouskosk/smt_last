<?php ?>

<div class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-collapse collapse" id="navbar-main">
      <ul class="nav navbar-nav">
        <? if(Yii::app()->user->checkAccess(User::ROLE_COMMISSION_MEMBER)): ?>
        <li class="">
          <a href="/certification/" id="themes"><i class="fa fa-user"></i> ����������� ����</a>
        </li>
        <?endif;?>

        <? if(Yii::app()->user->checkAccess(User::ROLE_MODER)): ?>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes2"><i class="fa fa-cog"></i> ���������� ������ <span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="themes2">
            <li><a href="/certification/center/<?=$centerId ?>"><i class="fa fa-home"></i> ����������� ����</a></li>
            <li><a href="/certification/users/<?=$centerId ?>"><i class="fa fa-user"></i> ����� �������������� ��������</a></li>
            <li><a href="/certification/devices/<?=$centerId ?>"><i class="fa fa-cog"></i> ����������</a></li>
            <li><a href="/certification/organizations/<?=$centerId ?>"><i class="fa fa-bank"></i> �����������</a></li>
          </ul>
        </li>
        <?endif;?>

        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes3"><i class="fa fa-file-text"></i> ���������� ����������� <span class="caret"></span></a>
            <ul class="dropdown-menu" aria-labelledby="themes3">
                <? if (Yii::app()->user->userAccess['welders']):?>
                <li><a href="/data/welders/<?=$centerId ?>"><i class="fa fa-user"></i> ���������� ���������</a></li>
                <? endif;?>
                <? if (Yii::app()->user->userAccess['tech']):?>
                <li><a href="/data/tech/<?=$centerId ?>"><i class="fa fa-cog"></i> ���������� ��������� ����������</a></li>
                <? endif;?>
                <? if (Yii::app()->user->userAccess['equip']):?>
                <li><a href="/data/equip/<?=$centerId ?>"><i class="fa fa-mobile"></i> ���������� ���������� ������������</a></li>
                <? endif;?>
            </ul>
        </li>
      </ul>
       <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes3"><i class="fa fa-bars"></i> ���� <span class="caret"></span></a>
            <ul class="dropdown-menu" aria-labelledby="themes3">
                <li>
                    <a href="<?=Yii::app()->createUrl("main/logout")?>"><i class="fa fa-sign-out"></i> <?=Yii::app()->topMenu->getUsername()?> (�����)</a>
                </li>
                <li>
                    <a href="/forum"><i class="fa fa-comments-o"></i> ������� �� �����</a>
                </li>
            </ul>
        </li>
      </ul>
    </div>
  </div>
</div>