<?php ?>

<div class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-collapse collapse" id="navbar-main">
      <ul class="nav navbar-nav">
        <li class="">
          <a href="/certification/all" id="themes"><i class="fa fa-user"></i> Юридические лица</a>
        </li> 
      </ul>  
       <ul class="nav navbar-nav navbar-right">
        <li>
            <a href="/forum"><i class="fa fa-comments-o"></i> Перейти на форум</a>
        </li> 
        <li>
            <a href="/messages"><i class="fa fa-weixin"></i> Обратная связь</a>
        </li> 
        <li ng-controller="ModalNews">
	        <script type="text/ng-template" id="myModalContent2.html">
	            <div ng-controller="addNewsCtrl" class="container app">
	              <h3>Добавить новость</h3>
	              <input ng-model="titlenews" type="text" class="form-control"> 
	              <div text-angular="text-angular" name="htmlcontent" ng-model="htmlcontent" ta-disabled='disabled'></div>
	              <br>
	                <button class="btn btn-default" ng-click="addNewsAjax();cancel()"><p><i class="fa fa-book"></i> Добавить новость</p></button>
	            <br><br> 

	            </div>
	        </script>
            <a href="#" ng-click="open('lg');"><i class="fa fa-book"></i> Добавить новость</a>
        </li> 
        <li>
            <a href="<?=Yii::app()->createUrl("main/logout")?>"><i class="fa fa-sign-out"></i> <?=Yii::app()->topMenu->getUsername()?> (выход)</a>
        </li> 
      </ul> 
    </div>
  </div>
</div>

  
<div id="wrapper">
	 <br>


    <div class="history">

	    <h3>Сообщения</h3>

    </div>

	<div  ng-controller="chat" class="dib" >
	    <div id="chatWithUser" class="dib"  ng-init="getMessage(<?=$userId?>);">
	        <div class="chatLeftSide">
	            <div class="scrollerChat">
	                <table class="table table-striped ">  
	                    <tr ng-repeat="message in messages | orderBy:predicate:!reverse">
	                        <td>
	                            <div class="topData">
	                                <div class="chatName"><b>{{message.name}}</b></div>
	                                <div class="chatData">{{message.date}}</div>
	                            </div>
	                            <div class="message">{{message.message}}</div>
	                        </td>
	                    </tr>
	                </table>
	            </div>
	            <textarea ng-model="ta_model" name="" id="" class="wide" rows="5"></textarea>
	            <button ng-click="sendMessageFromAdmin(<?=$userId?>)" class="btn btn-default pull-right">Отправить</button>
	        </div> 
	    </div>
	    <div id="chatWithUserList">
		    <div class="history">
		    	<h4>Активные диалоги</h4>
		    </div>
	        <table class="table table-striped " ng-init="getDialogs()">  
	            <tr ng-repeat="dialog in dialogs">
	                <td>
                        <a href="#" ng-click="getMessage2(dialog.userId)"  class="chatName"><b>{{dialog.name}}</b></a>
	                </td>
	            </tr>
	        </table>    	
	    </div>
	</div>
</div>
