<?

class CEquipSpec extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'equip_spec';
    }
    public static function renderPdf($certId)
    {
        $cEquip = CEquip::model()->findByPk($certId);
        $cData = $cEquip->getSpecialData();

        $html = "<h1>Отчет по специальным испытаниям оборудования:</h1><table>";

        $html .= "<tr><td>Организация:</td><td>" . $cData['orgName'] . "</td><td rowspan='10' class='map'>";
        $html .= "<img src='http://static-maps.yandex.ru/1.x/?size=420,420&amp;z=15&amp;l=map&amp;pt=".$cData['lon'].",".$cData['lat'].",pmwtm1' width='420'></td></tr>";
        $html .= "<tr><td>Член комиссии:</td><td>" . $cData['userName'] . "</td></tr>";
        $html .= "<tr><td>Шифр аттестационого центра:</td><td>" . $cData['centerName'] . "</td></tr>";
        $html .= "<tr><td>Номер программы:</td><td>" . $cData['programName'] . "</td></tr>";
        $html .= "<tr><td>Марка оборудования:</td><td>" . $cData['tradeMark'] . "</td></tr>";
        $html .= "<tr><td>Заводской номер:</td><td>" . $cData['factoryNum'] . "</td></tr>";
        $html .= "<tr><td>Инвертарный номер:</td><td>" . $cData['ivnNum'] . "</td></tr>";
        $html .= "<tr><td>Аттестация начата:</td><td>" . $cData['startDate'] . "</td></tr>";
        $html .= "<tr><td>Аттестация закончена:</td><td>" . $cData['endDate'] . "</td></tr>";
        $html .= "<tr><td>Общее время аттестации:</td><td>" . $cData['timeDiff'] . "</td></tr>";

        $html .= "</table>";

        $stylesheet = "table {font-size: 15px; font-family: Georgia, sans-serif;}";
        $stylesheet .= "table#params {font-size: 15px; margin-top: 20px;}";
        $stylesheet .= "table#params tr.color {background-color: #dbeaf0;}";
        $stylesheet .= "table#params tr.color td {width: 33%; border: 1px solid #000;}";
        $stylesheet .= "table td.map {padding-left: 60px;}";
        $stylesheet .= "h1 {font-size: 24px; text-align: center; display: block;}";
        //echo CParameter::getParameters($certId, 0);

        include(Yii::getPathOfAlias('webroot') . "/mpdf60/mpdf.php");
        //define("_JPGRAPH_PATH", Yii::getPathOfAlias('webroot') . "/jpgraph/src/");


        $mpdf = new mPDF('utf-8', 'A4-L', '8', '', 10, 10, 7, 7, 10, 10); /*задаем формат, отступы и.т.д.*/

        $mpdf->charset_in = 'utf-8'; /*не забываем про русский*/
        //$mpdf->useGraphs = true;
        $mpdf->list_indent_first_level = 0;

        $mpdf->WriteHTML($stylesheet,1);
        $mpdf->WriteHTML($html, 2); /*формируем pdf*/

        $html = "";
        $html .= "<table id='params' width='100%'><tr class='color'>";
        $html .= "<td>Рабочие точки</td>";
        $html .= "<td>Icp</td>";
        $html .= "<td>Ucp</td>";
        $html .= "</tr>";

        $html .= "<tr><td>Рабочая точка на режиме минимум</td><td>" . $cData['minI'] . "</td><td>" . $cData['minU'] . "</td></tr>";
        $html .= "<tr><td>Рабочая точка на режиме номинал</td><td>" . $cData['normI'] . "</td><td>" . $cData['normU'] . "</td></tr>";
        $html .= "<tr><td>Рабочая точка на режиме максимум</td><td>" . $cData['maxI'] . "</td><td>" . $cData['maxU'] . "</td></tr>";


        $html .= "</table>";
        $mpdf->WriteHTML($html, 2); /*формируем pdf*/

        $vacPoints = CEquipVac::getVac($certId);

        $data = $vacPoints['minMode'];
        $fileName = date('d-m-Y-H-i-s') . CEquipSpec::generateRandomString(5) . "minMode.png";

        CEquipSpec::createGraph($data, $fileName);

        $html = "";
        $html .= "<h1>Точки ВАХ в режиме MIN:</h1>";
        $html .= "<img src='" . Yii::app()->homeUrl . "upload/" . $fileName . "' />";
        $mpdf->WriteHTML($html, 2);


        $data = $vacPoints['normMode'];
        $fileName = date('d-m-Y-H-i-s') . CEquipSpec::generateRandomString(5) . "normMode.png";

        CEquipSpec::createGraph($data, $fileName);

        $html = "";
        $html .= "<h1>Точки ВАХ в режиме NORM:</h1>";
        $html .= "<img src='" . Yii::app()->homeUrl . "upload/" . $fileName . "' />";
        $mpdf->WriteHTML($html, 2);

        $data = $vacPoints['maxMode'];
        $fileName = date('d-m-Y-H-i-s') . CEquipSpec::generateRandomString(5) . "maxMode.png";

        CEquipSpec::createGraph($data, $fileName);

        $html = "";
        $html .= "<h1>Точки ВАХ в режиме MAX:</h1>";
        $html .= "<img src='" . Yii::app()->homeUrl . "upload/" . $fileName . "' />";
        $mpdf->WriteHTML($html, 2);

        //$html = "<a href='" . Yii::app()->homeUrl. "data/equip/" . $cEquip->centerId . "/" . $cEquip->id . "/special/'>Посмотреть графики ВАХ</a>";
        //$mpdf->WriteHTML($html, 2); /*формируем pdf*/

        return $mpdf->Output('mpdf.pdf', 'I');
    }

    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function createGraph($data, $fileName)
    {
        $width = 950; $height = 250;

        $img = @imagecreatetruecolor($width, $height);

        $white = imagecolorallocate($img, 255, 255, 255);
        $black = imagecolorallocate($img, 0, 0, 0);
        $red = imagecolorallocate($img, 255, 0, 0);

        imagefill($img, 0, 0, $white);

        //$data = $vacPoints['minMode'];

        $maxValue = 0;
        foreach ($data as $item)
            if($maxValue < $item->u)
                $maxValue = $item->u;

        $count = count($data);
        $stroke = 20;

        $step = $width / ($count - 1);
        $location = 0;

        for($i = 0; $i < $count - 1; $i += 1)
        {
            $current = $data[$i];
            $next = $data[$i + 1];

            $x1 = $location;//$data[$i]['u'];
            $x2 = $location + $step;
            $x3 = $location + $step;

            $y1 = $height - $height * $current->u / $maxValue + $stroke;
            $y2 = $height - $height * $next->u / $maxValue + $stroke;
            $y3 = $height - $stroke;

            imageline($img, $x1, $y1, $x2, $y2, $red);
            imageline($img, $x2, $y2, $x3, $y3, $red);

            if($i % 5 == 0) {
                imagestring($img, 3, $x1, $y1 - $stroke, $current->u, $black);
                imagestring($img, 3, $x3, $y3, $current->i, $black);
            }

            $location += $step;
        }

        imagepng($img, Yii::getPathOfAlias('webroot') . "/upload/" . $fileName);
        imagedestroy($img);
    }
}